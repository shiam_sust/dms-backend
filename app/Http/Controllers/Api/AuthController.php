<?php

namespace App\Http\Controllers\Api;

use App\Jobs\SendWelcomeEmailJob;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Mail\SignupMail;
use App\Mail\SigninMail;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Crypt;
use App\Rules\Recaptcha;
use DateTime;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api',
            ['except' => [ 'timeDiff','resendPasscode', 'changePassword',
                            'validatePasscode', 'sendPasscode','login',
                            'register','forgotPassword']]
        );
    }

    public function sendPasscode(Request $request)
    {
        $rand_passcode = $this->passcode_generate(6);
        $user = User::where('email', $request->email)->first();
        if($user){
            if (Hash::check($request->password, $user->password)) {
                $user->passcode = $rand_passcode;
                $user->save_passcode_at = date('Y-m-d H:i:s');
                $user->save();
                $this->sendPasscodeEmail($user->email, $user->first_name, $rand_passcode);
                return response()->json(['passcode' => 'passcode sent'], 200);
            }else{
                return response()->json(['errorPass' => 'password not correct'], 422);
            }
        }else{
            return response()->json(['error' => 'no such email found'], 422);
        }
    }

    public function resendPasscode(Request $request)
    {
        $rand_password = $this->passcode_generate(6);
        $user = User::where('email', $request->email)->first();
        if($user){
            $user->passcode = $rand_password;
            $user->save_passcode_at = date('Y-m-d H:i:s');
            $user->save();
            $this->sendEmail($user->email, $user->first_name, $rand_password, "su");
        }else{
            return response()->json(['error' => 'no such email found'], 422);
        }
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        //return($request);
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
            'passcode' => ['required'],
        ]);

        $user = User::where('email', $request->email)->first();
        $previous = new DateTime($user->save_passcode_at);
        $current = new DateTime(date('Y-m-d H:i:s'));
        $diff = $previous->diff($current);
        if($diff->i > 10){
            return response()->json(['latePasscode' => 'click resend button to send passcode again'], 422);
        }else{
            $credentials = $request->only('email', 'password', 'passcode');

            if ($token = $this->guard()->attempt($credentials)) {
                return $this->respondWithToken($token);
            }
            return response()->json(['error' => 'Unauthorized'], 422);
        }
    }

    public function register(Request $request, Recaptcha $recaptcha)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|between:2,100',
            'last_name' => 'required|string|between:2,100',
            'email' => 'required|email',
//            'email' => 'required|email|unique:users',
            'date_of_birth' => 'required',
            'recaptcha' => ['required', $recaptcha],
        ]);

        if($validator->fails()) {
            return response()->json([
                $validator->errors()
            ],422);
        }

        $age = $this->calculateAge(date('Y-m-d', strtotime($request->date_of_birth)));

        if($age < 18){
            return response()->json(['age_error' => 'your age must be 18 year or more.'], 422);
        }else{
            $rand_pass = $this->passcode_generate(6);

            $user = User::where('email', $request->email)->first();
            if ($user && $user->is_verified) {
                return response()->json([['email' => ['Email address already taken']]], 422);
            }

            if (!$user) {
                $user = new User();
                $user->email = $request->email;
            }

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->date_of_birth = date('Y-m-d', strtotime($request->date_of_birth));
            $user->passcode = $rand_pass;
            $user->save_passcode_at = date('Y-m-d H:i:s');
            $user->plan_id = 4;
            $user->default_personal_plan_id = 4;
            $user->save();

//            $user = User::create([
//                'first_name' => $request->first_name,
//                'last_name' => $request->last_name,
//                'date_of_birth' => date('Y-m-d', strtotime($request->date_of_birth)),
//                'email' => $request->email,
//                'password' => Hash::make($rand_pass)
//            ]);

            $flag = "su";

            $this->sendEmail($request->email, $request->first_name, $rand_pass, $flag);

            return response()->json(['message' => 'User Created successfully', 'user' => $user]);
        }
    }

    private function calculateAge($date_of_birth){
        $today = date("Y-m-d");
        $diff = date_diff(date_create($date_of_birth), date_create($today));
        return $diff->format('%y');
    }

    /**
     * This method was wrong
     * Now it's validating passcode not password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validatePasscode(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $previous = new DateTime($user->save_passcode_at);
        $current = new DateTime(date('Y-m-d H:i:s'));
        $diff = $previous->diff($current);
        //return $diff->i;
        if($diff->i > 10){
            return response()->json(['passcodeLate'=>"passcode late submeiited"], 422);
        }else{
            $user = User::where('email', $request->email)->where('passcode', $request->password)->first();
            if ($user)
            {
                return response()->json(['success'=>"password matched"], 200);
            }else{
                return response()->json(['error'=>"password not matched"], 422);
            }
        }
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'passcode' => 'required',
            'email' => 'required|email',
        ]);

        if($validator->fails()) {
            return response()->json([
                $validator->errors()
            ],422);
        }

        $user = User::where('email', $request->email)->where('passcode', $request->passcode)->first();
        if($user){
            $user->password = Hash::make($request->password);
            if(!$user->is_verified) {
                $user->is_verified = 1;
                $user->verified_at = date('Y-m-d H:i:s');
            }

            $user->save();
            if(!$request->has('from')) {
                $this->sendEmail($user->email, $user->first_name, "", "welcome");
            }

            $this->sendSuccessEmail($user->email, $user->first_name, $request->fmode);

            return response()->json(['message'=>"password changed successfully"], 200);
        }else{
            return response()->json(['message'=>"password can't be changed"], 422);
        }
    }

    public function forgotPassword(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $type = $request->type;

        if($user){
            $rand_pass = $this->password_generate(8);

            if(!$user->is_verified) {
                $user->is_verified = 1;
                $user->verified_at = date('Y-m-d H:i:s');
            }
            //$user->password = Hash::make($rand_pass);
            $user->password = Hash::make($rand_pass);
            $user->save();

            $flag = "fp";

            $this->sendForgotEmail($user->email, $user->first_name, $rand_pass, $type);

            return response()->json(['message' => 'Password sent successfully', 'user' => $user]);
        }else{
            return response()->json([
                'message' => 'unregistered email provided!'
            ],422);
        }
    }

    public function sendSuccessEmail($email, $name, $type)
    {
        $details = [
            'title' => 'Mail from Document Management System',
            'name' => $name,
            'email' => $email,
        ];

        if($type == 'femail')
            dispatch(new \App\Jobs\ChangedEmailJob($details));
        else{
            dispatch(new \App\Jobs\ChangedPasswordJob($details));
        }
        //Mail::to($email)->send(new ForgotPasswordMail($details));
    }

    public function sendForgotEmail($email, $name, $rand_pass, $type)
    {
        $subject = '';
        if($type == 'email'){
            $subject = 'Forgot Username – New Password';
        }else{
            $subject = 'Forgot Password – New Password';
        }

        $details = [
            'title' => 'Mail from Document Management System',
            'password' => $rand_pass,
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
        ];

        dispatch(new \App\Jobs\SendForgotPasswordEmailJob($details));
        //Mail::to($email)->send(new ForgotPasswordMail($details));
    }

    public function sendEmail($email, $name, $rand_pass, $flag)
    {
        $details = [
            'title' => 'Mail from Document Management System',
            'password' => $rand_pass,
            'name' => $name,
            'email' => $email,
        ];

        if($flag == "welcome"){
            dispatch(new SendWelcomeEmailJob($details));
        }
        elseif($flag == "fp"){
            dispatch(new \App\Jobs\SendForgotPasswordEmailJob($details));
            //Mail::to($email)->send(new ForgotPasswordMail($details));
        }else{
            dispatch(new \App\Jobs\SendSignupEmailJob($details));
            //Mail::to($email)->send(new SignupMail($details));
        }
    }

    public function sendPasscodeEmail($email, $name, $rand_passcode)
    {
        $details = [
            'title' => 'Mail from Document Management System',
            'passcode' => $rand_passcode,
            'name' => $name,
            'email' => $email,
        ];

        dispatch(new \App\Jobs\SendEmailJob($details));

        // Mail::to($email)->send(new SigninMail($details));
    }

    private function password_generate($chars)
    {
        $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($data), 0, $chars);
    }

    private function passcode_generate($chars)
    {
        $data = '1234567890';
        return substr(str_shuffle($data), 0, $chars);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }



    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

    // public function timeDiff(Request $request)
    // {
    //     $rand_passcode = $this->passcode_generate(6);
    //     $user = User::where('email', $request->email)->first();
    //     if($user){
    //         if (Hash::check($request->password, $user->password)) {
    //             $user->passcode = $rand_passcode;
    //             //$user->save_passcode_at = date('Y-m-d H:i:s');
    //             $previous = new DateTime($user->save_passcode_at);
    //             $current = new DateTime(date('Y-m-d H:i:s'));
    //             $diff = $previous->diff($current);
    //             return $diff->i;
    //             //$diff = abs($previous - $current);
    //             //return $diff/60;
    //             $user->save();
    //             $this->sendPasscodeEmail($user->email, $user->first_name, $rand_passcode);
    //             return response()->json(['passcode' => 'passcode sent'], 200);
    //         }else{
    //             return response()->json(['errorPass' => 'password not correct'], 422);
    //         }
    //     }else{
    //         return response()->json(['error' => 'no such email found'], 422);
    //     }
    // }
}
