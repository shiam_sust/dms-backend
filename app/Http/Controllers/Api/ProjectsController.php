<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\General\CollectionHelper;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Http\Resources\Projects\ProjectResource;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Repositories\Exceptions\Projects\ProjectCreateErrorException;


class ProjectsController extends Controller
{
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepo;

    /**
     * @var ProjectRepositoryInterface $projectRepo
     */
    public function __construct(ProjectRepositoryInterface $projectRepo)
    {
        $this->projectRepo = $projectRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $projects = $this->projectRepo->index($request);
        $perPage = $request->per_page?$request->per_page:20;
        return ProjectResource::collection(CollectionHelper::paginate($projects, $perPage));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchProject(Request $request)
    {
        $projects = $this->projectRepo->getSearchedProject($request);
        $perPage = $request->per_page?$request->per_page:20;
        return ProjectResource::collection(CollectionHelper::paginate($projects, $perPage));
    }

    /**
     * @param CreateProjectRequestValidation $request
     * @return ProjectResource
     * @throws ProjectCreateErrorException
     */
    public function store(CreateProjectRequestValidation $request)
    {
        try {
            $project = $this->projectRepo->store($request);
            return new ProjectResource($project);
        } catch (\Exception $e) {
            throw new ProjectCreateErrorException($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param UpdateProjectRequestValidation $request
     * @param $id
     * @return ProjectResource
     * @throws ProjectUpdateErrorException
     */
    public function update(CreateProjectRequestValidation $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
