<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Notifications\AlertResource;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\NotificationRepositoryInterface;
use App\Helper\General\CollectionHelper;
use App\Http\Resources\Notifications\NotificationResource;


class NotificationsController extends Controller
{
    /**
     * @var NotificationRepositoryInterface
     */
    private $notificationRepo;

    /**
     * @var NotificationRepositoryInterface $notificationRepo
     */
    public function __construct(NotificationRepositoryInterface $notificationRepo)
    {
        $this->notificationRepo = $notificationRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $notifications = $this->notificationRepo->index($request);
        $perPage = $request->per_page?$request->per_page:20;
        return NotificationResource::collection(CollectionHelper::paginate($notifications, $perPage));
    }

    public function getAlerts(Request $request)
    {
        $user = auth()->user();
        return AlertResource::collection($user->alerts);
    }

    public function markAsRead(){
        $user = auth()->user();
        //return $user->notifications;
        foreach ($user->notifications as $notification)
        {
            if($notification->is_read == 0){
                $notification->is_read = 1;
                $notification->save();
            }
        }
    }

    public function notificationCount(){
        $user = auth()->user();
        //return $user->notifications;
        $count = 0; 
        foreach ($user->notifications as $notification)
        {
            if($notification->is_read == 0){
                ++$count;
            }
        }

        return $count;

    }


}
