<?php

namespace App\Http\Controllers\Api\PublicProjects;

use App\Events\ProjectCreatedEvent;
use App\Helper\General\CollectionHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Documents\StoreDocumentsRequestValidation;
use App\Http\Requests\Projects\AssignCollaboratorsRequestValidation;
use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\AddCollaboratorsRequestValidation;
use App\Http\Requests\Projects\AddTotalCollaboratorsRequestValidation;
use App\Http\Requests\Projects\AddSupportStaffsRequestValidation;
use App\Http\Requests\Projects\ImportCsvRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Http\Requests\Projects\VerfiyProjectPIDRequestValidation;
use App\Http\Resources\Documents\DocumentResource;
use App\Http\Resources\Projects\ProjectDetailsResource;
use App\Http\Resources\Projects\ProjectListResource;
use App\Http\Resources\Projects\ExternalProjectListResource;
use App\Http\Resources\Projects\ProjectPrimaryInfoResource;
use App\Http\Resources\Projects\ProjectResource;
use App\Http\Resources\Projects\PublicProjectDetailsResource;
use App\Repositories\Exceptions\Attachments\AttachmentErrorException;
use App\Repositories\Exceptions\Documents\AssignCollaboratorsException;
use App\Http\Resources\Users\ProjectUserResource;
use App\Repositories\Exceptions\Documents\DocumentNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectCopyFailedException;
use App\Repositories\Exceptions\Projects\ProjectNotFoundException;
use App\Repositories\Exceptions\Projects\UserRemoveException;
use App\Repositories\Exceptions\Users\AssignProjectToUserErrorException;
use App\Repositories\Exceptions\Users\CreateUserErrorException;
use App\Repositories\Exceptions\Documents\DocumentCreateErrorException;
use App\Repositories\Exceptions\Projects\CreateDeadlineErrorException;
use App\Repositories\Exceptions\Projects\ProjectCreateErrorException;
use App\Repositories\Exceptions\Projects\CreateCollaboratorsNumberErrorException;
use App\Repositories\Exceptions\Projects\ProjectDeleteFailedException;
use App\Repositories\Exceptions\Projects\ProjectQueryException;
use App\Repositories\Interfaces\PublicProjectRepositoryInterface;
use App\Services\PublicProjectService;
use App\Utils\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use App\Http\Resources\Projects\ExternalPublicForAccountResource;

class PublicProjectsController extends Controller
{

    /**
     * @var PublicProjectService
     */
    private $projectService;

    /**
     * @var PublicProjectRepositoryInterface
     */
    private $projectRepo;

    /**
     * PrivateProjectsController constructor.
     * @param ProjectService $projectService
     */
    public function __construct(PublicProjectService $projectService, PublicProjectRepositoryInterface $projectRepo)
    {
        $this->projectService = $projectService;
        $this->projectRepo = $projectRepo;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws ProjectQueryException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index(Request $request)
    {
        try {
            $perPage = $request->per_page?$request->per_page:20;
            return ExternalProjectListResource::collection(CollectionHelper::paginate($this->projectRepo->index($request), $perPage));
        } catch(\ErrorException $exception) {
            throw new ProjectQueryException($exception->getMessage(), $exception->getCode());
        }

    }

    /**
     * @param CreateProjectRequestValidation $request
     * @return ProjectResource
     * @throws ProjectCreateErrorException
     */
    public function store(CreateProjectRequestValidation $request)
    {
        try {
            $project = $this->projectService->store($request);
            return new ProjectResource($project);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param UpdateProjectRequestValidation $request
     * @param $id
     * @return ProjectResource
     * @throws ProjectCreateErrorException
     */
    public function update(UpdateProjectRequestValidation $request, $id)
    {
        try {
            $project = $this->projectService->update($request,$id);
            return new ProjectResource($project);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $id
     * @return ProjectPrimaryInfoResource
     */
    public function getPrimaryInfo($id)
    {
        try {
            return new ProjectPrimaryInfoResource($this->projectRepo->getUserSpecificProject($id));
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    public function setDeadline(CreateProjectDeadlineRequestValidation $request, $project_id){
        try {
            return new ProjectResource($this->projectService->setDeadline($request,$project_id));
        }catch (\ErrorException $exception){
            throw new CreateDeadlineErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $id
     * @param AddTotalCollaboratorsRequestValidation $request
     * @return ProjectResource
     * @throws CreateUserErrorException
     */
    public function storeTotalCollaborators($id, AddTotalCollaboratorsRequestValidation $request)
    {
        try {
            $project = $this->projectService->storeCollaboratorsNumber($request->total_collaborators, $id);
            return new ProjectResource($project);
        }catch (\ErrorException $exception){
            throw new CreateCollaboratorsNumberErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     */
    public function storeDocuments(StoreDocumentsRequestValidation $request, $project_id)
    {
        try {
            $this->projectService->storeDocuments($request, $project_id);
            return response()->json(['message'=>"Documents Requirements Successfully Saved"], 201);
        }catch (\ErrorException $exception){
            throw new DocumentCreateErrorException($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @param $projectId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getDocuments($projectId)
    {
        $project = $this->projectRepo->show($projectId);
        return DocumentResource::collection($project->documents_by_position);
    }

    /**
     * @param Request $request
     * @param $projectId
     * @param $document_id
     * @return \Illuminate\Http\JsonResponse
     * @throws DocumentNotFoundException
     */
    public function removeDocument(Request $request, $projectId, $document_id){
        try{
            $this->projectService->removeDocument($document_id);
            return response()->json(["message" => "You have been successfully removed document from this project"], 200);
        } catch (\ErrorException $error){
            throw new DocumentNotFoundException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $projectId
     * @param $document_id
     * @return \Illuminate\Http\JsonResponse
     * @throws DocumentNotFoundException
     */
    public function removeTemplate(Request $request, $projectId, $document_id){
        try {
            $this->projectService->removeTemplate($document_id);
            return response()->json(["message" => "You have been successfully removed template from document"], 200);
        }catch (\ErrorException $exception){
            throw new DocumentNotFoundException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws ProjectCreateErrorException
     */
    public function getSupportStaffs($id)
    {
        try {
            $project = $this->projectRepo->show($id);
            return ProjectUserResource::collection($project->support_staffs);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $id
     * @param AddSupportStaffsRequestValidation $request
     * @return ProjectResource
     * @throws CreateUserErrorException
     * @throws ProjectCreateErrorException
     */
    public function storeSupportStaffs($id, AddSupportStaffsRequestValidation $request)
    {
        try {
            $project = $this->projectService->storeCollaboratorOrSupportStaff($request->supportStaffs, $id, Constant::USER_TYPE_SUPPORT_STAFF);
            return new ProjectResource($project);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     * @throws UserRemoveException
     */
    public function removeUser(Request $request, $project_id, $user_id)
    {
        try {
            if(!$request->has("user_type")){
                throw new UserRemoveException("User type is required.", 400);
            }
            $this->projectService->removeUser($project_id, $user_id, $request->user_type);
            return response()->json(["message" => "You have been successfully removed user from this project"], 200);
        }catch (\ErrorException $exception){
            throw new UserRemoveException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     * @throws ProjectCreateErrorException
     */
    public function startProject(Request $request, $project_id)
    {
        try{
            $this->projectService->startProject($project_id);
            return response()->json(['message'=>"Your project has been successfully started!"], 200);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), 500);
        }
    }

    public function generatePid($id, AddTotalCollaboratorsRequestValidation $request)
    {
        try {
            $project = $this->projectService->storeCollaboratorsNumber($request->total_collaborators, $id);
            return new ProjectResource($project);
        }catch (\ErrorException $exception){
            throw new CreateCollaboratorsNumberErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAttachment(Request $request,$project_id, $id)
    {
        try {
            $this->projectService->deleteAttachment($project_id, $id);
            return response()->json(['message'=>"Attachment successfully deleted"], 200);
        }catch (AttachmentErrorException $exception){
            return response()->json(['error'=> $exception->getMessage()], 500);
        }
    }

    /**
     * @param VerfiyProjectPIDRequestValidation $request
     * @return \Illuminate\Http\JsonResponse
     * @throws CreateCollaboratorsNumberErrorException
     */
    public function verifyProjectByPID(VerfiyProjectPIDRequestValidation $request){
        try {
            $this->projectRepo->verifyProjectByPID($request->pid);
            return response()->json(['message'=>"You can add collaborator in this project"], 200);
        }catch (\ErrorException $exception){
            throw new CreateCollaboratorsNumberErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $pid
     * @return PublicProjectDetailsResource
     */
    public function projectDetailsByPID(Request $request, $pid)
    {
        return new PublicProjectDetailsResource($this->projectRepo->getProjectByPID($pid));
    }

    /**
     * @param Request $request
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     * @throws AssignProjectToUserErrorException
     */
    public function addProject(Request $request, $project_id)
    {
        try {
            $this->projectService->addProject($project_id);
            return response()->json(['message'=>"You are successfully assigned in this project"], 200);
        }catch (\ErrorException $exception){
            throw new AssignProjectToUserErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    public function accountSettingExternal(Request $request){
        $users = auth()->user();
        // $projects = $this->projectRepo->accountSettingExternal();
        // return $projects;

        if($users->projects){
            return new ExternalPublicForAccountResource($users);
        }
        return response()->json(['message'=>"No external project is available"], 404);
    }

}
