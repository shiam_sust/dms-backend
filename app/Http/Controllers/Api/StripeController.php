<?php

namespace App\Http\Controllers\Api;

require_once('../vendor/autoload.php');

use App\Http\Controllers\Controller;
use App\Http\Requests\Payments\AddCardRequestValidation;
use App\Http\Requests\Payments\SaveCardRequestValidation;
use App\Http\Requests\Payments\SaveDefaultPlanRequest;
use App\Http\Requests\Payments\SubscribePersonalPlanRequest;
use App\Http\Requests\Teams\StoreTeamPlanRequest;
use App\Http\Resources\PaymentMethods\PaymentMethodsResource;
use App\Http\Resources\Plans\DefaultPlanResource;
use App\Repositories\Exceptions\Stripes\StripeErrorException;
use App\Services\StripeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \Stripe\Stripe;

class StripeController extends Controller
{
    /**
     * @var StripeService
     */
    private $stripeService;

    /**
     * StripeController constructor.
     * @param StripeService $stripeService
     */
    public function __construct(StripeService $stripeService)
    {
        $this->stripeService = $stripeService;
    }

    public function index(Request $request){
        return PaymentMethodsResource::collection($this->stripeService->index($request));
    }
    /**
     * @param AddCardRequestValidation $request
     * @return \Illuminate\Http\JsonResponse
     * @throws StripeErrorException
     */
    public function storePaymentMethod(AddCardRequestValidation $request)
    {
        try {
            $this->stripeService->storePaymentMethod($request);
            return response()->json(["message" => "Payment method saved successfully"]);
        }catch (\ErrorException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws StripeErrorException
     */
    public function deletePaymentMethod(Request $request, $id)
    {
        try {
            $this->stripeService->delete($id);
            return response()->json(["message" => "Payment method deleted successfully"]);
        }catch (\ErrorException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @param SaveCardRequestValidation $request
     * @return \Illuminate\Http\JsonResponse
     * @throws StripeErrorException
     */
    public function saveCreditCard(SaveCardRequestValidation $request)
    {
        try {
            $this->stripeService->saveCreditCard($request);
            return response()->json(["message" => "Payment method updated successfully"]);
        }catch (\ErrorException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param SaveDefaultPlanRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws StripeErrorException
     */
    public function saveDefaultPlans(SaveDefaultPlanRequest $request)
    {
        try {
            $this->stripeService->saveDefaultPlans($request);
            return response()->json(["message" => "Default plans saved!"]);
        }catch (\ErrorException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $teamId
     * @return \Illuminate\Http\JsonResponse
     * @throws StripeErrorException
     */
    public function teamSubscriptionCancel(Request $request, $teamId)
    {
        try {
            $this->stripeService->teamSubscriptionCancel($teamId);
            return response()->json(["message" => "Team plan subscription successfully stopped!"]);
        }catch (\ErrorException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function personalSubscriptionCancel(Request $request)
    {
        try {
            $this->stripeService->personalSubscriptionCancel();
            return response()->json(["message" => "Team plan subscription successfully stopped!"]);
        }catch (\ErrorException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }


    /**
     * @param SubscribePersonalPlanRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws StripeErrorException
     */
    public function subscribePersonalPlan(SubscribePersonalPlanRequest $request)
    {
        try {
            $this->stripeService->subscribePersonalPlan($request);
            return response()->json(["message" => "Personal plan subscribed successfully!"]);
        }catch (\ErrorException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function getDefaultPlans(Request $request)
    {
        try {
            return new DefaultPlanResource(auth()->user());
        }catch (\ErrorException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }
}
