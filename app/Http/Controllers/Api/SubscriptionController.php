<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plan;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{
    protected $stripe;

    public function __construct() 
    {
        $this->stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
    }

    public function create(Request $request, Plan $plan)
    {
        //$plan = Plan::findOrFail($request->get('plan'));
        $plan = Plan::findOrFail(1);
        
        //$user = $request->user();
        $user = Auth::user();

        $paymentMethod = $request->paymentMethod;

        $user->createOrGetStripeCustomer();
        $user->updateDefaultPaymentMethod($paymentMethod);
        $user->newSubscription('default', $plan->stripe_plan)
            ->create($paymentMethod, [
                'email' => $user->email,
            ]);
        
        return response()->json(['message' => 'Your plan subscribed successfully']);

    }

    // public function createPlan()
    // {
    //     return view('plans.create');
    // }

    public function storePlan(Request $request)
    {   
        $data = $request->all();

        //$data['slug'] = strtolower($data['name']);
        $price = $data['plan_price'];

        //create stripe product
        $stripeProduct = $this->stripe->products->create([
            'name' => $data['plan_name'],
        ]);
        
        //Stripe Plan Creation
        $stripePlanCreation = $this->stripe->plans->create([
            'amount' => $price,
            'currency' => 'bdt',
            'interval' => 'month', //  it can be day,week,month or year
            'product' => $stripeProduct->id,
        ]);

        $data['stripe_plan'] = $stripePlanCreation->id;

        Plan::create($data);

        return response()->json(['message' => 'plan has been created successfully']);

    }
}
