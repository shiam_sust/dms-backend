<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\AttachmentRepositoryInterface;
use App\Repositories\Exceptions\Users\ProfileUpdateErrorException;
use App\Repositories\Exceptions\Users\ProfileGetDocumentsErrorException;
use App\Http\Resources\Users\ProfileUserResource;
use App\Http\Resources\Users\ProfileDocumentResource;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\ProfileDocument;
use Storage;
use Response;

class ProfileController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;
    private $attachmentRepo;

    /**
     * @var UserRepositoryInterface $userRepo
     */
    public function __construct(UserRepositoryInterface $userRepo, AttachmentRepositoryInterface $attachmentRepo)
    {
        $this->userRepo = $userRepo;
        $this->attachmentRepo = $attachmentRepo;
    }

    public function profileStore(Request $request){
        try {
            $user = $this->userRepo->show(auth()->id());
            $user = $this->userRepo->storeProfile($user, $request);
            return $user;
        } catch (\Exception $e) {
            throw new ProfileUpdateErrorException($e);
        }
    }

    public function passcodeSendForPassword(Request $request)
    {
        $rand_passcode = $this->passcode_generate(6);
        $user = $this->userRepo->show(auth()->id());
        if (Hash::check($request->old_password, $user->password)) 
        {
            $user->passcode = $rand_passcode;
            $user->save();
            $this->sendPasscodeEmail($user->email, $user->first_name, $rand_passcode, 'password', 'Update Password');  
        }else{
            return response()->json(['error' => 'Password Not Match'], 422);
        }
    }

    public function passcodeToUsername(Request $request)
    {
        $rand_passcode = $this->passcode_generate(6);
        $user = $this->userRepo->show(auth()->id());
        if($user->email == $request->current_username)
        {
            $existing_user = User::where('email', $request->new_username)->first();
            if($existing_user){
                return response()->json(['new_error' => 'Email is Already in Use'], 422);
            }else{
                $user->passcode = $rand_passcode;
                $user->save();
                $this->sendPasscodeEmail($request->new_username, $user->first_name, $rand_passcode, 'username', 'Update Username');
            }
        }else{
            return response()->json(['error' => 'Email Not Found'], 422);
        }
    }

    public function resendPasscode(Request $request)
    {
        $rand_passcode = $this->passcode_generate(6);
        $user = $this->userRepo->show(auth()->id());
        $user->passcode = $rand_passcode;
        $user->save();
        $this->sendPasscodeEmail($request->new_username, $user->first_name, $rand_passcode, 'username', 'Update Username');
        return response()->json(['message' => 'New Passcode Send Successfully'], 200);
    }

    public function resendPasscodePassword(Request $request)
    {
        $rand_passcode = $this->passcode_generate(6);
        $user = $this->userRepo->show(auth()->id());
        $user->passcode = $rand_passcode;
        $user->save();
        $this->sendPasscodeEmail($user->email, $user->first_name, $rand_passcode, 'password', 'Update Password');
        return response()->json(['message' => 'New Passcode Send Successfully'], 200);
    }

    private function passcode_generate($chars)
    {
        $data = '1234567890';
        return substr(str_shuffle($data), 0, $chars);
    }

    public function sendPasscodeEmail($email, $name, $rand_passcode, $type, $subject)
    {
        $details = [
            'title' => 'Mail from Document Management System',
            'passcode' => $rand_passcode,
            'name' => $name,
            'email' => $email,
            'type' => $type,
            'subject' => $subject
        ];

        dispatch(new \App\Jobs\ProfileEmailJob($details));

        // Mail::to($email)->send(new SigninMail($details));
    }

    public function updateUsername(Request $request)
    {
        $user = $this->userRepo->show(auth()->id());

        try {
            $user = $this->userRepo->show(auth()->id());
            if($user->passcode == $request->passcode)
            {
                $user = $this->userRepo->updateUsername($user, $request);
            }
            $this->sendConfirmationEmail($user->email, $user->first_name, 'username', 'Username Updated');  
            return response()->json(['message' => 'Username Updated Successfully'], 200);
        } catch (\Exception $e) {
            throw new ProfileUpdateErrorException($e);
        }
    }

    public function updatePassword(Request $request)
    {
        try {
            $user = $this->userRepo->show(auth()->id());
            if($user->passcode == $request->passcode)
            {
                $user = $this->userRepo->updatePassword($user, $request);
            }
            $this->sendConfirmationEmail($user->email, $user->first_name, 'password', 'Password Updated');  
            return response()->json(['message' => 'Password Updated Successfully'], 200);
        } catch (\Exception $e) {
            throw new ProfileUpdateErrorException($e);
        }
    }

    public function sendConfirmationEmail($email, $name, $type, $subject)
    {
        $details = [
            'title' => 'Mail from Document Management System',
            'name' => $name,
            'email' => $email,
            'type' => $type,
            'subject' => $subject
        ];

        dispatch(new \App\Jobs\ProfileUpdateConfirmationJob($details));

        // Mail::to($email)->send(new SigninMail($details));
    }

    public function uploadDocuments(Request $request)
    {
        //return $request;
        try {
            $user = $this->userRepo->show(auth()->id());
            $this->userRepo->storeProfileAttachments($request, $user);
            return response()->json(['message' => 'Documents Uploaded Successfully'], 200);
        } catch (\Exception $e) {
            throw new ProfileUpdateErrorException($e);
        }
    }

    public function getDocuments(Request $request)
    {
        try {
            // return ProfileDocumentsResource::collection($this->userRepo->profileHasDocument());
            $docs = auth()->user()->profile_documents;
            //return $docs;
            if($docs){
                return ProfileDocumentResource::collection($docs);
            }
            return response()->json(['message'=>"No doc is available"], 404);
        }catch (\ErrorException $exception){
            throw new ProfileGetDocumentsErrorException($exception->getMessage(),$exception->getCode());
        }
    }

    public function deleteDocument(Request $request, $id){
        $doc = ProfileDocument::find($id);
        $doc->delete();
        return response()->json(['message'=>"document deleted successfully"], 200);
    }

    public function downloadDocument(Request $request, $id){
        $doc = ProfileDocument::find($id);
        $file_link = storage_path().'/'.'app/public'.'/uploads/profile_'.$doc->user_id.'/documents/'.$doc->attachment;
        if(file_exists($file_link)){
            $headers = ['Content-Type' => 'application/pdf'];
            return response()->download($file_link, $doc->attachment , $headers);
            // return Response::download($file_link, $headers);
        }else{
            return response()->json(['error'=>"document download error"], 404);
        }
    }
    

    public function getPersonalAccontData()
    {
        $projects = auth()->user()->projects->where('team_id', null);
        $personal_docs = auth()->user()->profile_documents->sum('file_size');
        $personal_docs = $this->byteToGb($personal_docs);
        //return $personal_docs; 
        $total_size = 0;
        $personal_active = null;
        $array = array();
        
        foreach($projects as $project){
            $array[$project->id] = false;
        }
        foreach($projects as $project){
            if($project->status == 'ACTIVE' && $array[$project->id] === false)
            {
                foreach($project->attachments as $attachment){
                    $total_size = $total_size + $attachment->file_size;
                        //return $attachment;
                }
                $array[$project->id] = true;
            }
        }
        $personal_active = $total_size;
        $personal_active = $this->byteToGb($personal_active);

        //return $personal_active;

        $total_size = 0;

        foreach($projects as $project){
            $array[$project->id] = false;
        }
        foreach($projects as $project){
            if($project->status == 'PAST' && $array[$project->id] === false)
            {
                foreach($project->attachments as $attachment){
                    $total_size = $total_size + $attachment->file_size;
                        //return $attachment;
                }
                $array[$project->id] = true;
            }
        }

        $personal_past = $total_size;
        $personal_past = $this->byteToGb($personal_past);

        //return personal past

        $total_size = 0;

        foreach($projects as $project){
            $array[$project->id] = false;
        }
        foreach($projects as $project){
            if($project->status == 'DRAFT' && $array[$project->id] === false)
            {
                foreach($project->attachments as $attachment){
                    $total_size = $total_size + $attachment->file_size;
                        //return $attachment;
                }
                $array[$project->id] = true;
            }
        }

        $personal_draft = $total_size;
        $personal_draft = $this->byteToGb($personal_draft);

        //after personal draft

        $total_size = 0;

        foreach($projects as $project){
            $array[$project->id] = false;
        }
        foreach($projects as $project){
            if($project->status == 'DELETED' && $array[$project->id] === false)
            {
                foreach($project->attachments as $attachment){
                    $total_size = $total_size + $attachment->file_size;
                        //return $attachment;
                }
                $array[$project->id] = true;
            }
        }

        $personal_deleted = $total_size;
        $personal_deleted = $this->byteToGb($personal_deleted);
        

        $plan = auth()->user()->plan;
        $plan_storage = null;
        if($plan){
            $plan_storage = $plan->storage;
        }
        $total_used = $personal_docs + $personal_active + $personal_past + $personal_deleted;

        // return $plan_storage;
        return response()->json([
            'total_used' => round($total_used, 6),
            'plan_storage' => $plan_storage,
            'personal_docs' => round($personal_docs, 6),
            'personal_active' => round($personal_active, 6),
            'personal_past' => round($personal_past, 6),
            'personal_deleted' => round($personal_deleted, 6),
            'personal_draft' => round($personal_draft, 6),
        ], 200);

    }

    public function byteToGb($size){
        $res = $size;
        $res = $res / 1024;
        $res = $res / 1024;
        $res = $res / 1024;

        return $res;
    }

    public function getProfile(){
        return new ProfileUserResource(auth()->user());
    }
}
