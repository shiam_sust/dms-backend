<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\ProjectUserRepositoryInterface;
use App\Repositories\Exceptions\ProjectUsers\CollaboratorCreateErrorException;
use App\Http\Requests\ProjectUsers\CreateCollaboratorsRequestValidation;
use Illuminate\Support\Facades\DB;


class ProjectUsersController extends Controller
{
    /**
     * @var ProjectUserRepositoryInterface
     */
    private $projectUserRepo;

    /**
     * @var ProjectUserRepositoryInterface $projectUserRepo
     */
    public function __construct(ProjectUserRepositoryInterface $projectUserRepo)
    {
        $this->projectUserRepo = $projectUserRepo;
    }

    /**
     * @param CreateCollaboratorsRequestValidation $request
     * @throws CollaboratorCreateErrorException
     */
    public function storeCollaborators(CreateCollaboratorsRequestValidation $request)
    {
        DB::beginTransaction();

        try{
            $this->projectUserRepo->storeCollaborators($request);
            DB::commit();
            return response()->json(['message'=>"collaborators added successfully!"], 200);
        } catch(\Exception $e){
            throw new CollaboratorCreateErrorException($e);
        }
    }
}
