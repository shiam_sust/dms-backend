<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Teams\TeamCreateRequestValidation;
use App\Http\Requests\Teams\TeamUpdateRequestValidation;
use App\Http\Resources\Teams\TeamDetailResource;
use App\Http\Resources\Teams\TeamPlansResource;
use App\Http\Resources\Teams\TeamPlanAccountResource;
use App\Http\Resources\Teams\TeamAccountStatusResource;
use App\Http\Resources\Teams\TeamResource;
use App\Repositories\Exceptions\Teams\TeamCreateException;
use App\Repositories\Exceptions\Teams\TeamPlanChangeException;
use App\Repositories\Exceptions\Teams\TeamException;
use App\Repositories\Interfaces\TeamRepositoryInterface;
use App\Services\TeamService;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    /**
     * @var TeamService
     */
    private $teamService;

    /**
     * @var TeamRepositoryInterface
     */
    private $teamRepo;

    /**
     * TeamsController constructor.
     * @param TeamService $teamService
     * @param TeamRepositoryInterface $teamRepo
     */
    public function __construct(TeamService $teamService, TeamRepositoryInterface $teamRepo)
    {
        $this->teamService = $teamService;
        $this->teamRepo = $teamRepo;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return TeamResource::collection($this->teamRepo->index($request));
    }

    /**
     * @param TeamCreateRequestValidation $request
     * @return TeamResource
     * @throws TeamCreateException
     */
    public function store(TeamCreateRequestValidation $request)
    {
        try {
            $team = $this->teamService->store($request);
            return new TeamResource($team);
        }catch (\ErrorException $exception){
            throw new TeamCreateException($exception->getMessage(),500);
        }
    }

    public function upgradeTeamPlan(Request $request)
    {
        try {
            $team = $this->teamService->changePricingPlan($request);
            return new TeamResource($team);
        }catch (\ErrorException $exception){
            throw new TeamPlanChangeException($exception->getMessage(),500);
        }
    }

    /**
     * @param $id
     * @return TeamDetailResource
     * @throws TeamException
     */
    public function show($id)
    {
        try {
            return new TeamDetailResource($this->teamRepo->show($id));
        }catch (\ErrorException $exception){
            throw new TeamException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $teamId
     * @param $memberId
     * @return \Illuminate\Http\JsonResponse
     * @throws TeamException
     */
    public function removeMember(Request $request, $teamId, $memberId)
    {
        try {
            $this->teamService->removeMember($teamId,$memberId);
            return response()->json(['message'=>"You are successfully removed member"], 200);
        }catch (\ErrorException $exception){
            throw new TeamException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param TeamUpdateRequestValidation $request
     * @param $id
     * @return TeamResource
     * @throws TeamCreateException
     * @throws TeamException
     */
    public function update(TeamUpdateRequestValidation $request, $id)
    {
        try {
            $team = $this->teamService->update($request, $id);
            return new TeamResource($team);
        }catch (\ErrorException $exception){
            throw new TeamException($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws TeamException
     */
    public function destroy($id)
    {
        try {
            $team = $this->teamRepo->show($id);
            $this->teamRepo->destroy($team);
            return response()->json(['message'=>"Successfully deleted"], 200);
        }catch (\ErrorException $exception){
            throw new TeamException($exception->getMessage(),$exception->getCode());
        }
    }

    public function getTeamPlans(Request $request)
    {
        try {
            return TeamPlansResource::collection($this->teamRepo->teamsHasPlan());
        }catch (\ErrorException $exception){
            throw new TeamException($exception->getMessage(),$exception->getCode());
        }
    }

    public function getTeamPlansAccount(Request $request)
    {
        try {
            return TeamPlanAccountResource::collection($this->teamRepo->teamsHasPlan());
        }catch (\ErrorException $exception){
            throw new TeamException($exception->getMessage(),$exception->getCode());
        }
    }

    public function getTeamAccountStatus(Request $request)
    {
        try {
            return TeamAccountStatusResource::collection($this->teamRepo->teamsHasPlan());
        }catch (\ErrorException $exception){
            throw new TeamException($exception->getMessage(),$exception->getCode());
        }
    }
}
