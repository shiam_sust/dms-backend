<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Plans\PlanReource;
use App\Http\Resources\Plans\PlanResourceForAccount;
use App\Repositories\Interfaces\PlanRepositoryInterface;
use App\Repositories\Interfaces\StripeRepositoryInterface;
use Illuminate\Http\Request;

class PlansController extends Controller
{
    /**
     * @var PlanRepositoryInterface
     */
    private $planRepo;

    /**
     * @var StripeRepositoryInterface
     */
    private $stripeRepo;

    /**
     * PlansController constructor.
     * @param PlanRepositoryInterface $planRepo
     * @param StripeRepositoryInterface $stripeRepo
     */
    public function __construct(PlanRepositoryInterface $planRepo, StripeRepositoryInterface $stripeRepo)
    {
        $this->planRepo = $planRepo;
        $this->stripeRepo = $stripeRepo;
    }


    /**
     * @param Request $request
     */
    public function index(Request $request)
    {
        return $this->planRepo->index($request);
    }

    public function personalPlan(Request $request)
    {
        $user = auth()->user();
        //$payment_method = auth()->user()->payment_method;

    // $plan_payment = array($plan, $payment_method);
    //return $plan_payment;
    //return $user;
        if($user->plan){
            return new PlanResourceForAccount($user);
        }
        return response()->json(['message'=>"No plan is available"], 404);
    }
    public function getSubscribedPlanInfo(Request $request)
    {
        $user = auth()->user();
        $subscription = $this->stripeRepo->getSubscriptionById($user->subscription_id);
        $invoice = $this->stripeRepo->getUpcomingInvoice($user->stripe_id, $subscription->id);
        $plan = $user->plan;
        $data = [];

        $data['plan_type'] = $plan->plan_type;
        $data['plan_name'] = $plan->plan_name;
        $data['time_period'] = $plan->time_period;
        $data['subtotal'] = $invoice->subtotal/100;
        $data['tax'] = $invoice->tax != null ? $invoice->tax/100 : 0.0;
        $data['total'] = $invoice->total/100;
        $response['data'] = $data;

        return $response;

    }

    public function upgradePersonalPlan(Request $request)
    {
        $user = auth()->user();
        $user->plan_id = $request->plan_id;
        $user->save();
        return response()->json(['message'=>"plan upgraded successfully"], 200);
    }

    public function planChargeInSummary()
    {
        $user = auth()->user();
        $plan = $this->planRepo->show($user->plan_id);
        if($plan){
            return new PlanReource($plan);
        }else{
            return response()->json(['message'=>"No plan is available"], 404);
        }
        // return new ProjectResource($project);
    }

}
