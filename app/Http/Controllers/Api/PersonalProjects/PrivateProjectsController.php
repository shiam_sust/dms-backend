<?php

namespace App\Http\Controllers\Api\PersonalProjects;

use App\Events\ProjectCreatedEvent;
use App\Helper\General\CollectionHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Documents\SaveDecisionRequestValidation;
use App\Http\Requests\Documents\StoreDocumentsRequestValidation;
use App\Http\Requests\Documents\UploadDocumentsRequestValidation;
use App\Http\Requests\Projects\AssignCollaboratorsRequestValidation;
use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\AddCollaboratorsRequestValidation;
use App\Http\Requests\Projects\AddSupportStaffsRequestValidation;
use App\Http\Requests\Projects\ImportCsvRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Http\Resources\Documents\CollaboratorDocumentResource;
use App\Http\Resources\Documents\DocumentResource;
use App\Http\Resources\Projects\ProjectDetailHostResource;
use App\Http\Resources\Projects\ProjectDetailsCollaboratorResource;
use App\Http\Resources\Projects\ProjectDetailsResource;
use App\Http\Resources\Projects\ProjectListResource;
use App\Http\Resources\Projects\ProjectPrimaryInfoResource;
use App\Http\Resources\Projects\ProjectResource;
use App\Repositories\Exceptions\Attachments\AttachmentErrorException;
use App\Repositories\Exceptions\Documents\AssignCollaboratorsException;
use App\Http\Resources\Users\ProjectUserResource;
use App\Repositories\Exceptions\Documents\DocumentNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectCopyFailedException;
use App\Repositories\Exceptions\Projects\ProjectNotFoundException;
use App\Repositories\Exceptions\Projects\UserRemoveException;
use App\Repositories\Exceptions\Users\CreateUserErrorException;
use App\Repositories\Exceptions\Documents\DocumentCreateErrorException;
use App\Repositories\Exceptions\Projects\CreateDeadlineErrorException;
use App\Repositories\Exceptions\Projects\ProjectCreateErrorException;
use App\Repositories\Exceptions\Projects\ProjectDeleteFailedException;
use App\Repositories\Exceptions\Projects\ProjectQueryException;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Repositories\Interfaces\NotificationRepositoryInterface;
use App\Services\ProjectServiceTest;
use App\Utils\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use App\Models\Project;
use \Datetime;

class PrivateProjectsController extends Controller
{

    /**
     * @var ProjectServiceTest
     */
    private $projectService;

    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepo;
    private $notificationRepo;

    /**
     * PrivateProjectsController constructor.
     * @param ProjectServiceTest $projectService
     * @param ProjectRepositoryInterface $projectRepo
     * @param NotificationRepositoryInterface $notificationRepo
     */
    public function __construct(ProjectServiceTest $projectService,
                                ProjectRepositoryInterface $projectRepo,
                                NotificationRepositoryInterface $notificationRepo)
    {
        $this->projectService = $projectService;
        $this->projectRepo = $projectRepo;
        $this->notificationRepo = $notificationRepo;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws ProjectQueryException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function index(Request $request)
    {
        try {
            $perPage = $request->per_page?$request->per_page:20;
            return ProjectListResource::collection(CollectionHelper::paginate($this->projectRepo->index($request), $perPage));
        } catch(\ErrorException $exception) {
            throw new ProjectQueryException($exception->getMessage(), $exception->getCode());
        }

    }

    /**
     * @param CreateProjectRequestValidation $request
     * @return ProjectResource
     * @throws ProjectCreateErrorException
     */
    public function store(CreateProjectRequestValidation $request)
    {
        try {
            $project = $this->projectService->store($request);
            return new ProjectResource($project);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $id
     * @return ProjectPrimaryInfoResource
     */
    public function getPrimaryInfo($id)
    {
        try {
            return new ProjectPrimaryInfoResource($this->projectRepo->getUserSpecificProject($id));
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws ProjectCreateErrorException
     */
    public function getCollaborators($id)
    {
        try {
            $project = $this->projectRepo->show($id);
            return ProjectUserResource::collection($project->collaborators);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws ProjectCreateErrorException
     */
    public function getSupportStaffs($id)
    {
        try {
            $project = $this->projectRepo->show($id);
            return ProjectUserResource::collection($project->support_staffs);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param UpdateProjectRequestValidation $request
     * @param $id
     * @return ProjectResource
     * @throws ProjectCreateErrorException
     */
    public function update(UpdateProjectRequestValidation $request, $id)
    {
        try {
            $project = $this->projectService->update($request,$id);
            return new ProjectResource($project);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    public function setDeadline(CreateProjectDeadlineRequestValidation $request, $project_id){
        try {
            return new ProjectResource($this->projectService->setDeadline($request,$project_id));
        }catch (\ErrorException $exception){
            throw new CreateDeadlineErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->projectService->deleteProject($id);
            return response()->json(['message'=>"Successfully deleted"],200);
        }catch (\ErrorException $exception){
            throw new ProjectDeleteFailedException($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelete($id)
    {
        try {
            $this->projectService->forceDelete($id);
            return response()->json(['message'=>"Successfully deleted"],200);
        }catch (\ErrorException $exception){
            throw new ProjectDeleteFailedException($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @param $id
     * @param AddCollaboratorsRequestValidation $request
     * @return ProjectResource
     * @throws CreateUserErrorException
     */
    public function storeCollaborators($id, AddCollaboratorsRequestValidation $request)
    {
        try {
            $project = $this->projectService->storeCollaboratorOrSupportStaff($request->collaborators, $id, Constant::USER_TYPE_COLLABORATOR);

            $project = $this->projectRepo->show($id);
            foreach ($request->collaborators as $collaborator)
            {
                $this->assignEmail($collaborator['email'], $collaborator['first_name'], 'collaborator', $project->title);
            }

            $this->notificationRepo->assignCollaboratorsNotification($request->collaborators, $project->title);

            return new ProjectResource($project);
        }catch (\ErrorException $exception){
            throw new CreateUserErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $id
     * @param AddSupportStaffsRequestValidation $request
     * @return ProjectResource
     * @throws CreateUserErrorException
     * @throws ProjectCreateErrorException
     */
    public function storeSupportStaffs($id, AddSupportStaffsRequestValidation $request)
    {
        try {
            $project = $this->projectService->storeCollaboratorOrSupportStaff($request->supportStaffs, $id, Constant::USER_TYPE_SUPPORT_STAFF);

            $project = $this->projectRepo->show($id);
            foreach ($request->supportStaffs as $staff)
            {
                $this->assignEmail($staff['email'], $staff['first_name'], 'staff', $project->title);
            }

            $this->notificationRepo->assignSupportStaffsNotification($request->supportStaffs, $project->title);

            return new ProjectResource($project);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    public function assignEmail($email, $name, $to, $title)
    {
        if($to == 'collaborator'){
            $text = 'You have been assigned as a collaborator';
        }else{
            $text = 'You have been assigned as a support staff';
        }
        $details = [
            'title' => 'Mail from Document Management System',
            'name' => $name,
            'email' => $email,
            'text' => $text,
            'projectTitle' => $title
        ];

        dispatch(new \App\Jobs\SendAssignEmailJob($details));

        // Mail::to($email)->send(new SigninMail($details));
    }

    /**
     * @param $projectId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getDocuments($projectId)
    {
        $project = $this->projectRepo->show($projectId);
        return DocumentResource::collection($project->documents_by_position);
    }

    /**
     * @param Request $request
     * @param $project_id
     */
    public function storeDocuments(StoreDocumentsRequestValidation $request, $project_id)
    {
        try {
            $this->projectService->storeDocuments($request, $project_id);
            return response()->json(['message'=>"Documents Requirements Successfully Saved"], 201);
        }catch (\ErrorException $exception){
            throw new DocumentCreateErrorException($exception->getMessage(),$exception->getCode());
        }
    }

    public function assignCollaborators(AssignCollaboratorsRequestValidation $request, $projectId)
    {
        try {
            $this->projectService->assignCollaborator($request);
            return response()->json(['message'=>"Collaborators successfully assigned to documents"], 200);
        }catch (\ErrorException $exception){
            throw new AssignCollaboratorsException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $projectId
     * @param $collaboratorId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws DocumentCreateErrorException
     */
    public function getDocumentsByCollaborator($projectId, $collaboratorId)
    {
        $documents = $this->projectService->getDocumentsByCollaborator($projectId, $collaboratorId);
        return (CollaboratorDocumentResource::collection($documents))->collaboratorId($collaboratorId);
    }

    /**
     * @param Request $request
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     * @throws ProjectCreateErrorException
     */
    public function startProject(Request $request, $project_id)
    {
        try{
            $this->projectService->startProject($project_id);
            return response()->json(['message'=>"Your project has been successfully started!"], 200);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAttachment(Request $request,$project_id, $id)
    {
        try {
            $this->projectService->deleteAttachment($project_id, $id);
            return response()->json(['message'=>"Attachment successfully deleted"], 200);
        }catch (AttachmentErrorException $exception){
            return response()->json(['error'=> $exception->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     * @return ProjectDetailHostResource
     * @throws ProjectNotFoundException
     */
    public function projectDetails(Request $request, $project_id)
    {
        try {
            return new ProjectDetailHostResource($this->projectRepo->getUserSpecificProject($project_id));
        }catch (\ErrorException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     * @return ProjectDetailsCollaboratorResource
     * @throws ProjectNotFoundException
     */
    public function projectDetailWithCollaborator(Request $request, $project_id)
    {
        try {
            return new ProjectDetailsCollaboratorResource($this->projectRepo->getUserSpecificProject($project_id));
        }catch (\ErrorException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }
    }


    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws ProjectQueryException
     */
    public function readCSVData(ImportCsvRequestValidation $request)
    {
        try {
            return $this->projectService->readCSVData($request);
        }catch (\ErrorException $exception){
            throw new ProjectQueryException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     * @return ProjectListResource
     * @throws ProjectCopyFailedException
     */
    public function copyProject(Request $request, $project_id){
        try{
            return new ProjectListResource($this->projectService->copyProjectDetails($project_id));
        }catch (\ErrorException $exception){
            throw new ProjectCopyFailedException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     * @throws UserRemoveException
     */
    public function removeUser(Request $request, $project_id, $user_id)
    {
        try {
            if(!$request->has("user_type")){
                throw new UserRemoveException("User type is required.", 400);
            }
            $this->projectService->removeUser($project_id, $user_id, $request->user_type);
            return response()->json(["message" => "You have been successfully removed user from this project"], 200);
        }catch (\ErrorException $exception){
            throw new UserRemoveException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $projectId
     * @param $document_id
     * @return \Illuminate\Http\JsonResponse
     * @throws DocumentNotFoundException
     */
    public function removeDocument(Request $request, $projectId, $document_id){
        try{
            $this->projectService->removeDocument($document_id);
            return response()->json(["message" => "You have been successfully removed document from this project"], 200);
        } catch (\ErrorException $error){
            throw new DocumentNotFoundException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $projectId
     * @param $document_id
     * @return \Illuminate\Http\JsonResponse
     * @throws DocumentNotFoundException
     */
    public function removeTemplate(Request $request, $projectId, $document_id){
        try {
            $this->projectService->removeTemplate($document_id);
            return response()->json(["message" => "You have been successfully removed template from document"], 200);
        }catch (\ErrorException $exception){
            throw new DocumentNotFoundException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     */
    public function stopProject(Request $request, $project_id){
        try {
            return new ProjectResource($this->projectService->stopProject($project_id));
        }catch (\ErrorException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $project_id
     */
    public function restoreProject(Request $request, $project_id){
        try {
            return new ProjectResource($this->projectService->restoreProject($project_id));
        }catch (\ErrorException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param UploadDocumentsRequestValidation $request
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     * @throws AttachmentErrorException
     * @throws DocumentCreateErrorException
     */
    public function uploadDocuments(UploadDocumentsRequestValidation $request, $project_id)
    {
        try {
            $this->projectService->uploadDocuments($request, $project_id);
            //sending mail
            $project = $this->projectRepo->show($project_id);
            $host = $project->userHost();
            $supportStaffs = $project->support_staffs;
            $this->docUploadEmail($host->email, $host->first_name, $project->title);
            foreach ($supportStaffs as $staff) {{
                $this->docUploadEmail($staff->email, $staff->first_name, $project->title);
            }}
            return response()->json(['message'=>"Documents successfully saved!"], 201);
        }catch (\ErrorException $exception){
            throw new DocumentCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    // public function testDoc(Request $request, $project_id)
    // {
    //     $this->projectService->testingDoc();

    // }

    // public function testingDoc(Request $request, $project_id)
    // {
    //     //$this->projectService->sendMailToHostSupportStaffs($project_id);
    //     $project = $this->projectRepo->show($project_id);
    //     $document = $this->projectService->testingDoc();

    //     $project = Project::where('id', $project_id)->first();
    //     $now = new DateTime();
    //     $current_time = $now->format('Y-m-d H:i:s');

    //     // if($current_time > $project->end_date_first){
    //     //     return "no";
    //     // }else{
    //     //     return "yes";
    //     // }
    //     return $document->pivot->status;
    //     // return $document->pluck['status'];
    // }

    public function docUploadEmail($email, $name, $projectTitle)
    {
        $details = [
            'title' => 'Mail from Document Management System',
            'name' => $name,
            'email' => $email,
            'projectTitle' => $projectTitle
        ];

        dispatch(new \App\Jobs\SendDocUploadEmailJob($details));

        // Mail::to($email)->send(new SigninMail($details));
    }

    /**
     * @param SaveDecisionRequestValidation $request
     * @param $project_id
     * @throws DocumentCreateErrorException
     */
    public function saveDecision(SaveDecisionRequestValidation $request, $project_id)
    {
        try {
            $this->projectService->saveDecision($request);
            return response()->json(['message'=>"Documents decisions successfully saved!"], 200);
        }catch (\ErrorException $exception){
            throw new DocumentCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    public function downloadDocuments(Request $request, $project_id, $user_id)
    {
        try {
            return $this->projectService->downloadDocuments($project_id, $user_id);
        }catch (\ErrorException $exception) {
            throw new DocumentNotFoundException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $projectId
     * @param $collaboratorId
     * @return \Illuminate\Http\JsonResponse
     * @throws DocumentCreateErrorException
     */
    public function sendDecisionToCollaborators($projectId, $collaboratorId)
    {
        $this->projectService->sendDecisionToCollaborators($projectId, $collaboratorId);
        return response()->json(['message'=>"Decisions is sent to the collaborator email"], 200);
    }
}
