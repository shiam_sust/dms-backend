<?php

namespace App\Http\Resources\Users;

use App\Http\Resources\Documents\DocumentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'organization' => $this->organization,
            'user_type' => $this->pivot->user_type,
            'scopes' => $this->pivot->scopes,
            'position' => $this->pivot->position
        ];
    }
}
