<?php

namespace App\Http\Resources\Users;

use App\Http\Resources\Documents\DocumentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserDocumentResource extends JsonResource
{
    protected $documentIds;

    public function documentIds($value){
        $this->documentIds = $value;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'user_type' => $this->pivot->user_type,
            'scopes' => $this->pivot->scopes ? json_decode($this->pivot->scopes):'',
            'total_assigned_documents' => $this->documents()->whereIn('document_id', $this->documentIds)->count(),
            'total_submitted_documents' => $this->documents()
                ->whereIn('document_id', $this->documentIds)
                ->whereIn('document_user.status',["SUBMITTED","APPROVED", "REJECTED"])
                ->count(),
            'total_approved_documents' => $this->documents()
                ->whereIn('document_id', $this->documentIds)
                ->where('document_user.status',["APPROVED"])
                ->count(),
            'documents' => [],
//            'documents' => DocumentResource::collection($this->documents()->whereIn('document_id', $this->documentIds)->get())
        ];
    }
}
