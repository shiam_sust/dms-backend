<?php


namespace App\Http\Resources\Users;


use Illuminate\Http\Resources\Json\ResourceCollection;

class UserDocumentResourceCollection extends ResourceCollection
{
    protected $documentIds;

    public function documentIds($value){
        $this->documentIds = $value;
        return $this;
    }

    public function toArray($request){
        return $this->collection->map(function(UserDocumentResource $resource) use($request){
            return $resource->documentIds($this->documentIds)->toArray($request);
        })->all();
    }
}
