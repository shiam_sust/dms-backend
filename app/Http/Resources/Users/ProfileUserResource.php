<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'address' => $this->address,
            'city' => $this->city,
            'state' => $this->state,
            'email' => $this->email,
            'country' => $this->country,
            'zip_code' => $this->zip_code,
            'payment_method_id' => $this->payment_method_id,
            'profile_picture' => $this->profile_picture,
            'profile_picture_link' => ($this->profile_picture)?asset('storage/uploads/profile_picture_'.$this->id.'/'.$this->profile_picture):'',
        ];
    }
}
