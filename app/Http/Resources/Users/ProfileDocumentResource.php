<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileDocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $file = str_split($this->attachment);
        $count = 0;
        $pdf = '';
        foreach($file as $char)
        {
            //return $char;
            if($char == '_'){
                ++$count;
                if($count == 2){
                    continue;
                }
            }
            if($count >= 2){
                $pdf .= $char;
            }
        }

        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'name' => $this->name,
            'attachment' => $pdf,
            'file_size' => $this->file_size,
            'file_link' => ($this->attachment)?asset('storage/uploads/profile_'.$this->user_id.'/'.$this->attachment):'',
        ];
    }
}
