<?php

namespace App\Http\Resources\Projects;

use App\Http\Resources\Documents\DocumentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectDetailsCollaboratorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = array();
        $data['id'] = $this->id;
        $data['title'] = $this->title;
        $data['summary'] = $this->project_summary;
        $documents = $this->documents()->whereHas('users', function ($q) {
            $q->where("users.id", auth()->id());
        })->get();

        $data['documents'] = DocumentResource::collection($documents);

        return $data;
    }
}
