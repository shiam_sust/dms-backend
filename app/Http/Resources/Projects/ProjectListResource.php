<?php

namespace App\Http\Resources\Projects;

use App\Http\Resources\Users\DeletedUserResource;
use App\Http\Resources\Users\ProjectUserResource;
use App\Models\Document;
use App\Models\User;
use App\Utils\Constant;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $documentIds = $this->documents->pluck('id');
//        $result = Document::whereHas('users', function($q) use($documentIds) {
//            $q->whereIn('document_id', $documentIds)
//                ->where('status', "SUBMITTED");
//        })->count();

        $data = [
            'id' => $this->id,
            'team_id' => $this->team_id,
            'title' => $this->title,
            'project_type' => $this->project_type,
            'start_date' => $this->start_date?date('j M, Y', strtotime($this->start_date)):NULL,
            'end_date_first' => $this->end_date_first?date('j M, Y', strtotime($this->end_date_first)):NULL,
            'end_date_second' => $this->end_date_second?date('j M, Y', strtotime($this->end_date_second)):NULL,
            'total_collaborators' => $this->total_collaborators,
            'total_support_stuff' => $this->total_support_staff,
            'project_summary' => $this->project_summary,
            'reminder_lw_first' => json_decode($this->reminder_lw_first),
            'reminder_lw_second' => json_decode($this->reminder_lw_second),
            'reminder_before_lw_first' => json_decode($this->reminder_before_lw_first),
            'reminder_before_lw_second' => json_decode($this->reminder_before_lw_second),
            'status' => $this->status,
            'is_draft' => $this->is_draft ? true : false,
            'host_reminder_email' => $this->host_reminder_email,
            'initiated_by' => $this->created_user?$this->created_user->first_name.' '.$this->created_user->last_name:'',
            'initial_date' => $this->created_at?date('j M, Y', strtotime($this->created_at)):NULL,

            'users' => ProjectUserResource::collection($this->users),


            $this->mergeWhen($this->status === Constant::PROJECT_STATUS_DRAFT, [
                'total_required_document' => count($this->documents),
            ]),

            $this->mergeWhen($this->status === Constant::PROJECT_STATUS_DELETED, [
                'deleted_at' => $this->deleted_at?date('j M, Y', strtotime($this->deleted_at)):NULL,
                'deleted_by_user' => $this->deleted_by ? new DeletedUserResource($this->deleted_by_user) : null,
                'deleted_from' => $this->deleted_from,
                'days_left' => $this->calculateDaysLeft($this->deleted_at)
            ]),

        ];

        if($this->status === Constant::PROJECT_STATUS_ACTIVE || $this->status === Constant::PROJECT_STATUS_PAST) {
            $documents = Document::whereHas('users', function($q) use($documentIds) {
                $q->whereIn('document_id', $documentIds);
            })->select('id')->get();

            $collectedDocs = 0;
            $approvedDocs = 0;
            foreach ($documents as $document) {
                $collectedDocs += $document->users()->wherePivotIn('status', [Constant::DOCUMENT_STATUS_APPROVED, Constant::DOCUMENT_STATUS_SUBMITTED, Constant::DOCUMENT_STATUS_REJECTED])->count();
                $approvedDocs += $document->users()->wherePivot('status', Constant::DOCUMENT_STATUS_APPROVED)->count();
            }

            $data['collected_documents'] = $collectedDocs;
            $data['approved_documents'] = $approvedDocs;
        }

        $myRoles = [];
        $users = $this->users()->wherePivot('user_id', auth()->id())->get();
        foreach ($users as $user) {
            array_push($myRoles, ['user_type'=>$user->pivot->user_type, 'scopes'=>$user->pivot->scopes]);
        }
        $data['my_roles'] = $myRoles;

        return $data;
    }

    public function calculateDaysLeft($deletedAt)
    {
        $now = Carbon::now();
        $deletedTimeAfterSevenDays = Carbon::parse($deletedAt)->addDays(7);
        return $deletedTimeAfterSevenDays->diffInDays($now);
    }
}
