<?php

namespace App\Http\Resources\Projects;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'pid' => $this->pid,
            'team_id' => $this->team_id,
            'title' => $this->title,
            'project_type' => $this->project_type,
            'start_date' => $this->start_date?date('j M, Y', strtotime($this->start_date)):NULL,
            'end_date_first' => $this->end_date_first?date('j M, Y', strtotime($this->end_date_first)):NULL,
            'end_date_second' => $this->end_date_second?date('j M, Y', strtotime($this->end_date_second)):NULL,
            'total_collaborators' => $this->total_collaborators,
            'total_support_stuff' => $this->total_support_staff,
            'project_summary' => $this->project_summary,
            'reminder_lw_first' => json_decode($this->reminder_lw_first),
            'reminder_lw_second' => json_decode($this->reminder_lw_second),
            'reminder_before_lw_first' => json_decode($this->reminder_before_lw_first),
            'reminder_before_lw_second' => json_decode($this->reminder_before_lw_second),
            'status' => $this->status,
            'is_draft' => $this->is_draft ? true : false,
            'host_reminder_email' => $this->host_reminder_email,
        ];
    }
}
