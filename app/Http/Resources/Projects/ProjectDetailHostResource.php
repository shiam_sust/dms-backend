<?php

namespace App\Http\Resources\Projects;

use App\Http\Resources\Users\UserDocumentResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectDetailHostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = array();
        $data['id'] = $this->id;
        $data['title'] = $this->title;
        $data['summary'] = $this->project_summary;
        $data["collaborators"] = (new UserDocumentResourceCollection($this->collaborators))->documentIds($this->documents->pluck('id'));

        return $data;
    }
}
