<?php

namespace App\Http\Resources\Projects;

use App\Http\Resources\Documents\DocumentResource;
use App\Http\Resources\Users\ProjectUserResource;
use App\Utils\Constant;
use Illuminate\Http\Resources\Json\JsonResource;

class PublicProjectDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'pid' => $this->pid,
            'title' => $this->title,
            'project_type' => $this->project_type,
            'start_date' => $this->start_date?date('j M, Y', strtotime($this->start_date)):NULL,
            'end_date_first' => $this->end_date_first?date('j M, Y', strtotime($this->end_date_first)):NULL,
            'end_date_second' => $this->end_date_second?date('j M, Y', strtotime($this->end_date_second)):NULL,
            'project_summary' => $this->project_summary,
            'project_owner' => new ProjectUserResource($this->users()->wherePivot("user_type", Constant::USER_TYPE_HOST)->first()),
            'documents' => DocumentResource::collection($this->documents)
        ];
    }
}
