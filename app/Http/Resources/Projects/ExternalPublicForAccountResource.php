<?php

namespace App\Http\Resources\Projects;

use Illuminate\Http\Resources\Json\JsonResource;

class ExternalPublicForAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $active_projects = $this->projects->where('project_type', 'PUBLIC')->where('status', 'ACTIVE')->count();
        $pricing_plan = $this->default_personal_plan->plan_name;
        $past_projects = $this->projects->where('project_type', 'PUBLIC')->where('status', 'PAST')->count();
        $deleted_projects = $this->projects->where('project_type', 'PUBLIC')->where('status', 'DELETED')->count();

        return [
            'active_projects' => $active_projects,
            'past_projects' => $past_projects,
            'deleted_projects' => $deleted_projects,
            'pricing_plan' => $pricing_plan
        ];
    }
}
