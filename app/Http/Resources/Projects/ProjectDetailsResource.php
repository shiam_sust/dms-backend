<?php

namespace App\Http\Resources\Projects;

use App\Http\Resources\Documents\DocumentResource;
use App\Http\Resources\Users\ProjectUserResource;
use App\Http\Resources\Users\UserDocumentResource;
use App\Http\Resources\Users\UserDocumentResourceCollection;
use App\Utils\Constant;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $host_support_staff = $this->users()
            ->wherePivot("user_id", auth()->id())
            ->wherePivotIn("user_type",[Constant::USER_TYPE_HOST, Constant::USER_TYPE_SUPPORT_STAFF])
            ->exists();


        $data = array();
        $data['id'] = $this->id;
        $data['title'] = $this->title;
        $data['summary'] = $this->project_summary;

        if($host_support_staff){
            $data["collaborators"] = (new UserDocumentResourceCollection($this->collaborators))->documentIds($this->documents->pluck('id'));
            //$data["collaborators"] = UserDocumentResource::collection($this->collaborators);
        }else{
            $documents = $this->documents()->whereHas('users', function ($q) {
                $q->where("users.id", auth()->id());
            })->get();

            $data['documents'] = DocumentResource::collection($documents);
        }

        return $data;
    }
}
