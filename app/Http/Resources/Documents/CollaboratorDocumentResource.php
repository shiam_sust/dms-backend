<?php

namespace App\Http\Resources\Documents;

use App\Http\Resources\Attachments\DocumentAttachmentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CollaboratorDocumentResource extends JsonResource
{
    protected $collaboratorId;

    public function collaboratorId($value){
        $this->collaboratorId = $value;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'title' => $this->title,
            'template' => $this->template,
            'status' => $this->status,
//            'note' => $this->latest_note?$this->latest_note->note:'',
            'max_size' => $this->max_size,
            'size_unit' => $this->size_unit,
            'formats' => $this->formats,
            'position' => $this->position,
            'template_url' =>($this->template)?asset('storage/uploads/project_'.$this->project_id.'/templates/'.$this->template):'',
            'attachment' => (new DocumentAttachmentResource($this->latest_attachment))->projectId($this->project_id)
        ];

        $docUser = $this->users()->wherePivot('user_id', $this->collaboratorId)->first();
        if($docUser) {
            $data['status'] = $docUser->pivot->status;
        }

        $notes = $this->notes()->where('user_id', $this->collaboratorId)->orderBy('id', 'desc')->first();
        if($notes) {
            $data['note'] = $notes->note;
        }

        return $data;
    }

    public static function collection($resource){
        return new CollaboratorDocumentResourceCollection($resource);
    }
}
