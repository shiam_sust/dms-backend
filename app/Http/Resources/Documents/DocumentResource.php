<?php

namespace App\Http\Resources\Documents;

use App\Http\Resources\Attachments\DocumentAttachmentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'template' => $this->template,
            'status' => $this->status,
            'note' => $this->latest_note?$this->latest_note->note:'',
            'max_size' => $this->max_size,
            'size_unit' => $this->size_unit,
            'formats' => $this->formats,
            'position' => $this->position,
            'collaborators_ids' => $this->collaborators->pluck('id'),
            'template_url' =>($this->template)?asset('storage/uploads/project_'.$this->project_id.'/templates/'.$this->template):'',
            'attachment' => (new DocumentAttachmentResource($this->latest_attachment))->projectId($this->project_id)
        ];
    }
}
