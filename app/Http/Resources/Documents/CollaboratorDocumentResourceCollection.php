<?php

namespace App\Http\Resources\Documents;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CollaboratorDocumentResourceCollection extends ResourceCollection
{
    protected $collaboratorId;

    public function collaboratorId($value){
        $this->collaboratorId = $value;
        return $this;
    }

    public function toArray($request){
        return $this->collection->map(function(CollaboratorDocumentResource $resource) use($request){
            return $resource->collaboratorId($this->collaboratorId)->toArray($request);
        })->all();
    }
}
