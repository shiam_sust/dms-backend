<?php

namespace App\Http\Resources\Attachments;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentAttachmentResource extends JsonResource
{
    protected $projectId;

    public function projectId($value){
        $this->projectId = $value;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'attachment' => $this->attachment,
            'project_id' => $this->projectId,
            'attachment_url' => ($this->attachment)?asset('storage/uploads/project_'.$this->projectId.'/documents/'.$this->attachment):'',
            'file_type' => $this->file_type
        ];
    }

    public static function collection($resource){
        return new DocumentAttachmentResourceCollection($resource);
    }

    //$file->storeAs('uploads/project_'.$project->id.'/attachments', $fileName, 'public');

}
