<?php
namespace App\Http\Resources\Attachments;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DocumentAttachmentResourceCollection extends ResourceCollection{

    protected $projectId;

    public function projectId($value){
        $this->projectId = $value;
        return $this;
    }

    public function toArray($request){
        return $this->collection->map(function(DocumentAttachmentResource $resource) use($request){
            return $resource->projectId($this->projectId)->toArray($request);
        })->all();

// or use HigherOrderCollectionProxy
// return $this->collection->each->foo($this->foo)->map->toArray($request)->all()

// or simple
// $this->collection->each->foo($this->foo);
// return parent::toArray($request);
    }
}
