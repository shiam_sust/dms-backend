<?php

namespace App\Http\Resources\PaymentMethods;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentMethodsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "nickname" => $this->nickname,
            "last_four_digit" => $this->number,
            "type" => $this->type,
            "brand" => $this->brand,
            "card_holder_name" => $this->billing_name,
            "is_for_personal" => $this->used_user ? true : false,
            "is_for_team" => $this->used_team ? true : false,
        ];
    }
}
