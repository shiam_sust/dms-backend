<?php

namespace App\Http\Resources\Plans;

use Illuminate\Http\Resources\Json\JsonResource;

class PlanReource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'plan_type' => $this->plan_type,
            'plan_name' => $this->plan_name,
            'time_period' => $this->time_period,
            'plan_price' => $this->plan_price,
            'max_active_project' => $this->max_active_project,
            'storage' => $this->storage,
            'max_staff' => $this->max_staff,
            'total_charge' => $this->plan_price,
        ];
    }
}
