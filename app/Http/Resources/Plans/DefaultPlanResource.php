<?php

namespace App\Http\Resources\Plans;

use Illuminate\Http\Resources\Json\JsonResource;

class DefaultPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'team_plan' => new PlanReource($this->default_team_plan),
            'personal_plan' => new PlanReource($this->default_personal_plan),
        ];
    }
}
