<?php

namespace App\Http\Resources\Plans;

use Illuminate\Http\Resources\Json\JsonResource;

class PlanResourceForAccount extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $total_projects = $this->projects->count();
        $active_projects = $this->projects->where('team_id', null)->where('status', 'ACTIVE')->count();
        $past_projects = $this->projects->where('team_id', null)->where('status', 'PAST')->count();
        $draft_projects = $this->projects->where('team_id', null)->where('status', 'DRAFT')->count();
        $deleted_projects = $this->projects->where('team_id', null)->where('status', 'DELETED')->count();

        return [
            'user_id' => $this->id,
            'plan_id' => $this->plan->id,
            'plan_type' => $this->plan->plan_type,
            'plan_name' => $this->plan->plan_name,
            'nickname' => $this->payment_method->nickname,
            'plan_price' => $this->plan->plan_price,
            'time_period' => $this->plan->time_period,
            'max_active_project' => $this->plan->max_active_project,
            'storage' => $this->plan->storage,
            'max_staff' => $this->plan->max_staff,
            'active_projects' => $active_projects,
            'past_projects' => $past_projects,
            'draft_projects' => $draft_projects,
            'deleted_projects' => $deleted_projects,
            'total_projects' => $total_projects
        ];
    }
}
