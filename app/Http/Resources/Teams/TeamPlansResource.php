<?php

namespace App\Http\Resources\Teams;

use App\Http\Resources\Plans\PlanReource;
use App\Models\Project;
use App\Utils\Constant;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamPlansResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $active = Project::where("team_id", $this->id)
            ->where("status", Constant::PROJECT_STATUS_ACTIVE)
            ->exists();

        $past = Project::where("team_id", $this->id)
            ->where("status", Constant::PROJECT_STATUS_PAST)
            ->exists();

        return [
            "team_id" =>  $this->id,
            "name" => $this->name,
            "members" => $this->number_of_members,
            "plan_name" => $this->plan->plan_name,
            "time_period" => $this->plan->time_period,
            "card_nickname" => $this->paymentMethod->nickname,
            "payment_method_id" => $this->paymentMethod->id,
            "active_projects" => $active ? "YES" : "NO",
            "past_projects" => $past ? "YES" : "NO",
            "start_date" => $this->created_at?date('j M, Y', strtotime($this->created_at)):NULL,
            "lead" => $this->teamLeader->first_name,
        ];

    }
}
