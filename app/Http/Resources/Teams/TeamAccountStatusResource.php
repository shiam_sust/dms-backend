<?php

namespace App\Http\Resources\Teams;

use App\Models\Project;
use App\Utils\Constant;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamAccountStatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $active = Project::where("team_id", $this->id)
            ->where("status", Constant::PROJECT_STATUS_ACTIVE)
            ->get();

        $team_active = $this->totalDocSize($active, 'ACTIVE');
        $team_active = $this->byteToGb($team_active);

        $past = Project::where("team_id", $this->id)
            ->where("status", Constant::PROJECT_STATUS_PAST)
            ->get();

        $team_past = $this->totalDocSize($past, 'PAST');
        $team_past = $this->byteToGb($team_past);

        $deleted = Project::where("team_id", $this->id)
            ->where("status", Constant::PROJECT_STATUS_DELETED)
            ->get();

        $team_deleted = $this->totalDocSize($deleted, 'DELETED');
        $team_deleted = $this->byteToGb($team_deleted);

        $total_used = $team_active + $team_past + $team_deleted;
        $total_used = $this->byteToGb($total_used);

        return [
            "id" =>  $this->id,
            "name" => $this->name,
            "total_used" => $total_used,
            "plan_storage" => $this->plan->storage,
            "team_active" => $team_active,
            "team_past" => $team_past,
            "team_deleted" => $team_deleted,
        ];

    }

    public function totalDocSize($projects, $status){
        $total_size = 0;
        $array = array();
        
        foreach($projects as $project){
            $array[$project->id] = false;
        }
        foreach($projects as $project){
            if($project->status == $status && $array[$project->id] === false)
            {
                foreach($project->attachments as $attachment){
                    $total_size = $total_size + $attachment->file_size;
                        //return $attachment;
                }
                $array[$project->id] = true;
            }
        }
        return $total_size;
    }

    public function byteToGb($size){
        $res = $size;
        $res = $res / 1024;
        $res = $res / 1024;
        $res = $res / 1024;
        return $res;
    }
}
