<?php

namespace App\Http\Resources\Teams;

use App\Http\Resources\Plans\PlanReource;
use App\Models\Project;
use App\Utils\Constant;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamPlanAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $active = Project::where("team_id", $this->id)
            ->where("status", Constant::PROJECT_STATUS_ACTIVE)
            ->count();

        $past = Project::where("team_id", $this->id)
            ->where("status", Constant::PROJECT_STATUS_PAST)
            ->count();

        $draft = Project::where("team_id", $this->id)
        ->where("status", Constant::PROJECT_STATUS_DRAFT)
        ->count();

        $deleted = Project::where("team_id", $this->id)
        ->where("status", Constant::PROJECT_STATUS_DELETED)
        ->count();
        
        $total = Project::where("team_id", $this->id)->count();

        return [
            "id" =>  $this->id,
            "name" => $this->name,
            "members" => $this->number_of_members,
            "plan_name" => $this->plan->plan_name,
            "card_nickname" => $this->paymentMethod->nickname,
            "active_projects" => $active,
            "past_projects" => $past,
            "draft_projects" => $draft,
            "deleted_projects" => $deleted,
            "total_projects" => $total,
            "start_date" => $this->created_at?date('j M, Y', strtotime($this->created_at)):NULL,
            "lead" => $this->teamLeader->first_name,
        ];

    }
}
