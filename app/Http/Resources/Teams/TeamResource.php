<?php

namespace App\Http\Resources\Teams;

use App\Http\Resources\Users\TeamUserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'details' => $this->details,
            'profile_icon' => $this->profile_icon,
            'profile_icon_url' =>($this->profile_icon)?asset('storage/uploads/teams_'.$this->id.'/'.$this->profile_icon):'',
        ];
    }
}
