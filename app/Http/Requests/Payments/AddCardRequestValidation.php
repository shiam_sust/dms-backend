<?php

namespace App\Http\Requests\Payments;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AddCardRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $validator = [
            'is_same' => 'required',
            'billing_info' => 'required|array',
            'billing_info.name' => 'required',
            'billing_info.line1' => 'required',
            'billing_info.city' => 'required',
            'billing_info.state' => 'required',
            'billing_info.country' => 'required',
            'billing_info.zip_code' => 'required',
            'card_info' => 'required',
            'card_info.nickname' => 'required',
            'card_info.number' => 'required',
            'card_info.expire_date' => 'required',
            'card_info.cvv' => 'required',
        ];

        if(!$request->is_same){
            $validator['mailing_info'] = 'required|array';
            $validator['mailing_info.line1'] = 'required';
            $validator['mailing_info.city'] = 'required';
            $validator['mailing_info.state'] = 'required';
            $validator['mailing_info.country'] = 'required';
            $validator['mailing_info.zip_code'] = 'required';
        }
        return $validator;
    }
}
