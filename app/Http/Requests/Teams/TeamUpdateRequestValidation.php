<?php

namespace App\Http\Requests\Teams;

use Illuminate\Foundation\Http\FormRequest;

class TeamUpdateRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'details' => 'required',
            'profile_icon' => 'mimes:jpeg,jpg,png|max:1024',
            'members.*.email' => 'required',
            'members.*.first_name' => 'required',
            'members.*.last_name' => 'required',
            'members.*.organization' => 'required',
            'members.*.position' => 'required',
        ];
    }
}
