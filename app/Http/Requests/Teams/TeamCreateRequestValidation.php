<?php

namespace App\Http\Requests\Teams;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class TeamCreateRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @param Request $request
     * @return string[]
     */
    public function rules(Request $request)
    {
        $validator = [
            'name' => 'required|unique:teams,name',
            'details' => 'required',
            'profile_icon' => 'required|mimes:jpeg,jpg,png|max:1024',
            'members.*.email' => 'required',
            'members.*.first_name' => 'required',
            'members.*.last_name' => 'required',
            'members.*.organization' => 'required',
            'members.*.position' => 'required',

            'is_same' => 'required',
            'plan_id' => 'required',
            'billing_info' => 'required',
            'billing_info.name' => 'required',
            'billing_info.line1' => 'required',
            'billing_info.line2' => 'required',
            'billing_info.city' => 'required',
            'billing_info.state' => 'required',
            'billing_info.country' => 'required',
            'billing_info.zip_code' => 'required',
            'card_info' => 'required',
            'card_info.nickname' => 'required',
            'card_info.number' => 'required',
            'card_info.expire_date' => 'required',
            'card_info.cvv' => 'required',
        ];

        if(!$request->is_same){
            $validator['mailing_info'] = 'required';
            $validator['mailing_info.line1'] = 'required';
            $validator['mailing_info.line2'] = 'required';
            $validator['mailing_info.city'] = 'required';
            $validator['mailing_info.state'] = 'required';
            $validator['mailing_info.country'] = 'required';
            $validator['mailing_info.zip_code'] = 'required';
        }

        return $validator;
    }
}
