<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

class CreateProjectDeadlineRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "end_date_first" => "required|date|after:".now(),
            "end_date_second" => "required|date|after:end_date_first",
            "reminder_before_lw_first" => "array|required",
            "reminder_before_lw_first.*" => "required|distinct|integer|min:1|max:7",
            "reminder_lw_first" => "array|required",
            "reminder_lw_first.*" => "required|distinct|integer|min:1|max:7",
            "reminder_before_lw_second" => "array|required",
            "reminder_before_lw_second.*" => "required|distinct|integer|min:1|max:7",
            "reminder_lw_second" => "array|required",
            "reminder_lw_second.*" => "required|distinct|integer|min:1|max:7",
        ];
    }
}
