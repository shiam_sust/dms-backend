<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

class AddCollaboratorsRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'collaborators' => 'required|array|min:1',
            'collaborators.*.email' => 'required|email|distinct',
            'collaborators.*.first_name' => 'required|min:3',
            'collaborators.*.last_name' => 'required|min:3',
            'collaborators.*.organization' => 'required|min:3',
            'collaborators.*.position' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'collaborators.*.email' => 'email',
            'collaborators.*.first_name' => 'first name',
            'collaborators.*.last_name' => 'last name',
            'collaborators.*.organization' => 'organization',
        ];
    }

}
