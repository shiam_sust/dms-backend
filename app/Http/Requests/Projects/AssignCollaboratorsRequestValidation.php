<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

class AssignCollaboratorsRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "data" => 'required|array',
            "data.*.document_id" => 'required|integer|min:1|exists:documents,id',
            "data.*.collaborators_ids" => 'required|array|distinct',
            "data.*.collaborators_ids.*" => 'required|integer|min:1|exists:users,id'
        ];
    }

    public function messages()
    {
        return [
            'data.*.collaborators_ids.required' => 'Collaborators are not selected.',
            'data.*.collaborators_ids.*.required' => 'Collaborators are not selected.'
        ];
    }
}
