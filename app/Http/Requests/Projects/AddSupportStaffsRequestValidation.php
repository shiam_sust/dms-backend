<?php

namespace App\Http\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

class AddSupportStaffsRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'supportStaffs' => 'required|array|min:1',
            'supportStaffs.*.email' => 'required|email|distinct',
            'supportStaffs.*.first_name' => 'required|min:3',
            'supportStaffs.*.last_name' => 'required|min:3',
            'supportStaffs.*.organization' => 'required|min:3',
            'supportStaffs.*.scopes' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'supportStaffs.*.email' => 'email',
            'supportStaffs.*.first_name' => 'first name',
            'supportStaffs.*.last_name' => 'last name',
            'supportStaffs.*.organization' => 'organization',
            'supportStaffs.*.scopes' => 'access',
        ];
    }


}
