<?php

namespace App\Http\Requests\Documents;

use Illuminate\Foundation\Http\FormRequest;

class SaveDecisionRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'documents' => 'required|array|min:1',
            'id' => 'required',
            'status' => 'required',
            'note' => 'required',
            'user_id' => 'required'
        ];
    }
}
