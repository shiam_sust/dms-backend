<?php

namespace App\Http\Requests\Documents;

use Illuminate\Foundation\Http\FormRequest;

class StoreDocumentsRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documents' => 'required|array|min:1',
            'documents.*.title' => 'required',
            // 'documents.*.max_size' => 'required|integer',
            'documents.*.size_unit' => 'required|in:MB,KB,GB',
            // 'documents.*.formats' => 'required',
            'documents.*.position' => 'required|integer',
//            'documents.*.template' => 'file',
        ];
    }

    public function attributes()
    {
        return [
            'documents.*.title' => 'title',
            'documents.*.size_unit' => 'size unit'
        ];
    }
}
