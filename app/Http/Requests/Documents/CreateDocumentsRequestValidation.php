<?php

namespace App\Http\Requests\Documents;

use Illuminate\Foundation\Http\FormRequest;

class CreateDocumentsRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required|integer',
            'instruction' => 'required',
            'documents' => 'required|array|min:1',
            'documents.*instruction' => 'required|min:5',
            'documents.*title' => 'required|min:3',
            'documents.*deadline' => 'required',
            'documents.*status' => 'required',
        ];
    }
}
