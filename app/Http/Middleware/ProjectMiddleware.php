<?php

namespace App\Http\Middleware;

use App\Models\Project;
use App\Repositories\Exceptions\Projects\ProjectOwnerException;
use App\Utils\Constant;
use Closure;
use Illuminate\Http\Request;

class ProjectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if($request->id){
                $project = Project::findOrFail($request->id);
                $result =  $project->users()
                    ->wherePivot("user_id", auth()->id())
                    ->wherePivot("user_type", Constant::USER_TYPE_HOST)
                    ->exists();
                if(!$result){
                    throw new ProjectOwnerException("Permission denied", 403);
                }
            }
            return $next($request);
        }catch (\ErrorException $exception){
            throw new ProjectOwnerException("Permission denied", 500);
        }
    }
}
