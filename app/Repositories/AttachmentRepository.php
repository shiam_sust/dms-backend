<?php


namespace App\Repositories;


use App\Http\Requests\Documents\UploadDocumentsRequestValidation;
use App\Models\Attachment;
use App\Models\Project;
use App\Models\User;
use App\Repositories\Exceptions\Attachments\AttachmentCreateException;
use App\Repositories\Exceptions\Attachments\AttachmentErrorException;
use App\Repositories\Interfaces\AttachmentRepositoryInterface;
use App\Utils\Constant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AttachmentRepository implements AttachmentRepositoryInterface
{

    /**
     * @var Attachment
     */
    private $attachmentModel;

    /**
     * AttachmentRepository constructor.
     * @param Attachment $attachmentModel
     */
    public function __construct(Attachment $attachmentModel)
    {
        $this->attachmentModel = $attachmentModel;
    }


    /**
     * @param Request $request
     * @param Project $project
     * @throws AttachmentCreateException
     */
    public function storePrivateProjectAttachments(Request $request, Project $project): void
    {
        try{
            if($request->hasfile('related_files'))
            {
                foreach($request->file('related_files') as $key => $file)
                {
                    $fileName = auth()->id().'_'.time().'_'.str_replace(' ', '_',$file->getClientOriginalName());
                    $this->attachmentModel = new Attachment();
//                    $this->attachmentModel->organization_id = 1;
                    $this->attachmentModel->user_id = auth()->id();
                    $this->attachmentModel->attachment = $fileName;
                    $this->attachmentModel->attachmentable_id = $project->id;
                    $this->attachmentModel->attachmentable_type = Constant::MODEL_PROJECT;
                    $this->attachmentModel->file_type = $file->getClientOriginalExtension();;
                    $this->attachmentModel->file_size = $file->getSize();
                    $this->attachmentModel->created_by = auth()->id();
                    $this->attachmentModel->save();
                    $file->storeAs('uploads/project_'.$project->id.'/attachments', $fileName, 'public');
                }
            }
        }catch (QueryException $exception){
            throw new AttachmentCreateException($exception->getMessage());
        }
    }

    public function show($id): Attachment
    {
        try{
            return $this->attachmentModel->findOrFail($id);
        }catch (ModelNotFoundException $error){
            throw new AttachmentErrorException("Attachment not found",400);
        }
    }

    public function delete(Attachment $attachment, $project_id): bool
    {
        try{
            $attachment->delete();
            if($attachment->attachmentable_type == Constant::MODEL_PROJECT){
                if(Storage::exists("public/uploads/project_".$project_id."/attachments/".$attachment->attachment)) {
                    Storage::delete("public/uploads/project_".$project_id."/attachments/".$attachment->attachment);
                    return true;
                }
            }
            return false;

        }catch (ModelNotFoundException $exception){
            throw new AttachmentErrorException($exception->getMessage(), 400);
        }
    }

    /**
     * @param $request_document
     * @param $project_id
     * @throws AttachmentCreateException
     */
    public function storeDocumentAttachments($request_document, $project_id): void
    {
        try{
            $attachment = $this->attachmentModel
                ->where('user_id', auth()->id())
                ->where('attachmentable_id', $request_document['id'])
                ->first();

            $fileName = auth()->id().'_'.time().'_'.str_replace(' ', '_',$request_document['file']->getClientOriginalName());
            $fileSize = $request_document['file']->getSize();
            $fileType = $request_document['file']->getClientOriginalExtension();

            if($attachment){
                if(Storage::exists("public/uploads/project_".$project_id."/documents/".$attachment->attachment)) {
                    Storage::delete("public/uploads/project_".$project_id."/documents/".$attachment->attachment);
                }
                $attachment->attachment = $fileName;
                $attachment->modified_by = auth()->id();
                $attachment->file_type = $fileType;
                $attachment->file_size = $fileSize;
                $attachment->save();

            }else{
                $this->attachmentModel =  new Attachment();
                $this->attachmentModel->user_id = auth()->id();
                $this->attachmentModel->attachment = $fileName;
                $this->attachmentModel->attachmentable_id = $request_document['id'];
                $this->attachmentModel->attachmentable_type = Constant::MODEL_DOCUMENT;
                $this->attachmentModel->file_type = $fileType;
                $this->attachmentModel->file_size = $fileSize;
                $this->attachmentModel->created_by = auth()->id();
                $this->attachmentModel->save();
            }
            $request_document['file']->storeAs('uploads/project_'.$project_id.'/documents', $fileName, 'public');

        }catch (QueryException $exception){
            throw new AttachmentCreateException($exception->getMessage(), 500);
        }
    }

    /**
     * @param array $projectIds
     * @param array $documentIds
     * @return int
     * @throws AttachmentCreateException
     */
    public function usedStorageSize(array $projectIds, array $documentIds): int
    {
        try {
            return $this->attachmentModel->where(function($query) use ($projectIds){
                        $query->whereIn('attachmentable_id', $projectIds);
                        $query->where('attachmentable_type', Constant::MODEL_PROJECT);
                    })->orWhere(function ($query) use ($documentIds) {
                        $query->whereIn('attachmentable_id', $documentIds);
                        $query->where('attachmentable_type', Constant::MODEL_DOCUMENT);
                    })
                    ->sum('file_size');

        } catch (QueryException $exception) {
            throw new AttachmentCreateException($exception->getMessage(), 500);
        }
    }

}
