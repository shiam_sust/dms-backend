<?php


namespace App\Repositories;


use App\Events\ProjectCreatedEvent;
use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Models\Document;
use App\Models\Project;
use App\Repositories\Exceptions\Projects\CreateDeadlineErrorException;
use App\Repositories\Exceptions\Projects\ProjectCopyFailedException;
use App\Repositories\Exceptions\Projects\ProjectCreateErrorException;
use App\Repositories\Exceptions\Projects\ProjectDeleteFailedException;
use App\Repositories\Exceptions\Projects\ProjectNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectQueryException;
use App\Repositories\Exceptions\Projects\ProjectUpdateErrorException;
use App\Repositories\Exceptions\Projects\UserRemoveException;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Utils\Constant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;
use DateTime;

class ProjectRepository implements ProjectRepositoryInterface
{
    /**
     * @var Project
     */
    private $projectModel;

    /**
     * ProjectRepository constructor.
     * @param Project $privateModel
     */
    public function __construct(Project $projectModel)
    {
        $this->projectModel = $projectModel;
    }

    /**
     * @param Request $request
     * @return Collection
     * @throws ProjectQueryException
     */
    public function index(Request $request): Collection
    {
        try {
            $projects = $this->projectModel->newQuery()
                                            ->when($request->status === Constant::PROJECT_STATUS_DELETED, function ($query) use ($request) {
                                                return $query->onlyTrashed();
                                            })
                                            ->when($request->status, function ($query) use ($request) {
                                                return $query->where('status', $request->status);
                                            })
                                            ->when($request->status === Constant::PROJECT_STATUS_ACTIVE, function ($query) use ($request) {
                                                return $query->whereHas('users', function ($q) {
                                                    $q->where('users.id', auth()->id());
                                                });
                                            })
                                            ->when($request->status !== Constant::PROJECT_STATUS_ACTIVE, function ($query) use ($request) {
                                                return $query->whereHas('users',function ($q){
                                                    $q->where('user_type', Constant::USER_TYPE_HOST)
                                                        ->where('users.id', auth()->id());
                                                });
                                            })
                                            ->whereNull('team_id')
                                            ->orderBy('id', 'desc')
                                            ->get();
            return $projects;
        } catch(QueryException $exception) {
            throw new ProjectQueryException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return Collection
     * @throws ProjectQueryException
     */
    public function getSearchedProject(Request $request): Collection
    {
        try {
            $projects = $this->projectModel->newQuery()
                                            ->when($request->q, function ($query) use ($request) {
                                                return $query->where('title', 'like', '%' . $request->q . '%');
                                            })
                                            ->when($request,function ($query) use ($request) {
                                                return $query->whereHas('users',function ($q){
                                                    $q->where('users.id', auth()->id());
                                                });
                                            })
                                            ->whereNull('team_id')
                                            ->orderBy('id', 'desc')
                                            ->get();
            return $projects;
        } catch(QueryException $exception) {
            throw new ProjectQueryException($exception->getMessage(), 500);
        }
    }


    /**
     * @param CreateProjectRequestValidation $request
     * @return Project
     * @throws ProjectCreateErrorException
     */
    public function store(CreateProjectRequestValidation $request): Project
    {
        try{
            $this->projectModel->title = $request->name;
            $this->projectModel->project_summary = $request->summary;
            $this->projectModel->status = $request->is_draft ? Constant::PROJECT_STATUS_DRAFT : Constant::PROJECT_STATUS_ACTIVE;
            $this->projectModel->project_type = Constant::PROJECT_TYPE_PRIVATE;
            $this->projectModel->is_draft = $request->is_draft;
            $this->projectModel->created_by = auth()->id();
            $this->projectModel->save();
            return $this->projectModel;
        }catch (QueryException $error){
            throw new ProjectCreateErrorException($error->getMessage());
        }
    }

    /**
     * @param UpdateProjectRequestValidation $request
     * @param Project $project
     * @return Project
     * @throws ProjectCreateErrorException
     */
    public function update(UpdateProjectRequestValidation $request,Project $project): Project
    {
        try{
            $this->projectModel = $project;
            $this->projectModel->title = $request->name;
            $this->projectModel->project_summary = $request->summary;
            $this->projectModel->project_type = Constant::PROJECT_TYPE_PRIVATE;
            $this->projectModel->is_draft = $request->is_draft;
            $this->projectModel->save();
            return $this->projectModel;
        }catch (QueryException $error){
            throw new ProjectCreateErrorException($error->getMessage());
        }
    }

    /**
     * @param Project $project
     * @param string $type
     * @param string|null $scope
     * @return Project
     * @throws ProjectCreateErrorException
     */
    public function assignProjectToUser(Project $project, string $type, string $scope = null) : Project
    {
        try{
            $project->users()->attach(auth()->id(),["user_type" => $type, "scopes" => $scope]);
            return $this->projectModel;
        }catch (QueryException $error){
            throw new ProjectCreateErrorException($error->getMessage());
        }
    }

    public function show($id): Project
    {
        try{
            return $this->projectModel->findOrFail($id);
        }catch (ModelNotFoundException $error){
            throw new ProjectNotFoundException("Project Not Found",400);
        }
    }

    /**
     * @param CreateProjectDeadlineRequestValidation $request
     * @param Project $project
     * @return Project
     * @throws CreateDeadlineErrorException
     */
    public function setDeadline(CreateProjectDeadlineRequestValidation $request, Project $project): Project
    {
        try{
            $this->projectModel = $project;

            // $first_date = date('Y-m-d H:i',strtotime($request->end_date_first));
            // $utc_date_first = Carbon::createFromFormat('Y-m-d H:i', $first_date, 'GMT');
            // $utc_date_first->setTimezone('UTC');
            // $this->projectModel->end_date_first = date('Y-m-d H:i',strtotime($request->end_date_first));
            // $this->projectModel->end_date_second = date('Y-m-d H:i',strtotime($request->end_date_second));
            // $second_date = date('Y-m-d H:i',strtotime($request->end_date_second));
            // $utc_date_second = Carbon::createFromFormat('Y-m-d H:i', $second_date, 'GMT');
            // $utc_date_second->setTimezone('UTC');
            // $this->projectModel->end_date_second = $utc_date_second;

            $first_date = $this->converToTz($request->end_date_first, config('app.timezone'), $request->timezone);
            $this->projectModel->end_date_first = $first_date;
            $second_date = $this->converToTz($request->end_date_second, config('app.timezone'), $request->timezone);
            $this->projectModel->end_date_second = $second_date;
            $this->projectModel->reminder_before_lw_first = json_encode($request->reminder_before_lw_first);
            $this->projectModel->reminder_lw_first = json_encode($request->reminder_lw_first);
            $this->projectModel->reminder_before_lw_second = json_encode($request->reminder_before_lw_second);
            $this->projectModel->reminder_lw_second = json_encode($request->reminder_lw_second);
            $this->projectModel->save();
            return $this->projectModel;
        }catch (QueryException $error){
            throw new CreateDeadlineErrorException($error->getMessage());
        }
    }

    function converToTz($time="",$toTz='',$fromTz='')
    {   
        // timezone by php friendly values
        $date = new DateTime($time, new \DateTimeZone($fromTz));
        $date->setTimezone(new \DateTimeZone($toTz));
        $time= $date->format('Y-m-d H:i:s');
        return $time;
    }

    /**
     * @param int $projectId
     * @param string $userType
     * @param $count
     * @return Project
     * @throws ProjectNotFoundException
     * @throws ProjectUpdateErrorException
     */
    public function participantCountUpdate(int $projectId, string $userType, $count): Project
    {
        try{
            $project = $this->show($projectId);
            if($userType === Constant::USER_TYPE_COLLABORATOR) {
                $project->total_collaborators = count($project->collaborators);
            } else {
                $project->total_support_staff = count($project->support_staffs);
            }
            $project->save();

            return $project;
        }catch (QueryException $exception){
            throw new ProjectUpdateErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param Project $project
     * @return bool
     * @throws ProjectDeleteFailedException
     */
    public function deleteProject(Project $project): bool
    {
        try {
            $project->deleted_from = $project->status;
            $project->status = Constant::PROJECT_STATUS_DELETED;
            $project->deleted_by = auth()->id();
            $project->save();
            return $project->delete();
        }catch (QueryException $exception){
            throw new ProjectDeleteFailedException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Project $project
     * @return Project
     */
    public function startProject(Project $project): Project
    {
        try{
            $project->start_date = date('Y-m-d H:i:s');
            $project->status = Constant::PROJECT_STATUS_ACTIVE;
            $project->save();
            return $project;
        }catch (QueryException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Project $project
     * @throws ProjectCreateErrorException
     */
    public function sendProjectInvitation(Project $project): void
    {
        try {
            $users = $project->collaboratorsAndSupportStaff;
            Event::dispatch(new ProjectCreatedEvent($users,$project));
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Project $project
     * @return Project
     * @throws ProjectCopyFailedException
     */
    public function copyProjectDetails(Project $project): Project
    {
        try{
            $clone_project = $project->replicate();
            $clone_project->status = Constant::PROJECT_STATUS_DRAFT;
            $clone_project->is_draft = 1;
            $clone_project->save();

            foreach ($project->users as $user){
                $clone_project->users()->attach($user,["user_type" => $user->pivot->user_type]);
            }
            foreach ($project->documents as $document){
                $clone_document = $clone_project->documents()->save($document->replicate());
                foreach ($document->users as $user){
                    $clone_document->users()->attach($user,["user_type" => $user->pivot->user_type]);
                }
            }
            return $clone_project;

        }catch (QueryException $exception){
            throw new ProjectCopyFailedException("Duplicate project creation has been failed", 500);
        }

    }

    /**
     * @param Project $project
     * @param $user_id
     * @throws UserRemoveException
     */
    public function removeUser(Project $project, $user_id, string $user_type): void
    {
        try {
            $query = $project->users()
                ->wherePivot("user_type", $user_type)
                ->wherePivot("user_id", $user_id);

            if($query->exists()){
                $project->users()
                    ->wherePivot("user_type", $user_type)
                    ->detach($user_id);
            }else{
                throw new UserRemoveException("User not found in this project", 400);
            }
        }catch (ModelNotFoundException $exception){
            throw new UserRemoveException($exception->getMessage(), $exception->getCode());
        }
    }

    public function stopProject(Project $project): Project
    {
        try {
            $this->projectModel = $project;
            $this->projectModel->status = Constant::PROJECT_STATUS_PAST;
            $this->projectModel->stop_by = auth()->id();
            $this->projectModel->stop_at = date('Y-m-d H:i:s');
            $this->projectModel->save();
            return $this->projectModel;
        }catch (QueryException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }
    }

    /**
     * @param int $projectId
     * @return Project
     * @throws ProjectNotFoundException
     */
    public function restoreProject(int $projectId): Project
    {
        try {
            $project = $this->projectModel->onlyTrashed()->find($projectId);
            $project->status = $project->deleted_from;
            $project->deleted_by = NULL;
            $project->deleted_from = NULL;
            $project->deleted_at = NULL;
            $project->save();
            return $project;
        }catch (QueryException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }
    }

    public function getUserSpecificProject($id): Project
    {
        try {
            $project = $this->show($id);
            $result = $project->users()
                ->wherePivot("user_id", auth()->id())
                ->exists();
            if($result){
                return $project;
            }
            throw new ProjectNotFoundException("Access Invalid", 403);

        }catch (QueryException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }

    }

    /**
     * @return array|mixed
     */
    public function reminderProjectHost()
    {
        $projects = $this->projectModel
            ->where('status', Constant::PROJECT_STATUS_ACTIVE)
            ->get();

        $data = array();
        foreach ($projects as $project){
            $lw_first = date('Y-m-d', strtotime($project->end_date_first. ' - 7 days'));
            $before_lw_first = date('Y-m-d', strtotime($project->end_date_first. ' - 14 days'));

            $lw_second = date('Y-m-d', strtotime($project->end_date_second. ' - 7 days'));
            $before_lw_second = date('Y-m-d', strtotime($project->end_date_second. ' - 14 days'));


            $current_week_number = date('N');

            if(now() > $before_lw_first && now() <= $lw_first && in_array($current_week_number, json_decode($project->reminder_before_lw_first)))
            {
                $data[] = $this->getEmailBodyData($project,$project->end_date_first,"first");
            }elseif (now() > $lw_first && now() <= $project->end_date_first && in_array($current_week_number, json_decode($project->reminder_lw_first)))
            {
                $data[] = $this->getEmailBodyData($project,$project->end_date_first,"first");
            }
            elseif(now() > $before_lw_second && now() <= $lw_second && in_array($current_week_number, json_decode($project->reminder_before_lw_second)))
            {
                $data[] = $this->getEmailBodyData($project,$project->end_date_second,"second");

            }elseif (now() > $lw_second && now() <= $project->end_date_second && in_array($current_week_number, json_decode($project->reminder_lw_second)))
            {
                $data[] = $this->getEmailBodyData($project,$project->end_date_second,"second");
            }
        }

        return $data;
    }

    private function getEmailBodyData(Project $project, $reminder_date, $first_or_second){
        $data = array();
        $collaborators = $project->collaborators;

        foreach ($collaborators as $row)
        {
            $email = $row->email;

            $data[$email]['project_id'] = $project->id;
            $data[$email]['project_name'] = $project->title;
            $data[$email]['first_name'] = $row->first_name;
            $data[$email]['user_id'] = $row->id;
            $data[$email]['message'] = 'Your '.$first_or_second.' deadline for uploading documents is '.date('d M, Y',strtotime($reminder_date));
        }
        return $data;
    }

    /**
     * @return mixed|void
     * @throws ProjectQueryException
     */
    public function projectScheduleDelete()
    {

        try {
            DB::beginTransaction();
            $projects = $this->projectModel
                ->whereRaw('deleted_at <= date_sub(now(), interval 7 day)')
                ->onlyTrashed()
                ->get();

            foreach ($projects as $project)
            {
                $this->forceDelete($project);
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new ProjectQueryException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Project $project
     * @return mixed|void
     * @throws ProjectQueryException
     */
    public function forceDelete(Project $project)
    {
        try {
            $project->users()->detach();
            foreach ($project->documents as $document)
            {
                $document->users()->delete();
                $document->notes()->delete();
                $document->attachments()->delete();
                $document->delete();
            }
            $project->attachments()->delete();
            if(Storage::exists("public/uploads/project_".$project->id)) {
                Storage::deleteDirectory("public/uploads/project_".$project->id);
            }
            $project->forceDelete();
        }catch (\Exception $exception){
            throw new ProjectQueryException($exception->getMessage(), $exception->getCode());
        }
    }

    public function setProjectAsPast()
    {
        try {
            $projectIds = $this->projectModel
                ->where('end_date_second','<=', Carbon::today()->toDateString())
                ->get()
                ->pluck("id");
            if(!empty($projectIds)){
                $this->projectModel
                    ->whereIn('id',$projectIds)
                    ->update(['status' => Constant::PROJECT_STATUS_PAST]);
            }

        }catch (\Exception $exception){
            throw new ProjectQueryException($exception->getMessage(), $exception->getCode());
        }
    }

    public function showTrashed($id): Project
    {
        try{
            return $this->projectModel
                ->onlyTrashed()
                ->findOrFail($id);
        }catch (ModelNotFoundException $error){
            throw new ProjectNotFoundException("Project Not Found",400);
        }
    }

    public function makeProjectPast()
    {
        $activeProjects = Project::where('status', 'ACTIVE')->get();

        $now = new DateTime();
        $current_time = $now->format('Y-m-d H:i:s'); 

        foreach($activeProjects as $project){
            // dd($project);
            if($current_time > $project->end_date_second){
                $project->status = 'PAST';
                $project->save();
                //return $project;
            }
        }
        //return $activeProjects;
    }
}
