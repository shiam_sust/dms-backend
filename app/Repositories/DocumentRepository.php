<?php

namespace App\Repositories;
use App\Http\Requests\Documents\SaveDecisionRequestValidation;
use App\Http\Requests\Documents\StoreDocumentsRequestValidation;
use App\Http\Requests\Documents\UploadDocumentsRequestValidation;
use App\Http\Requests\Projects\AssignCollaboratorsRequestValidation;
use App\Models\DocumentNote;
use App\Repositories\Exceptions\Documents\AssignCollaboratorsException;
use App\Repositories\Exceptions\Documents\DocumentNotFoundException;
use App\Repositories\Exceptions\Documents\DocumentValidationFailedException;
use App\Repositories\Interfaces\DocumentRepositoryInterface;
use App\Models\Document;
use App\Models\Project;
use App\Http\Requests\Documents\CreateDocumentsRequestValidation;
use App\Repositories\Exceptions\Documents\DocumentCreateErrorException;
use App\Utils\Constant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use function Clue\StreamFilter\fun;
use \Datetime;

class DocumentRepository implements DocumentRepositoryInterface
{
    /**
     * @var Document
     */
    private $model;

    /**
     * ProjectRepository constructor
     * @param Document $document
     */
    public function __construct(Document $document)
    {
        $this->model = $document;
    }

    /**
     * @param StoreDocumentsRequestValidation $request
     * @param $project_id
     * @throws DocumentCreateErrorException
     */
    public function storeDocumentRequirements(StoreDocumentsRequestValidation $request, $project_id): void
    {
        try {
            foreach ($request->documents as $document) {
                if(isset($document["id"]) && $document["id"]){
                    $this->model = $this->show($document["id"]);
                    $this->model->modified_by = auth()->id();
                }else{
                    $this->model = new Document();
                    $this->model->project_id = $project_id;
                    $this->model->instruction = "instruction";
                    $this->model->status = Constant::DOCUMENT_STATUS_PENDING;
                    $this->model->created_by = auth()->id();
                }

                if (isset($document['template'])) {
                    $file = $document['template'];
                    $fileName =  auth()->id().'_'.time().'_'.str_replace(' ', '_',$file->getClientOriginalName());
                    $file->storeAs('uploads/project_'.$project_id.'/templates', $fileName, 'public');
                    $this->model->template = $fileName;
                    $this->model->template_size = $file->getSize();
                }
                $this->model->formats = $document['formats'];
                $this->model->title = $document['title'];
                $this->model->max_size = $document['max_size'];
                $this->model->size_unit = $document['size_unit'];
                $this->model->position = $document['position'];
                $this->model->save();
            }
        } catch (QueryException $e) {
            throw new DocumentCreateErrorException($e);
        }
    }

    /**
     * @param int $projectId
     * @param int $collaboratorId
     * @return Collection
     * @throws DocumentCreateErrorException
     */
    public function getDocumentsByCollaborator(int $projectId, int $collaboratorId): Collection
    {
        try {
            return $this->model->where('project_id', $projectId)
                        ->whereHas('users', function ($q) use($collaboratorId) {
                           $q->where('users.id', $collaboratorId)->where('user_type', Constant::USER_TYPE_COLLABORATOR)
                               ->where('status', '!=', Constant::DOCUMENT_STATUS_PENDING);
                        })->get();
        } catch (QueryException $e) {
            throw new DocumentCreateErrorException($e);
        }
    }

    /**
     * @param AssignCollaboratorsRequestValidation $request
     * @throws AssignCollaboratorsException
     */
    public function assignCollaborators(AssignCollaboratorsRequestValidation $request): void
    {
        try {
            foreach ($request->data  as $row) {
                $this->model = $this->model->findOrFail($row["document_id"]);
                $this->model->users()->syncWithPivotValues($row["collaborators_ids"],["user_type" => Constant::USER_TYPE_COLLABORATOR]);
            }
        }catch (QueryException $exception){
            throw new AssignCollaboratorsException($exception->getMessage(), 500);
        }
    }

    public function assignProjectDocumentToUser(Project $project): void
    {
        try {
            foreach ($project->documents as $document)
            {
                $document->users()->attach(auth()->id(),["user_type" => Constant::USER_TYPE_COLLABORATOR]);
            }
        }catch (QueryException $exception){
            throw new AssignCollaboratorsException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $id
     * @return Document
     * @throws DocumentNotFoundException
     */
    public function show($id): Document
    {
        try{
            return $this->model->findOrFail($id);
        }catch (ModelNotFoundException $error){
            throw new DocumentNotFoundException("Document Not Found",400);
        }
    }

    /**
     * @param Document $document
     * @throws DocumentNotFoundException
     */
    public function removeDocument(Document $document): void
    {
        try {
            $this->removeDocumentTemplate($document);
            $document->delete();
            $document->users()->detach();
        }catch (ModelNotFoundException $error){
            throw new DocumentNotFoundException("Document Not Found",400);
        }
    }

    /**
     * @param Document $document
     * @return bool
     */
    public function removeDocumentTemplate(Document $document): bool
    {
        if($document->template && Storage::exists("public/uploads/project_".$document->project_id."/templates/".$document->template)){
            Storage::delete("public/uploads/project_".$document->project_id."/templates/".$document->template);
            $document->template = null;
            $document->template_size = null;
            $document->save();
            return true;
        }
        return false;
    }

    /**
     * @param Document $document
     * @param string $status
     * @return Document
     * @throws DocumentNotFoundException
     */
    public function updateDocumentStatus(Document $document, string $status): Document
    {
        try {
//            $document->status = $status;
//            $document->save();
            $document->users()->updateExistingPivot(auth()->id(), ['status' => $status]);
            return $document;
        }catch (QueryException $error){
            throw new DocumentNotFoundException("Document status update failed!", 500);
        }
    }

    /**
     * @param Document $document
     * @param $file
     * @return bool
     */
    public function isValidUploadDocument(Document $document, $file, $project_id) : bool
    {
        $user = $document->users->where('id', auth()->id())->first();

        $project = Project::where('id', $project_id)->first();
        $now = new DateTime();
        $current_time = $now->format('Y-m-d H:i:s');

        if($project->end_date_first > $current_time){
            return true;
        }else{
            if($user->pivot->status == Constant::DOCUMENT_STATUS_SUBMITTED){
                return true;
            }else{
                throw new DocumentValidationFailedException("Document upload deadline have passed!", 422);
            }
        }

        $file_type = $file->getClientOriginalExtension();
        $file_size = $file->getSize();
        $size_limit = 0;

        $strSearch = [".", 'null'];
        $strReplace   = ["", ''];
        $document->formats = str_replace($strSearch, $strReplace, $document->formats);

        if($document->size_unit == "KB")
        {
            $size_limit = $document->max_size * 1024;
        }elseif ($document->size_unit == "MB")
        {
            $size_limit = $document->max_size * 1024 * 1024;
        }elseif ($document->size_unit == "GB")
        {
            $size_limit = $document->max_size * 1024 * 1024 * 1024;
        }
        if(!$document->formats && $file_size <= $size_limit)
        {
            return true;
        }

        $formats = array_map('trim', explode(',', $document->formats));

        if(in_array($file_type, $formats) && $file_size <= $size_limit)
        {
            return true;
        }
        throw new DocumentValidationFailedException("Uploaded document validation failed, Please recheck file type and size before upload.", 422);
    }

    public function saveDecision(SaveDecisionRequestValidation $request): void
    {
        try {
                $document = $this->show($request['id']);
//                $document->status = $request['status'];
//                $document->save();

                $document->users()->updateExistingPivot($request['user_id'], ['status' => $request['status']]);

                $documentNote = $document->notes()
                    ->where('user_id', $request['user_id'])
                    ->first();
                if(!$documentNote){
                    $documentNote = new DocumentNote();
                    $documentNote->user_id = $request['user_id'];
                    $documentNote->created_by = auth()->id();
                }
                $documentNote->note = $request['note'];
                $document->notes()->save($documentNote);

        }catch (QueryException $exception){
            throw new DocumentValidationFailedException($exception->getMessage(), 500);
        }
    }

    public function downloadDocumentAttachments(Collection $documents)
    {
        $path = storage_path('app/public/zips');

        if(!File::exists($path)){
            File::makeDirectory($path, 0755, true, true);
        }

        $zip_file = $path.'/documents_'.auth()->id().'.zip';
        if (is_file($zip_file)) {
            unlink($zip_file);
        }

        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE);

        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );

        foreach ($documents as $document)
        {
            foreach ($document->attachments as $attachment)
            {
                $path = "public/uploads/project_".$document->project_id."/documents/".$attachment->attachment;
                if($attachment->attachment)
                {
                    $zip->addFile(Storage::path($path), $attachment->attachment);
                }
            }
        }
        if($zip->close())
        {
            return response()->download($zip_file,"documents",$headers);
        }
        return response()->json([ 'message' => 'Export Problem'], 400);
    }

    /**
     * @param int $projectId
     * @param int $collaboratorId
     * @return Collection
     * @throws DocumentCreateErrorException
     */
    public function getApprovedOrRejectedDocuments(int $projectId, int $collaboratorId): Collection
    {
        try {
            return $this->model
                ->where('project_id', $projectId)
                ->whereHas('users', function ($q) use($collaboratorId) {
                    $q->where('users.id', $collaboratorId)
                        ->where('user_type', Constant::USER_TYPE_COLLABORATOR)
                        ->whereIn('status',  [Constant::DOCUMENT_STATUS_APPROVED, Constant::DOCUMENT_STATUS_REJECTED]);
                })
                ->get();
        } catch (QueryException $e) {
            throw new DocumentCreateErrorException($e->getMessage(), 500);
        }
    }

    /**
     * @param array $projectIds
     * @return array
     * @throws DocumentCreateErrorException
     */
    public function getDocumentIds(array $projectIds): array
    {
        try {
            return $this->model
                        ->whereIn('project_id', $projectIds)
                        ->pluck('id')
                        ->toArray();
        } catch(QueryException $exception) {
            throw new DocumentCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param array $projectIds
     * @return int
     * @throws DocumentCreateErrorException
     */
    public function usedStorageSize(array $projectIds): int
    {
        try {
            return $this->model
                ->whereIn('project_id', $projectIds)
                ->sum('template_size');
        } catch(QueryException $exception) {
            throw new DocumentCreateErrorException($exception->getMessage(), 500);
        }
    }
}
