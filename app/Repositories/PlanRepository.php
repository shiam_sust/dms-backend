<?php


namespace App\Repositories;


use App\Models\Plan;
use App\Repositories\Exceptions\Plans\PlanNotFoundException;
use App\Repositories\Interfaces\PlanRepositoryInterface;
use App\Utils\Constant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PlanRepository implements PlanRepositoryInterface
{

    /**
     * @var Plan
     */
    private $planModel;

    /**
     * PlanRepository constructor.
     * @param Plan $planModel
     */
    public function __construct(Plan $planModel)
    {
        $this->planModel = $planModel;
    }


    public function index(Request $request): Collection
    {

        return $this->planModel
            ->newQuery()
            ->when($request->plan_type, function ($query) use ($request) {
                return $query->where('plan_type', $request->plan_type);
            })
            ->when($request->time_period, function ($query) use ($request) {
                return $query->where('time_period', $request->time_period);
            })
            ->get();
    }

    public function show($id): Plan
    {
        try{
            return $this->planModel->findOrFail($id);
        }catch (ModelNotFoundException $error){
            throw new PlanNotFoundException("Plan not found",400);
        }
    }

    
}
