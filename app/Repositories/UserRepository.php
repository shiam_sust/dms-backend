<?php

namespace App\Repositories;
use App\Http\Requests\Payments\SaveDefaultPlanRequest;
use App\Models\User;
use App\Models\ProfileDocument;
use App\Repositories\Exceptions\Teams\TeamCreateException;
use App\Repositories\Exceptions\Users\AssignProjectToUserErrorException;
use App\Repositories\Exceptions\Users\CreateUserErrorException;
use App\Repositories\Exceptions\Users\ProfileDocumentCreateErrorException;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Exceptions\Users\UserException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;


class UserRepository implements UserRepositoryInterface
{
    /**
     * @var User
     */
    private $model;
    private $profileDocumentModel;

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(User $user, ProfileDocument $profileDocumentModel)
    {
        $this->model = $user;
        $this->profileDocumentModel = $profileDocumentModel;
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function findUserByEmail(string $email):?User
    {
        return $this->model->where('email', $email)->first();
    }

    /**
     * @param $data
     * @return User
     * @throws CreateUserErrorException
     */
    public function createCollaboratorOrSupportUser($data): User
    {
        try {
            $this->model = new User();
            $this->model->first_name = $data['first_name'];
            $this->model->last_name = $data['last_name'];
            $this->model->email = $data['email'];
            $this->model->organization = $data['organization'];
            $this->model->save();
            return $this->model;
        } catch(QueryException $exception) {
            throw new CreateUserErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param User $user
     * @param int $projectId
     * @param string $type
     * @param string $scopes
     * @param int $position
     * @return User
     * @throws AssignProjectToUserErrorException
     */
    public function assignProject(User $user, int $projectId, string $type, string $scopes, int $position = 0) : User
    {
        try{
            $user->projects()->attach($projectId,["user_type" => $type, "scopes" => $scopes?$scopes:'', "position" => $position]);
            return $user;
        }catch (QueryException $error){
            throw new AssignProjectToUserErrorException($error->getMessage(), 500);
        }
    }

    /**
     * @param User $user
     * @param $teamId
     * @param int $position
     * @return User
     * @throws TeamCreateException
     */
    public function assignTeam(User $user, $teamId, $position = 0) : User
    {
        try{
            $user->teams()->attach($teamId,["position" => $position]);
            return $user;
        }catch (QueryException $error){
            throw new TeamCreateException($error->getMessage(), 500);
        }
    }

    /**
     * @param User $user
     * @param $teamId
     * @param int $position
     * @return User
     * @throws TeamCreateException
     */
    public function updateTeamUserPosition(User $user, $teamId, int $position) : User
    {
        try{
            $user->teams()->updateExistingPivot($teamId, ["position" => $position]);
            return $user;
        }catch (QueryException $error){
            throw new TeamCreateException($error->getMessage(), 500);
        }
    }

    /**
     * @param User $user
     * @param $projectId
     * @param int $position
     * @return User
     * @throws AssignProjectToUserErrorException
     */
    public function updateUserPosition(User $user, $projectId, int $position) : User
    {
        try{
            $user->projects()->updateExistingPivot($projectId, ["position" => $position]);
            return $user;
        }catch (QueryException $error){
            throw new AssignProjectToUserErrorException($error->getMessage(), 500);
        }
    }

    /**
     * @param SaveDefaultPlanRequest $request
     * @return User
     */
    public function saveDefaultPlans(SaveDefaultPlanRequest $request): User
    {
        $this->model = auth()->user();
        if($request->team_plan_id){
            $this->model->default_team_plan_id = $request->team_plan_id;
        }
        if($request->personal_plan_id){
            $this->model->default_personal_plan_id = $request->personal_plan_id;
        }
        $this->model->save();
        return $this->model;
    }

    /**
     * @param $subscription
     * @return User
     */
    public function updateUserOnSubscription($planId, $pmId, $subscriptionId): User
    {
        try{
            $this->model = auth()->user();
            $this->model->plan_id = $planId;
            $this->model->payment_method_id = $pmId;
            $this->model->subscription_id = $subscriptionId;
            $this->model->default_personal_plan_id =  $planId;
            $this->model->save();
            return $this->model;
        }catch (\ErrorException $error){
            throw new AssignProjectToUserErrorException($error->getMessage(), 500);
        }
    }

    public function show($id): User
    {
        try{
            return $this->model->findOrFail($id);
        }catch (ModelNotFoundException $error){
            throw new UserException("User not found",400);
        }
    }

    public function cancelPersonalSubscription(User $user, $subscription): User
    {
        $user->plan_id = null;
        $user->payment_method_id = null;
        $user->subscription_id = null;
        $user->save();
        return $user;
    }

    public function storeProfile(User $user,Request $request): User
    {
        $file = $request->profile_picture;
        $fileName='';
        if(is_string($file)){

        }else{
            $fileName = auth()->id().'_'.time().'_'.str_replace(' ', '_',$file->getClientOriginalName());
            $user->profile_picture = $fileName;
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        // $user->email = $request->email;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->zip_code = $request->zip_code;
        $user->save();

        if(is_string($file)){

        }else{
            $file->storeAs('uploads/profile_picture_'.$user->id, $fileName, 'public');
        }

        return $user;
    }

    public function updateUsername(User $user,Request $request): User
    {
        $user->email = $request->new_username;
        $user->save();

        return $user;
    }

    public function updatePassword(User $user,Request $request): User
    {
        $user->password = Hash::make($request->new_password);
        $user->save();

        return $user;
    }

    /**
     * @param Request $request
     * @param User $user
     * @throws AttachmentCreateException
     */
    public function storeProfileAttachments(Request $request, User $user): void
    {
        try{
            // foreach($request->documents as $key => $document){}
            $file = $request->file;
            $fileSize = $request->file->getSize();
            $fileName = auth()->id().'_'.time().'_'.str_replace(' ', '_',$file->getClientOriginalName());
            $this->profileDocumentModel = new ProfileDocument();
            $this->profileDocumentModel->user_id = $user->id;
            $this->profileDocumentModel->name = $request->name;
            $this->profileDocumentModel->attachment = $fileName;
            $this->profileDocumentModel->file_size = $fileSize;
            $this->profileDocumentModel->save();
            $file->storeAs('uploads/profile_'.$user->id.'/documents', $fileName, 'public');
        }catch (QueryException $exception){
            throw new ProfileDocumentCreateErrorException($exception->getMessage());
        }
    }

    /**
     * @return Collection
     */
    public function profileHasDocument(): Collection
    {
        return $this->profileDocumentModel
            ->where('user_id', auth()->user())
            ->orderBy("id", "desc")
            ->get();
    }

    /**
     * @return Collection
     */
    public function getSubscribedUsers(): Collection
    {
        return $this->model
            ->where('plan_id', '!=', null)
            ->where('subscription_id', '!=', null)
            ->get();
    }
}
