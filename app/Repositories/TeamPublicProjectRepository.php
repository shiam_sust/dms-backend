<?php


namespace App\Repositories;


use App\Events\ProjectCreatedEvent;
use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Http\Requests\Projects\VerfiyProjectPIDRequestValidation;
use App\Models\Document;
use App\Models\Project;
use App\Repositories\Exceptions\Projects\CreateDeadlineErrorException;
use App\Repositories\Exceptions\Projects\CreatePidErrorException;
use App\Repositories\Exceptions\Projects\CreateCollaboratorsNumberErrorException;
use App\Repositories\Exceptions\Projects\ProjectCopyFailedException;
use App\Repositories\Exceptions\Projects\ProjectCreateErrorException;
use App\Repositories\Exceptions\Projects\ProjectDeleteFailedException;
use App\Repositories\Exceptions\Projects\ProjectNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectQueryException;
use App\Repositories\Exceptions\Projects\ProjectUpdateErrorException;
use App\Repositories\Exceptions\Projects\UserRemoveException;
use App\Repositories\Interfaces\TeamPublicProjectRepositoryInterface;
use App\Utils\Constant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class TeamPublicProjectRepository implements TeamPublicProjectRepositoryInterface
{
    /**
     * @var Project
     */
    private $projectModel;

    /**
     * ProjectRepository constructor.
     * @param Project $privateModel
     */
    public function __construct(Project $projectModel)
    {
        $this->projectModel = $projectModel;
    }

    /**
     * @param Request $request
     * @return Collection
     * @throws ProjectQueryException
     */
    public function index(Request $request): Collection
    {
        try {
            $projects = $this->projectModel->newQuery()
                                            ->when($request->status, function ($query) use ($request) {
                                                return $query->where('status', $request->status);
                                            })
                                            ->when($request->status === Constant::PROJECT_STATUS_ACTIVE, function ($query) use ($request) {
                                                return $query->whereHas('users', function ($q) {
                                                    $q->where('users.id', auth()->id());
                                                });
                                            })
                                            ->when($request->status !== Constant::PROJECT_STATUS_ACTIVE, function ($query) use ($request) {
                                                return $query->whereHas('users',function ($q){
                                                    $q->where('user_type', Constant::USER_TYPE_HOST)
                                                        ->where('users.id', auth()->id());
                                                });
                                            })
                                            ->orderBy('id', 'desc')
                                            ->get();
            return $projects;
        } catch(QueryException $exception) {
            throw new ProjectQueryException($exception->getMessage(), 500);
        }
    }

    /**
     * @param CreateProjectRequestValidation $request
     * @return Project
     * @throws ProjectCreateErrorException
     */
    public function store(CreateProjectRequestValidation $request): Project
    {
        try{
            $this->projectModel->team_id = $request->team_id;
            $this->projectModel->title = $request->name;
            $this->projectModel->project_summary = $request->summary;
            $this->projectModel->status = $request->is_draft ? Constant::PROJECT_STATUS_DRAFT : Constant::PROJECT_STATUS_ACTIVE;
            $this->projectModel->project_type = Constant::PROJECT_TYPE_PUBLIC;
            $this->projectModel->is_draft = $request->is_draft;
            $this->projectModel->created_by = auth()->id();
            $this->projectModel->save();
            return $this->projectModel;
        }catch (QueryException $error){
            throw new ProjectCreateErrorException($error->getMessage());
        }
    }

    /**
     * @param UpdateProjectRequestValidation $request
     * @param Project $project
     * @return Project
     * @throws ProjectCreateErrorException
     */
    public function update(UpdateProjectRequestValidation $request,Project $project): Project
    {
        try{
            $this->projectModel = $project;
            $this->projectModel->title = $request->name;
            $this->projectModel->project_summary = $request->summary;
            $this->projectModel->project_type = Constant::PROJECT_TYPE_PUBLIC;
            $this->projectModel->is_draft = $request->is_draft;
            $this->projectModel->save();
            return $this->projectModel;
        }catch (QueryException $error){
            throw new ProjectCreateErrorException($error->getMessage());
        }
    }

    public function show($id): Project
    {
        try{
            return $this->projectModel->findOrFail($id);
        }catch (ModelNotFoundException $error){
            throw new ProjectNotFoundException("Project Not Found",400);
        }
    }

    /**
     * @param CreateProjectDeadlineRequestValidation $request
     * @param Project $project
     * @return Project
     * @throws CreateDeadlineErrorException
     */
    public function setDeadline(CreateProjectDeadlineRequestValidation $request, Project $project): Project
    {
        try{
            $this->projectModel = $project;
            $this->projectModel->end_date_first = date('Y-m-d',strtotime($request->end_date_first));
            $this->projectModel->end_date_second = date('Y-m-d',strtotime($request->end_date_second));
            $this->projectModel->reminder_before_lw_first = json_encode($request->reminder_before_lw_first);
            $this->projectModel->reminder_lw_first = json_encode($request->reminder_lw_first);
            $this->projectModel->reminder_before_lw_second = json_encode($request->reminder_before_lw_second);
            $this->projectModel->reminder_lw_second = json_encode($request->reminder_lw_second);
            $this->projectModel->save();
            return $this->projectModel;
        }catch (QueryException $error){
            throw new CreateDeadlineErrorException($error->getMessage());
        }
    }

    /**
     * @return Project
     */
    public function storeCollaboratorsNumber($total_collaborators, Project $project) : Project
    {
        try{
            $this->projectModel = $project;
            $this->projectModel->total_collaborators = $total_collaborators;
            $this->projectModel->save();

            return $this->projectModel;
        }catch(QueryException $error){
            throw new CreateCollaboratorsNumberErrorException($error->getMessage());
        }
    }


    /**
     * @param Project $project
     * @param string $type
     * @param string|null $scope
     * @return Project
     * @throws ProjectCreateErrorException
     */
    public function assignProjectToUser(Project $project, string $type, string $scope = null) : Project
    {
        try{
            $project->users()->attach(auth()->id(),["user_type" => $type, "scopes" => $scope]);
            return $this->projectModel;
        }catch (QueryException $error){
            throw new ProjectCreateErrorException($error->getMessage());
        }
    }

    public function getUserSpecificProject($id): Project
    {
        try {
            $project = $this->show($id);
            $result = $project->users()
                ->wherePivot("user_id", auth()->id())
                ->exists();
            if($result){
                return $project;
            }
            throw new ProjectNotFoundException("Access Invalid", 403);

        }catch (QueryException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }
    }

    /**
     * @param int $projectId
     * @param string $userType
     * @param $count
     * @return Project
     * @throws ProjectNotFoundException
     * @throws ProjectUpdateErrorException
     */
    public function participantCountUpdate(int $projectId, string $userType, $count): Project
    {
        try{
            $project = $this->show($projectId);
            if($userType === Constant::USER_TYPE_COLLABORATOR) {
                $project->total_collaborators = count($project->collaborators);
            } else {
                $project->total_support_staff = count($project->support_staffs);
            }
            $project->save();

            return $project;
        }catch (QueryException $exception){
            throw new ProjectUpdateErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param Project $project
     * @param $user_id
     * @throws UserRemoveException
     */
    public function removeUser(Project $project, $user_id, string $user_type): void
    {
        try {
            $query = $project->users()
                ->wherePivot("user_type", $user_type)
                ->wherePivot("user_id", $user_id);

            if($query->exists()){
                $project->users()
                    ->wherePivot("user_type", $user_type)
                    ->detach($user_id);
            }else{
                throw new UserRemoveException("User not found in this project", 400);
            }
        }catch (ModelNotFoundException $exception){
            throw new UserRemoveException($exception->getMessage(), $exception->getCode());
        }
    }

    public function findProject($pid): bool
    {
        $project = $this->projectModel->where('pid', $pid)->first();
        if($project){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param Project $project
     * @return Project
     */
    public function startProject(Project $project): Project
    {
        try{
            $project->start_date = date('Y-m-d H:i:s');
            $project->status = Constant::PROJECT_STATUS_ACTIVE;
            $project->save();
            return $project;
        }catch (QueryException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Project $project
     * @throws ProjectCreateErrorException
     */
    public function sendProjectInvitation(Project $project): void
    {
        try {
            $users = $project->collaboratorsAndSupportStaff;
            Event::dispatch(new ProjectCreatedEvent($users,$project));
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @return Project
     */
    public function savePid($pid, Project $project) : Project
    {
        try{
            $this->projectModel = $project;
            $this->projectModel->pid = $pid;
            $this->projectModel->save();

            return $this->projectModel;
        }catch(QueryException $error){
            throw new CreatePidErrorException($error->getMessage());
        }
    }

    public function verifyProjectByPID(string $pid): Project
    {
        try {
            $project = $this->projectModel
                ->where("pid", $pid)
                ->where("status", Constant::PROJECT_STATUS_ACTIVE)
                ->firstOrFail();
            if($project)
            {
                if($project->collaborators->count() < $project->total_collaborators){
                    return $project;
                }
                throw new ProjectNotFoundException("Total collaborators is full.", 400);
            }
            throw new ProjectNotFoundException("Project not found", 400);

        }catch (ModelNotFoundException $exception){
            throw new ProjectNotFoundException("Project not found", 400);
        }
    }

    /**
     * @param $pid
     * @return Project
     * @throws ProjectNotFoundException
     */
    public function getProjectByPID($pid): Project
    {
        try {
            return $this->projectModel
                ->where("pid", $pid)
                ->where("status", Constant::PROJECT_STATUS_ACTIVE)
                ->firstOrFail();
        }catch (ModelNotFoundException $exception){
            throw new ProjectNotFoundException("Project doesn't exist on this PID", 400);
        }
    }
}
