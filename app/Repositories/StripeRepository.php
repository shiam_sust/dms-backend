<?php


namespace App\Repositories;


use App\Http\Requests\Payments\AddCardRequestValidation;
use App\Http\Requests\Teams\StoreTeamPlanRequest;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\Models\User;
use App\Repositories\Exceptions\Stripes\StripeErrorException;
use App\Repositories\Interfaces\StripeRepositoryInterface;
use Illuminate\Http\Request;
use Stripe\StripeClient;

class StripeRepository implements StripeRepositoryInterface
{
    /**
     * @var StripeClient
     */
    private $stripe;

    /**
     * StripeRepository constructor.
     */
    public function __construct()
    {
        $this->stripe = new StripeClient(
            config('stripe.stripe_secret')
        );
    }

    public function storeCustomer(): void
    {
        $user = auth()->user();
        if(!$user->stripe_id){
            $customer = $this->stripe->customers->create([
                'email' => $user->email,
                'name' => $user->first_name.' '.$user->last_name,
            ]);
            $user->stripe_id = $customer->id;
            $user->save();
        }
    }

    public function storePaymentMethod($card_info, $billing_info)
    {
        try {
            $expire_date = explode('/',$card_info['expire_date']);

            $payment_method = $this->stripe->paymentMethods->create([
                'type' => 'card',
                'card' => [
                    'number' => $card_info['number'],
                    'exp_month' => $expire_date[0],
                    'exp_year' => $expire_date[1],
                    'cvc' => $card_info['cvv'],
                ],
                "billing_details" => [
                    "name" => $billing_info['name'],
                    "address" => [
                        "line1" => $billing_info['line1'],
                        "line2" => $billing_info['line2'],
                        "city" => $billing_info['city'],
                        "country" => $billing_info['country'],
                        "postal_code" => $billing_info['zip_code'],
                        "state" => $billing_info['state'],
                        ]
                ]
            ]);

            $this->stripe->paymentMethods->attach(
                $payment_method->id,
                ['customer' => auth()->user()->stripe_id]
            );
            return $payment_method;
        }catch (\Exception $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function show($pm_id)
    {
        return $this->stripe->paymentMethods->retrieve(
            $pm_id,
            []
        );
    }

    public function delete($pm_id)
    {
        $this->stripe->paymentMethods->detach(
            $pm_id,
            []
        );
    }

    /**
     * @param $stripe_planId
     * @param $pm_id
     * @return mixed|\Stripe\Subscription
     * @throws StripeErrorException
     */
    public function subscribePlan($stripe_planId, $pm_id)
    {
        try {
            $subscription = $this->stripe->subscriptions->create([
                'customer' => auth()->user()->stripe_id,
                'items' => [
                    ['price' => $stripe_planId],
                ],
                'default_payment_method' => $pm_id
            ]);
            return $subscription;
        }catch (\Stripe\Exception\RateLimitException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\AuthenticationException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\ApiErrorException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function cancelSubscription($subscriptionId)
    {
        try {
            $subscription = $this->stripe->subscriptions->cancel(
                $subscriptionId,
                []
            );
            return $subscription;
        }catch (\Stripe\Exception\RateLimitException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\AuthenticationException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\ApiErrorException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function getSubscriptionById(string $stripeSubscriptionId)
    {
        try {
            return $this->stripe->subscriptions->retrieve(
                $stripeSubscriptionId,
                []
            );
        }catch (\Stripe\Exception\RateLimitException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\AuthenticationException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Stripe\Exception\ApiErrorException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function getUpcomingInvoice(string $customerId, string $subscriptionId)
    {
        try {
            return $this->stripe->invoices->upcoming([
                'customer' => $customerId,
                'subscription' => $subscriptionId
            ]);
        }catch (\Exception $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }
}
