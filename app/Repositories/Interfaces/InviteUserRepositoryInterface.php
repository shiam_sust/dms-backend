<?php


namespace App\Repositories\Interfaces;

use App\Models\InviteUser;
use Illuminate\Http\Request;

interface InviteUserRepositoryInterface
{

    /**
     * @param $data
     * @param int $projectId
     * @param string $userType
     * @return InviteUser
     */
    public function store($data, int $projectId, string $userType): InviteUser;

}
