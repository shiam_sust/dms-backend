<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Payments\AddCardRequestValidation;
use App\Http\Requests\Teams\StoreTeamPlanRequest;
use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Http\Request;

interface StripeRepositoryInterface
{
    /**
     *
     */
    public function storeCustomer():void;

    /**
     * @param $card_info
     * @param $billing_info
     * @return mixed
     */
    public function storePaymentMethod($card_info, $billing_info);

    /**
     * @param $pm_id
     * @return mixed
     */
    public function show($pm_id);

    /**
     * @param $pm_id
     * @return mixed
     */
    public function delete($pm_id);

    /**
     * @param $stripe_planId
     * @param $pm_id
     * @return mixed
     */
    public function subscribePlan($stripe_planId, $pm_id);

    /**
     * @param $subscriptionId
     * @return mixed
     */
    public function cancelSubscription($subscriptionId);

    /**
     * @param string $stripeSubscriptionId
     * @return mixed
     */
    public function getSubscriptionById(string $stripeSubscriptionId);

    /**
     * @param string $customerId
     * @param string $subscriptionId
     * @return mixed
     */
    public function getUpcomingInvoice(string $customerId, string $subscriptionId);
}
