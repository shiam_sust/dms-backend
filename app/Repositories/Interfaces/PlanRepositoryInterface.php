<?php


namespace App\Repositories\Interfaces;




use App\Models\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface PlanRepositoryInterface
{
    /**
     * @param $id
     * @return Plan
     */
    public function show($id): Plan;

    /**
     * @param Request $request
     * @return Collection
     */
    public function index(Request $request): Collection;
}
