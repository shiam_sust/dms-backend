<?php


namespace App\Repositories\Interfaces;

use App\Http\Requests\ProjectUsers\CreateCollaboratorsRequestValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface ProjectUserRepositoryInterface
{
    
    /**
     * @param CreateCollaboratorsRequestValidation $request
     * @param Request $request
     */
    public function storeCollaborators(CreateCollaboratorsRequestValidation $request);

    public function assignCollaborators(Request $request);

}
