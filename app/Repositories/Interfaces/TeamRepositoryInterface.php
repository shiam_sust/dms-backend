<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Teams\TeamCreateRequestValidation;
use App\Http\Requests\Teams\TeamUpdateRequestValidation;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface TeamRepositoryInterface
{
    /**
     * @param Request $request
     * @return Collection
     */
    public function index(Request $request): Collection;
    /**
     * @param $id
     * @return Team
     */
    public function show($id): Team;

    /**
     * @param TeamCreateRequestValidation $request
     * @param $plan_id
     * @param $pm_id
     * @param $subscription
     * @return Team
     */
    public function store(TeamCreateRequestValidation $request, $plan_id,$pm_id, $subscription) : Team;

    public function ChangeTeamPlan(Team $team, $plan_id, $pm_id, $subscription): Team;

    /**
     * @param Team $team
     * @param TeamUpdateRequestValidation $request
     * @return Team
     */
    public function update(Team $team, TeamUpdateRequestValidation $request) : Team;

    /**
     * @param Team $team
     * @param $userId
     */
    public function removeTeamMember(Team $team, $userId) : void;

    /**
     * @param Team $team
     */
    public function destroy(Team $team): void;

    /**
     * @return Collection
     */
    public function teamsHasPlan(): Collection;

    /**
     * @param Team $team
     * @param $subscription
     * @return Team
     */
    public function cancelTeamSubscription(Team $team, $subscription): Team;
}
