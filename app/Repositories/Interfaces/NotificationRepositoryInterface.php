<?php


namespace App\Repositories\Interfaces;

use App\Models\Document;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface NotificationRepositoryInterface
{

    /**
     * @param Request $request
     * @return Collection
     */
    public function index(Request $request): Collection;

    /**
     * @return Notification
     */
    public function store(Request $request): Notification;

    /**
     * @return Notification
     * @throws ProjectCreateErrorException
     */
    public function storeLowStorageAlert($user_id): Notification;

    public function LowStorageAlertReset($user_id): void;

    /**
     * @param User $user
     * @param $subscription
     * @return mixed
     */
    public function subscriptionEndAlert(User $user, $subscription);

    /**
     * @param int $userId
     * @param string $status
     * @return mixed
     */
    public function saveDocumentDecisionNotification(int $userId, string $status);

    /**
     * @param Document $document
     * @return mixed
     */
    public function uploadDocumentNotification(Document $document);

    public function assignCollaboratorsNotification($collaborators, $projectTitle);

    public function assignSupportStaffsNotification($staffs, $projectTitle);
    public function storeNotification(int $userId, string $type, string $title, string $description) : Notification;

}
