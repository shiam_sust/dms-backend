<?php


namespace App\Repositories\Interfaces;

use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface TeamPrivateProjectRepositoryInterface
{

    /**
     * @param Request $request
     * @return Collection
     */
    public function index(Request $request, $teamId): Collection;

    /**
     * @param CreateProjectRequestValidation $request
     * @return Project
     */
    public function store(CreateProjectRequestValidation $request): Project;

    /**
     * @param $id
     * @return Project
     */
    public function show($id) : Project;

    /**
     * @param $id
     * @return Project
     */
    public function getUserSpecificProject($id) : Project;

    /**
     * @param CreateProjectDeadlineRequestValidation $request
     * @param Project $project
     * @return Project
     */
    public function setDeadline(CreateProjectDeadlineRequestValidation $request, Project $project): Project;

    /**
     * @param UpdateProjectRequestValidation $request
     * @param Project $project
     * @return Project
     */
    public function update(UpdateProjectRequestValidation $request, Project $project) : Project;

    /**
     * @param Project $project
     * @param string $type
     * @param string|null $scope
     * @return Project
     */
    public function assignProjectToUser(Project $project, string $type, string $scope = null) : Project;

    /**
     * @param int $projectId
     * @param string $userType
     * @param $count
     * @return Project
     */
    public function participantCountUpdate(int $projectId, string $userType, $count): Project;

    /**
     * @param Project $project
     * @return bool
     */
    public function deleteProject(Project $project) : bool;

    /**
     * @param Project $project
     * @return Project
     */
    public function startProject(Project $project) : Project;

    /**
     * @param Project $project
     */
    public function sendProjectInvitation(Project $project) : void;

    /**
     * @param Project $project
     * @return Project
     */
    public function copyProjectDetails(Project $project) : Project;

    /**
     * @param Project $project
     * @param $user_id
     * @param string $user_type
     */
    public function removeUser(Project $project, $user_id, string $user_type): void;

    /**
     * @param Project $project
     * @return Project
     */
    public function stopProject(Project $project) : Project;

    /**
     * @param int $projectId
     * @return Project
     */
    public function restoreProject(int $projectId): Project;
}
