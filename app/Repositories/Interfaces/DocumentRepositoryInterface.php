<?php


namespace App\Repositories\Interfaces;

use App\Http\Requests\Documents\SaveDecisionRequestValidation;
use App\Http\Requests\Documents\StoreDocumentsRequestValidation;
use App\Http\Requests\Documents\UploadDocumentsRequestValidation;
use App\Http\Requests\Projects\AssignCollaboratorsRequestValidation;
use App\Models\Document;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface DocumentRepositoryInterface
{

    /**
     * @param $id
     * @return Document
     */
    public function show($id) : Document;
    /**
     * @param StoreDocumentsRequestValidation $request
     * @param $project_id
     */
    public function storeDocumentRequirements(StoreDocumentsRequestValidation $request, $project_id) : void;

    /**
     * @param int $projectId
     * @param int $collaboratorId
     * @return Collection
     */
    public function getDocumentsByCollaborator(int $projectId, int $collaboratorId): Collection;

    /**
     * @param AssignCollaboratorsRequestValidation $request
     */
    public function assignCollaborators(AssignCollaboratorsRequestValidation $request) : void;

    /**
     * @param Project $project
     */
    public function assignProjectDocumentToUser(Project $project) : void;

    /**
     * @param Document $document
     */
    public function removeDocument(Document $document) : void;

    /**
     * @param Document $document
     * @return bool
     */
    public function removeDocumentTemplate(Document $document) : bool;

    /**
     * @param Document $document
     * @param string $status
     * @return Document
     */
    public function updateDocumentStatus(Document $document, string $status) : Document;

    /**
     * @param Document $document
     * @param $file
     * @return bool
     */
    public function isValidUploadDocument(Document $document, $file, $project_id) : bool;

    /**
     * @param SaveDecisionRequestValidation $request
     */
    public function saveDecision(SaveDecisionRequestValidation $request) : void;

    /**
     * @param Collection $documents
     * @return mixed
     */
    public function downloadDocumentAttachments(Collection $documents);

    /**
     * @param int $projectId
     * @param int $collaboratorId
     * @return Collection
     */
    public function getApprovedOrRejectedDocuments(int $projectId, int $collaboratorId): Collection;

    /**
     * @param array $projectIds
     * @return array
     */
    public function getDocumentIds(array $projectIds): array;

    /**
     * @param array $projectIds
     * @return int
     */
    public function usedStorageSize(array $projectIds): int;
}
