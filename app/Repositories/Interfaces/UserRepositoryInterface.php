<?php


namespace App\Repositories\Interfaces;

use App\Http\Requests\Payments\SaveDefaultPlanRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface UserRepositoryInterface
{

    /**
     * @param string $email
     * @return User|null
     */
    public function findUserByEmail(string $email):?User;

    /**
     * @param $data
     * @return User
     */
    public function createCollaboratorOrSupportUser($data): User;

    /**
     * @param User $user
     * @param int $projectId
     * @param string $type
     * @param string $scopes
     * @param int $position
     * @return User
     */
    public function assignProject(User $user, int $projectId, string $type, string $scopes, int $position = 0) : User;

    /**
     * @param User $user
     * @param $teamId
     * @param int $position
     * @return User
     */
    public function assignTeam(User $user, $teamId, $position = 0) : User;

    /**
     * @param User $user
     * @param $teamId
     * @param int $position
     * @return User
     */
    public function updateTeamUserPosition(User $user, $teamId, int $position) : User;
    /**
     * @param User $user
     * @param $projectId
     * @param int $position
     * @return User
     */
    public function updateUserPosition(User $user, $projectId, int $position) : User;

    /**
     * @param SaveDefaultPlanRequest $request
     * @return User
     */
    public function saveDefaultPlans(SaveDefaultPlanRequest $request): User;

    /**
     * @param $planId
     * @param $pmId
     * @param $subscriptionId
     * @return User
     */
    public function updateUserOnSubscription($planId, $pmId, $subscriptionId):User;

    public function show($id): User;

    public function cancelPersonalSubscription(User $user, $subscription): User;

    public function storeProfile(User $user, Request $request): User;

    public function updateUsername(User $user,Request $request): User;

    public function updatePassword(User $user,Request $request): User;

    public function storeProfileAttachments(Request $request, User $user): void;

    public function profileHasDocument(): Collection;

    /**
     * @return Collection
     */
    public function getSubscribedUsers(): Collection;

}
