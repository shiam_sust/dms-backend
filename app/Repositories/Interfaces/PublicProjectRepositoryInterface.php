<?php


namespace App\Repositories\Interfaces;

use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Http\Requests\Projects\VerfiyProjectPIDRequestValidation;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface PublicProjectRepositoryInterface
{

    /**
     * @param Request $request
     * @return Collection
     */
    public function index(Request $request): Collection;

    /**
     * @param $id
     * @return Project
     */
    public function show($id) : Project;

    /**
     * @param $id
     * @return Project
     */
    public function getUserSpecificProject($id) : Project;

    /**
     * @param CreateProjectDeadlineRequestValidation $request
     * @param Project $project
     * @return Project
     */
    public function setDeadline(CreateProjectDeadlineRequestValidation $request, Project $project): Project;

    /**
     * @param CreateProjectRequestValidation $request
     * @return Project
     */
    public function store(CreateProjectRequestValidation $request): Project;

    /**
     * @return Project
     */
    public function storeCollaboratorsNumber($total_collaborators, Project $project): Project;

    /**
     * @param UpdateProjectRequestValidation $request
     * @param Project $project
     * @return Project
     */
    public function update(UpdateProjectRequestValidation $request, Project $project) : Project;

    /**
     * @param Project $project
     * @param string $type
     * @param string|null $scope
     * @return Project
     */
    public function assignProjectToUser(Project $project, string $type, string $scope = null) : Project;

    /**
     * @param int $projectId
     * @param string $userType
     * @param $count
     * @return Project
     */
    public function participantCountUpdate(int $projectId, string $userType, $count): Project;

    // /**
    //  * @param Project $project
    //  * @return bool
    //  */
    // public function deleteProject(Project $project) : bool;

    /**
     * @param Project $project
     * @return Project
     */
    public function startProject(Project $project) : Project;

    /**
     * @param Project $project
     */
    public function sendProjectInvitation(Project $project) : void;

    // /**
    //  * @param Project $project
    //  * @return Project
    //  */
    // public function copyProjectDetails(Project $project) : Project;

    /**
     * @param Project $project
     * @param $user_id
     * @param string $user_type
     */
    public function removeUser(Project $project, $user_id, string $user_type): void;

    // public function stopProject(Project $project) : Project;

    // public function restoreProject(Project $project) : Project;

    /**
     * @param $pid
     * @return bool
     */
    public function findProject($pid): bool;

    /**
     * @return Project
     */
    public function savePid($pid, Project $project) : Project;

    /**
     * @param string $pid
     * @return Project
     */
    public function verifyProjectByPID(string $pid) : Project;

    /**
     * @param $pid
     * @return Project
     */
    public function getProjectByPID($pid) : Project;

    // public function accountSettingExternal(): Project;

}
