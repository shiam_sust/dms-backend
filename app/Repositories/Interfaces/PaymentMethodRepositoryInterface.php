<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Payments\AddCardRequestValidation;
use App\Http\Requests\Payments\SaveCardRequestValidation;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface PaymentMethodRepositoryInterface
{
    public function index(Request $request): Collection;

    public function delete(PaymentMethod $paymentMethod):void;

    /**
     * @param $id
     * @return PaymentMethod
     */
    public function show($id):PaymentMethod;

    public function store($card_info, $billing_info, $mailing_info, $payment_method):PaymentMethod;

    public function saveCreditCard(SaveCardRequestValidation $request);
}
