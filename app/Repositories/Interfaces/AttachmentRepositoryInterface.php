<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Documents\UploadDocumentsRequestValidation;
use App\Models\Attachment;
use App\Models\Document;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;

interface AttachmentRepositoryInterface
{
    /**
     * @param $id
     * @return Attachment
     */
    public function show($id) : Attachment;

    /**
     * @param Request $request
     * @param Project $project
     */
    public function storePrivateProjectAttachments(Request $request, Project $project): void;

    /**
     * @param Attachment $attachment
     * @param $project_id
     * @return bool
     */
    public function delete(Attachment $attachment,$project_id) : bool;

    /**
     * @param $request_document
     * @param $project_id
     */
    public function storeDocumentAttachments($request_document, $project_id) : void;

    /**
     * @param array $projectIds
     * @param array $documentIds
     * @return int
     */
    public function usedStorageSize(array $projectIds, array $documentIds): int;

}
