<?php


namespace App\Repositories;


use App\Http\Requests\Teams\TeamCreateRequestValidation;
use App\Http\Requests\Teams\TeamUpdateRequestValidation;
use App\Models\Team;
use App\Repositories\Exceptions\Teams\TeamCreateException;
use App\Repositories\Exceptions\Teams\TeamPlanChangeException;
use App\Repositories\Exceptions\Teams\TeamException;
use App\Repositories\Interfaces\TeamRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class TeamRepository implements TeamRepositoryInterface
{
    /**
     * @var Team
     */
    private $teamModel;

    /**
     * TeamRepository constructor.
     * @param Team $teamModel
     */
    public function __construct(Team $teamModel)
    {
        $this->teamModel = $teamModel;
    }


    /**
     * @param Request $request
     * @return Collection
     */
    public function index(Request $request): Collection
    {

        return $this->teamModel
            ->whereHas('users', function($q) {
                $q->where('user_id', '=', auth()->id());
            })
            ->orWhere('team_leader',auth()->id())
            ->orderBy("id", "desc")
            ->get();

    }

    /**
     * @param TeamCreateRequestValidation $request
     * @param $plan_id
     * @param $pm_id
     * @param $subscription
     * @return Team
     * @throws TeamCreateException
     */
    public function store(TeamCreateRequestValidation $request, $plan_id, $pm_id, $subscription): Team
    {
        try {
            $file = $request->profile_icon;
            $fileName = auth()->id().'_'.time().'_'.str_replace(' ', '_',$file->getClientOriginalName());
            $this->teamModel->name = $request->name;
            $this->teamModel->details = $request->details;
            $this->teamModel->profile_icon = $fileName;
            $this->teamModel->icon_size = $file->getSize();
            $this->teamModel->team_leader = auth()->id();
            $this->teamModel->plan_id = $plan_id;
            $this->teamModel->payment_method_id = $pm_id;
            $this->teamModel->subscription_id = $subscription->id;
            $this->teamModel->subscription_status = $subscription->status;
            $this->teamModel->subscribed_at = date('Y-m-d H:i:s');
            $this->teamModel->save();
            $file->storeAs('uploads/teams_'.$this->teamModel->id, $fileName, 'public');
            return $this->teamModel;
        }catch (\ErrorException $exception){
            throw new TeamCreateException("Team create failed", 500);
        }
    }

    public function ChangeTeamPlan(Team $team, $plan_id, $pm_id, $subscription): Team
    {
        try {
            $team->plan_id = $plan_id;
            $team->payment_method_id = $pm_id;
            $team->subscription_id = $subscription->id;
            $team->subscription_status = $subscription->status;
            $team->subscribed_at = date('Y-m-d H:i:s');
            $team->save();
            return $team;
        }catch (\ErrorException $exception){
            throw new TeamPlanChangeException("Team plan change failed", 500);
        }
    }

    /**
     * @param $id
     * @return Team
     * @throws TeamException
     */
    public function show($id): Team
    {
        try{
            return $this->teamModel->findOrFail($id);
        }catch (ModelNotFoundException $error){
            throw new TeamException("Team not found",400);
        }
    }

    public function update(Team $team, TeamUpdateRequestValidation $request): Team
    {
        try {
            $this->teamModel = $team;

            if ($request->hasFile('profile_icon')) {
                if(Storage::exists("public/uploads/teams_".$this->teamModel->id.'/'.$team->profile_icon)) {
                    Storage::delete("public/uploads/teams_".$this->teamModel->id.'/'.$team->profile_icon);
                }
                $file = $request->profile_icon;
                $fileName = auth()->id().'_'.time().'_'.str_replace(' ', '_',$file->getClientOriginalName());
                $this->teamModel->profile_icon = $fileName;
                $file->storeAs('uploads/teams_'.$this->teamModel->id, $fileName, 'public');
            }

            $this->teamModel->name = $request->name;
            $this->teamModel->details = $request->details;
            $this->teamModel->save();
            return $this->teamModel;
        }catch (\ErrorException $exception){
            throw new TeamCreateException("Team update failed", 500);
        }
    }

    /**
     * @param Team $team
     * @param $userId
     * @throws TeamException
     */
    public function removeTeamMember(Team $team, $userId): void
    {
        try {
            $team->users()->detach($userId);
        }catch (QueryException $exception) {
            throw new TeamException("User remove failed.", 500);
        }
    }

    public function destroy(Team $team): void
    {
        try {
            $team->deleted_by = auth()->id();
            $team->save();
            $team->delete();
        }catch (QueryException $exception){
            throw new TeamException("Team delete failed", 500);
        }
    }

    /**
     * @return Collection
     */
    public function teamsHasPlan(): Collection
    {
        return $this->teamModel
            ->where('plan_id', '!=', null)
            ->where('subscription_status', '=', "active")
            ->where(function($query){
                $query->whereHas('users', function($q) {
                    $q->where('user_id', '=', auth()->id());
                });
                $query->OrWhere('team_leader',auth()->id());
            })
            ->orderBy("id", "desc")
            ->get();
    }

    public function cancelTeamSubscription(Team $team, $subscription): Team
    {
        $team->subscription_status = "cancel";
        $team->subscription_cancel_at = date('Y-m-d H:i:s',$subscription->canceled_at);
        $team->save();
        return $team;
    }
}
