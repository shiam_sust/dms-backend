<?php


namespace App\Repositories;


use App\Http\Requests\Payments\AddCardRequestValidation;
use App\Http\Requests\Payments\SaveCardRequestValidation;
use App\Models\PaymentMethod;
use App\Repositories\Exceptions\Stripes\StripeErrorException;
use App\Repositories\Interfaces\PaymentMethodRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PaymentMethodRepository implements PaymentMethodRepositoryInterface
{
    /**
     * @var PaymentMethod
     */
    private $model;

    /**
     * PaymentMethodRepository constructor.
     * @param PaymentMethod $model
     */
    public function __construct(PaymentMethod $model)
    {
        $this->model = $model;
    }


    public function delete(PaymentMethod $paymentMethod): void
    {
        try {
            $paymentMethod->delete();
        }catch (ModelNotFoundException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function show($id): PaymentMethod
    {
        try {
            return $this->model->findOrFail($id);
        }catch (ModelNotFoundException $e) {
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function store($card_info, $billing_info, $mailing_info, $payment_method): PaymentMethod
    {
        try {
            $this->model->pm_id = $payment_method->id;
            $this->model->user_id = auth()->id();
            $this->model->nickname = $card_info['nickname'];
            $this->model->number = $payment_method->card['last4'];
            $this->model->type = $payment_method->type;
            $this->model->brand = $payment_method->card['brand'];

            $this->model->billing_name = $billing_info['name'];
            $this->model->billing_line1 = $billing_info['line1'];
            $this->model->billing_line2 = $billing_info['line2'];
            $this->model->billing_city = $billing_info['city'];
            $this->model->billing_country = $billing_info['country'];
            $this->model->billing_state = $billing_info['state'];
            $this->model->billing_zip_code = $billing_info['zip_code'];

            $this->model->mailing_name = $mailing_info['name'];
            $this->model->mailing_line1 = $mailing_info['line1'];
            $this->model->mailing_line2 = $mailing_info['line2'];
            $this->model->mailing_city = $mailing_info['city'];
            $this->model->mailing_country = $mailing_info['country'];
            $this->model->mailing_state = $mailing_info['state'];
            $this->model->mailing_zip_code = $mailing_info['zip_code'];

            $this->model->save();

            return $this->model;
        }catch (\ErrorException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function saveCreditCard(SaveCardRequestValidation $request)
    {
        try {
           foreach ($request->data as $row){
               $this->model = $this->show($row['id']);
               $this->model->is_for_team = $row['is_for_team'];
               $this->model->is_for_personal = $row['is_for_personal'];
               $this->model->save();
           }
        }catch (QueryException $e){
            throw new StripeErrorException($e->getMessage(), $e->getCode());
        }
    }

    public function index(Request $request): Collection
    {
        return auth()->user()->payment_methods;
    }
}
