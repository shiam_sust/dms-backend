<?php


namespace App\Repositories;

use App\Models\Document;
use App\Models\Notification;
use App\Models\User;
use App\Repositories\Exceptions\Notifications\NotificationCreateErrorException;
use App\Repositories\Exceptions\Projects\ProjectQueryException;
use App\Repositories\Exceptions\Notifications\NotificationQueryException;
use App\Repositories\Interfaces\NotificationRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Utils\Constant;

class NotificationRepository implements NotificationRepositoryInterface
{
    /**
     * @var Notification
     */
    private $notificationModel;
    private $userRepo;


    /**
     * NotificationRepository constructor.
     * @param Notification $notificationModel
     */
    public function __construct(Notification $notificationModel, UserRepositoryInterface $userRepo)
    {
        $this->notificationModel = $notificationModel;
        $this->userRepo = $userRepo;
    }

    /**
     * @param Request $request
     * @return Collection
     * @throws ProjectQueryException
     */
    public function index(Request $request): Collection
    {
        //$notifications = $this->notificationModel->where('user_id', auth()->id())->orderBy('id', 'desc')->get();
        try {
            $notifications = $this->notificationModel->newQuery()
                ->where('user_id', auth()->id())
                ->where('type', 'notification')
                ->orderBy('id', 'desc')
                ->get();
            return $notifications;
        } catch(QueryException $exception) {
            throw new NotificationQueryException($exception->getMessage(), 500);
        }
    }


    /**
     * @return Notification
     * @throws ProjectCreateErrorException
     */
    public function store($request): Notification
    {
        try{
            // $this->notificationModel =  new Notification();
            $this->notificationModel->user_id = $request['user_id'];
            $this->notificationModel->type = "notification";
            $this->notificationModel->title = $request['message'];
            $this->notificationModel->save();
            return $this->notificationModel;
        }catch (QueryException $error){
            throw new NotificationCreateErrorException($error->getMessage());
        }
    }

    /**
     * @return Notification
     * @throws ProjectCreateErrorException
     */
    public function storeLowStorageAlert($user_id): Notification
    {
        try{
            // $this->notificationModel =  new Notification();
            $this->notificationModel->user_id = $user_id;
            $this->notificationModel->type = "alert";
            $this->notificationModel->title = Constant::PLAN_STORAGE_LIMIT;
            $this->notificationModel->save();
            return $this->notificationModel;
        }catch (QueryException $error){
            throw new NotificationCreateErrorException($error->getMessage());
        }
    }

    public function LowStorageAlertReset($user_id): void
    {
        $notification = $this->notificationModel->where("user_id", $user_id)
        ->where("title", Constant::PLAN_STORAGE_LIMIT)->first();

        if($notification){
            $notification->delete();
        }
    }


    /**
     * @param User $user
     * @param $subscription
     * @return mixed|void
     */
    public function subscriptionEndAlert(User $user, $subscription)
    {
        $this->notificationModel->user_id = $user->id;
        $this->notificationModel->title = "Subscription ending alert";
        $this->notificationModel->type = "alert";
        $this->notificationModel->description = 'Your subscription will be ended at '.date('d M, Y', $subscription->current_period_end);
        $this->notificationModel->is_read = 0;
        $this->notificationModel->save();
    }

    public function saveDocumentDecisionNotification(int $userId, string $status)
    {
        $this->notificationModel->user_id = $userId;
        $this->notificationModel->title = "Your document Decision";
        $this->notificationModel->type = "notification";
        $this->notificationModel->description = 'Your document has been '.$status;
        $this->notificationModel->is_read = 0;
        $this->notificationModel->save();
    }

    public function uploadDocumentNotification(Document $document)
    {
        foreach ($document->users as $user)
        {
            $this->notificationModel = new Notification();

            $this->notificationModel->user_id = $user->id;
            $this->notificationModel->title = "Document uploaded";
            $this->notificationModel->type = "notification";
            $this->notificationModel->description = "Document has been successfully uploaded";
            $this->notificationModel->is_read = 0;
            $this->notificationModel->save();
        }
    }

    public function assignCollaboratorsNotification($collaborators, $projectTitle)
    {
        foreach ($collaborators as $collaborator)
        {
            $user = $this->userRepo->findUserByEmail($collaborator['email']);

            $this->notificationModel = new Notification();
            $this->notificationModel->user_id = $user->id;
            $this->notificationModel->title = "Assign Project";
            $this->notificationModel->type = "notification";
            $this->notificationModel->description = "You have been assigned as collaborator of project ".$projectTitle;
            $this->notificationModel->is_read = 0;
            $this->notificationModel->save();
        }
    }

    public function assignSupportStaffsNotification($staffs, $projectTitle)
    {
        foreach ($staffs as $staff)
        {
            $user = $this->userRepo->findUserByEmail($staff['email']);

            $this->notificationModel = new Notification();
            $this->notificationModel->user_id = $user->id;
            $this->notificationModel->title = "Assign Project";
            $this->notificationModel->type = "notification";
            $this->notificationModel->description = "You have been assigned as support staff of project ".$projectTitle;
            $this->notificationModel->is_read = 0;
            $this->notificationModel->save();
        }
    }
    
    public function storeNotification(int $userId, string $type, string $title, string $description) : Notification
    {
        $this->notificationModel = new Notification();

        $this->notificationModel->user_id = $userId;
        $this->notificationModel->title = $title;
        $this->notificationModel->type = $type;
        $this->notificationModel->description = $description;
        $this->notificationModel->is_read = 0;
        $this->notificationModel->save();
        return $this->notificationModel;
    }
}
