<?php

namespace App\Console\Commands;

use App\Repositories\Interfaces\ProjectRepositoryInterface;
use Illuminate\Console\Command;

class DeleteProjectCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete project with all resources';

    /**
     * @var ProjectRepositoryInterface
     */
    private $privateProjectRepo;

    /**
     * DeleteProjectCommand constructor.
     * @param ProjectRepositoryInterface $privateProjectRepo
     */
    public function __construct(ProjectRepositoryInterface $privateProjectRepo)
    {
        parent::__construct();
        $this->privateProjectRepo = $privateProjectRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->privateProjectRepo->projectScheduleDelete();
    }
}
