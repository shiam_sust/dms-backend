<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Interfaces\NotificationRepositoryInterface;
use App\Models\User;

class PlanStorageAlertCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planStorage:alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'plan storage limit about about to finish alert';

    private $notificationRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NotificationRepositoryInterface $notificationRepo)
    {
        parent::__construct();
        $this->notificationRepo = $notificationRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::where('plan_id', '!=', null)->get();
        foreach($users as $user)
        {
            $storage_used = $this->usedSpaceOfPlan($user);
            $plan = $user->plan;
            $plan_storage = null;
            if($plan){
                $plan_storage = $plan->storage;
                if($plan_storage > $storage_used){
                    $remaining_storage = $plan_storage - $storage_used;
                    //return $remaining_storage;
                    if($remaining_storage < 0.05){
                        $this->notificationRepo->storeLowStorageAlert($user->id);
                    }else{
                        $this->notificationRepo->LowStorageAlertReset($user->id);
                    }
                }
            }
        }
    }

    public function usedSpaceOfPlan($user)
    {
        $projects = $user->projects->where('team_id', null);
        $personal_docs = $user->profile_documents->sum('file_size');
        $personal_docs = $this->byteToGb($personal_docs);
        //return $personal_docs; 
        $total_size = 0;
        $personal_active = null;
        $array = array();
        
        foreach($projects as $project){
            $array[$project->id] = false;
        }
        foreach($projects as $project){
            if($project->status == 'ACTIVE' && $array[$project->id] === false)
            {
                foreach($project->attachments as $attachment){
                    $total_size = $total_size + $attachment->file_size;
                        //return $attachment;
                }
                $array[$project->id] = true;
            }
        }
        $personal_active = $total_size;
        $personal_active = $this->byteToGb($personal_active);

        //return $personal_active;

        $total_size = 0;

        foreach($projects as $project){
            $array[$project->id] = false;
        }
        foreach($projects as $project){
            if($project->status == 'PAST' && $array[$project->id] === false)
            {
                foreach($project->attachments as $attachment){
                    $total_size = $total_size + $attachment->file_size;
                        //return $attachment;
                }
                $array[$project->id] = true;
            }
        }

        $personal_past = $total_size;
        $personal_past = $this->byteToGb($personal_past);

        $total_size = 0;

        foreach($projects as $project){
            $array[$project->id] = false;
        }
        foreach($projects as $project){
            if($project->status == 'DELETED' && $array[$project->id] === false)
            {
                foreach($project->attachments as $attachment){
                    $total_size = $total_size + $attachment->file_size;
                        //return $attachment;
                }
                $array[$project->id] = true;
            }
        }

        $personal_deleted = $total_size;
        $personal_deleted = $this->byteToGb($personal_deleted);
        
        $total_used = $personal_docs + $personal_active + $personal_past + $personal_deleted;

        return $total_used;

    }

    public function byteToGb($size){
        $res = $size;
        $res = $res / 1024;
        $res = $res / 1024;
        $res = $res / 1024;
        return $res;
    }
}
