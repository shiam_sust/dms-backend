<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Interfaces\ProjectRepositoryInterface;


class ActiveToPastCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:past';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'make past after second deadline';

    /**
     * @var ProjectRepositoryInterface
     */
    private $privateProjectRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ProjectRepositoryInterface $privateProjectRepo)
    {
        parent::__construct();
        $this->privateProjectRepo = $privateProjectRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $activeProjects = $this->privateProjectRepo->makeProjectPast();
        //dd($activeProjects);
    }
}
