<?php

namespace App\Console\Commands;

use App\Repositories\Interfaces\ProjectRepositoryInterface;
use Illuminate\Console\Command;

class PastProjectCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:past';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make project as past';

    /**
     * @var ProjectRepositoryInterface
     */
    private $privateProjectRepo;

    /**
     * PastProjectCommand constructor.
     * @param ProjectRepositoryInterface $privateProjectRepo
     */
    public function __construct(ProjectRepositoryInterface $privateProjectRepo)
    {
        parent::__construct();
        $this->privateProjectRepo = $privateProjectRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->privateProjectRepo->setProjectAsPast();
    }
}
