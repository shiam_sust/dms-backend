<?php

namespace App\Console\Commands;

use App\Mail\ProjectCreatedMail;
use App\Mail\ProjectReminderMail;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Repositories\Interfaces\NotificationRepositoryInterface;

class ProjectReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind project deadline';

    /**
     * @var ProjectRepositoryInterface
     */
    private $privateProjectRepo;
    private $notificationRepo;

    /**
     * ProjectReminderCommand constructor.
     * @param ProjectRepositoryInterface $privateProjectRepo
     */
    public function __construct(ProjectRepositoryInterface $privateProjectRepo, NotificationRepositoryInterface $notificationRepo)
    {
        parent::__construct();
        $this->privateProjectRepo = $privateProjectRepo;
        $this->notificationRepo = $notificationRepo;
    }


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $projects = $this->privateProjectRepo->reminderProjectHost();

        //dd($projects);

        foreach ($projects as $project){
            foreach ($project as $email => $message)
            {
                //dd($message);

                $this->notificationRepo->store($message);
                Mail::to($email)
                    ->queue(new ProjectReminderMail($message));
            }
        }
    }
}
