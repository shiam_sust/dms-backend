<?php

namespace App\Listeners;

use App\Events\ProjectCreatedEvent;
use App\Mail\ProjectCreatedMail;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class ProjectCreatedEventListener implements ShouldQueue
{
    public $tries = 3;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProjectCreatedEvent  $event
     * @return void
     */
    public function handle(ProjectCreatedEvent $event)
    {
        foreach ($event->users as $user){
            Mail::to($user->email)->queue(new ProjectCreatedMail($user, $event->project));
        }
    }
}
