<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Register Interface and Repository in here
        // You must place Interface in first place
        // If you dont, the Repository will not get readed.
        $this->app->bind(
            'App\Repositories\Interfaces\ProjectRepositoryInterface',
            'App\Repositories\ProjectRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\ProjectUserRepositoryInterface',
            'App\Repositories\ProjectUserRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\DocumentRepositoryInterface',
            'App\Repositories\DocumentRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\ProjectRepositoryInterface',
            'App\Repositories\ProjectRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\AttachmentRepositoryInterface',
            'App\Repositories\AttachmentRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\UserRepositoryInterface',
            'App\Repositories\UserRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\PublicProjectRepositoryInterface',
            'App\Repositories\PublicProjectRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\TeamRepositoryInterface',
            'App\Repositories\TeamRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\PlanRepositoryInterface',
            'App\Repositories\PlanRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\TeamPublicProjectRepositoryInterface',
            'App\Repositories\TeamPublicProjectRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\StripeRepositoryInterface',
            'App\Repositories\StripeRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\PaymentMethodRepositoryInterface',
            'App\Repositories\PaymentMethodRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\TeamPrivateProjectRepositoryInterface',
            'App\Repositories\TeamPrivateProjectRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\NotificationRepositoryInterface',
            'App\Repositories\NotificationRepository'
        );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
