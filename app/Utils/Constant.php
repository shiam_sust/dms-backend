<?php


namespace App\Utils;


class Constant
{
    //Project Status
    const PROJECT_STATUS_ACTIVE = "ACTIVE";
    const PROJECT_STATUS_DRAFT = "DRAFT";
    const PROJECT_STATUS_DELETED = "DELETED";
    const PROJECT_STATUS_PAST = "PAST";
    const PROJECT_STATUS_STOP = "STOP";

    //Project Type
    const PROJECT_TYPE_PUBLIC = "PUBLIC";
    const PROJECT_TYPE_PRIVATE = "PRIVATE";

    //User type
    const USER_TYPE_HOST = "HOST";
    const USER_TYPE_COLLABORATOR = "COLLABORATOR";
    const USER_TYPE_SUPPORT_STAFF = "SUPPORT_STAFF";


    const MODEL_PROJECT = "app\Models\Project";
    const MODEL_DOCUMENT = "app\Models\Document";
    const MODEL_USER = "app\Models\User";

    //document status
    const DOCUMENT_STATUS_PENDING = "PENDING";
    const DOCUMENT_STATUS_SUBMITTED = "SUBMITTED";
    const DOCUMENT_STATUS_APPROVED = "APPROVED";
    const DOCUMENT_STATUS_REJECTED = "REJECTED";

    const PLAN_TYPE_PERSONAL = "PERSONAL";
    const PLAN_TYPE_TEAM = "TEAM";
    const PLAN_TIME_PERIOD_MONTHLY = "MONTHLY";
    const PLAN_TIME_PERIOD_ANNUALLY = "ANNUALLY";
    const PLAN_NAME_BRONZE = "BRONZE";
    const PLAN_NAME_SILVER = "SILVER";
    const PLAN_NAME_GOLD = "GOLD";
    const PLAN_NAME_PLATINUM = "PLATINUM";
    const PLAN_NAME_BASIC = "BASIC";
    const PLAN_NAME_FREE = "FREE";
    const PLAN_NAME_PLUS = "PLUS";
    const PLAN_NAME_PROFESSIONAL = "PROFESSIONAL";
    const PLAN_NAME_EXECUTIVE = "EXECUTIVE";

    //alert title
    const PLAN_STORAGE_LIMIT = "Storage is low, please upgrade your plan";

}
