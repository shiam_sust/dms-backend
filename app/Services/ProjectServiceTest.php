<?php


namespace App\Services;


use App\Http\Requests\Documents\SaveDecisionRequestValidation;
use App\Http\Requests\Documents\StoreDocumentsRequestValidation;
use App\Http\Requests\Documents\UploadDocumentsRequestValidation;
use App\Http\Requests\Projects\AssignCollaboratorsRequestValidation;
use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Mail\SendDecisionMail;
use App\Models\Project;
use App\Repositories\Exceptions\Attachments\AttachmentErrorException;
use App\Repositories\Exceptions\Documents\AssignCollaboratorsException;
use App\Repositories\Exceptions\Documents\DocumentCreateErrorException;
use App\Repositories\Exceptions\Documents\DocumentNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectCopyFailedException;
use App\Repositories\Exceptions\Projects\ProjectNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectQueryException;
use App\Repositories\Exceptions\Projects\UserRemoveException;
use App\Repositories\Exceptions\Users\CreateUserErrorException;
use App\Repositories\Exceptions\Projects\CreateDeadlineErrorException;
use App\Repositories\Exceptions\Projects\ProjectCreateErrorException;
use App\Repositories\Exceptions\Projects\ProjectDeleteFailedException;
use App\Repositories\Interfaces\AttachmentRepositoryInterface;
use App\Repositories\Interfaces\DocumentRepositoryInterface;
use App\Repositories\Interfaces\NotificationRepositoryInterface;
use App\Repositories\Interfaces\PlanRepositoryInterface;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Utils\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ProjectServiceTest
{
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepo;

    /**
     * @var AttachmentRepositoryInterface
     */
    private $attachmentRepo;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepo;

    /**
     * @var NotificationRepositoryInterface
     */
    private $notificationRepo;

    private $planRepo;

    /**
     * ProjectServiceTest constructor.
     * @param ProjectRepositoryInterface $projectRepo
     * @param AttachmentRepositoryInterface $attachmentRepo
     * @param UserRepositoryInterface $userRepo
     * @param DocumentRepositoryInterface $documentRepo
     * @param NotificationRepositoryInterface $notificationRepo
     */
    public function __construct(ProjectRepositoryInterface $projectRepo, AttachmentRepositoryInterface $attachmentRepo,
                                UserRepositoryInterface $userRepo, DocumentRepositoryInterface $documentRepo,
                                NotificationRepositoryInterface $notificationRepo, PlanRepositoryInterface $planRepo)
    {
        $this->projectRepo = $projectRepo;
        $this->attachmentRepo = $attachmentRepo;
        $this->userRepo = $userRepo;
        $this->documentRepo = $documentRepo;
        $this->notificationRepo = $notificationRepo;
        $this->planRepo = $planRepo;
    }


    /**
     * @param CreateProjectRequestValidation $request
     * @return \App\Models\Project
     * @throws ProjectCreateErrorException
     */
    public function store(CreateProjectRequestValidation $request)
    {
        DB::beginTransaction();
        try {

            //            ============================ Plan: (Active Project) Limit Validation ================================
            if($request->hasfile('related_files')) {
                $plan = auth()->user()->plan;
                if (!$plan) {
                    throw new ProjectCreateErrorException('You don\'t have any personal plan, please select a personal plan to continue.', 400);
                }
                $projectIds = auth()->user()->created_projects()->whereNull('team_id')->pluck('id')->toArray();
                $documentIds = $this->documentRepo->getDocumentIds($projectIds);
                $docTempSize = $this->documentRepo->usedStorageSize($projectIds);
                $attachmentSize = $this->attachmentRepo->usedStorageSize($projectIds, $documentIds);
                if (($docTempSize + $attachmentSize + $this->getAttachmentsSize($request)) >= $plan->storage * 1073741824) {
                    throw new ProjectCreateErrorException('You already reached storage limit. Please upgrade your plan.', 400);
                }
            }
            //            ============================ Plan Limit Validation End ==============================================


            $project = $this->projectRepo->store($request);
            $this->projectRepo->assignProjectToUser($project,Constant::USER_TYPE_HOST, null);
            $this->attachmentRepo->storePrivateProjectAttachments($request, $project);
            DB::commit();
            return $project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new ProjectCreateErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param Request $request
     * @return int
     */
    public function getAttachmentsSize(Request $request): int
    {
        $size = 0;
        if($request->hasfile('related_files'))
        {
            foreach($request->file('related_files') as $key => $file)
            {
                $size += $file->getSize();
            }
        }
        return $size;
    }

    /**
     * @param UpdateProjectRequestValidation $request
     * @param $project_id
     * @return Project
     * @throws ProjectCreateErrorException
     */
    public function update(UpdateProjectRequestValidation $request, $project_id)
    {
        DB::beginTransaction();
        try {
            //            ============================ Plan: (Active Project) Limit Validation ================================
            if($request->hasfile('related_files')) {
                $plan = auth()->user()->plan;
                if (!$plan) {
                    throw new ProjectCreateErrorException('You don\'t have any personal plan, please select a personal plan to continue.', 400);
                }
                $projectIds = auth()->user()->created_projects()->whereNull('team_id')->pluck('id')->toArray();
                $documentIds = $this->documentRepo->getDocumentIds($projectIds);
                $docTempSize = $this->documentRepo->usedStorageSize($projectIds);
                $attachmentSize = $this->attachmentRepo->usedStorageSize($projectIds, $documentIds);
                if (($docTempSize + $attachmentSize + $this->getAttachmentsSize($request)) >= $plan->storage * 1073741824) {
                    throw new ProjectCreateErrorException('You already reached storage limit. Please upgrade your plan.', 400);
                }
            }
            //            ============================ Plan Limit Validation End ==============================================

            $project = $this->projectRepo->show($project_id);
            $project = $this->projectRepo->update($request, $project);
            $this->attachmentRepo->storePrivateProjectAttachments($request, $project);
            DB::commit();
            return $project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new ProjectCreateErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param array $persons
     * @param int $projectId
     * @param string $userType
     * @param int $position
     * @return Project
     * @throws CreateUserErrorException
     */
    public function storeCollaboratorOrSupportStaff(array $persons, int $projectId, string $userType):Project
    {
        DB::beginTransaction();
        try {

            //            ======================== Plan: (Staff) Limit Validation ================================
            $plan = auth()->user()->plan;
            if(!$plan) {
                throw new ProjectCreateErrorException('You don\'t have any personal plan, please select a personal plan to continue.', 400);
            }
            $activeUsers = [];
            if(auth()->user()->created_projects()->exists()) {
                $activeProjects = auth()->user()->created_projects()
                                ->whereNull('team_id')
                                ->where('status', Constant::PROJECT_STATUS_ACTIVE)
                                ->select('id')
                                ->get();

                foreach ($activeProjects as $project) {
                    foreach ($project->users as $user) {
                        if(!in_array($user->id, $activeUsers)) {
                            array_push($activeUsers, $user->id);
                        }
                    }
                }
            }
            $totalNewStaff = 0;
            foreach ($persons as $person) {
                if(empty($person['id'])) { $totalNewStaff += 1; }
            }

            if(count($activeUsers) + $totalNewStaff > $plan->max_staff) {
                throw new ProjectCreateErrorException('You already reached active Staffs limit. Please upgrade your plan.', 400);
            }
            //            ======================== Plan Limit Validation End ================================


            foreach ($persons as $person) {
                $user = $this->userRepo->findUserByEmail($person['email']);

                if(empty($person["id"])){
                    if(!$user) {
                        $user = $this->userRepo->createCollaboratorOrSupportUser($person);
                    }
                    $this->userRepo->assignProject($user, $projectId, $userType, (isset($person['scopes']))?$person['scopes']: '', $person['position']);
                }else{
                    $this->userRepo->updateUserPosition($user, $projectId, $person['position']);
                }
            }
            $project = $this->projectRepo->participantCountUpdate($projectId, $userType, count($persons));
            DB::commit();

            return $project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new CreateUserErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param $projectId
     * @param $collaboratorId
     * @return Collection
     * @throws DocumentCreateErrorException
     */
    public function getDocumentsByCollaborator($projectId, $collaboratorId): Collection
    {
        try {
            return $this->documentRepo->getDocumentsByCollaborator($projectId, $collaboratorId);
        }catch (\ErrorException $exception){
            throw new DocumentCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param CreateProjectDeadlineRequestValidation $request
     * @param $project_id
     * @return \App\Models\Project
     * @throws CreateDeadlineErrorException
     */
    public function setDeadline(CreateProjectDeadlineRequestValidation $request, $project_id){
        try {
            $project = $this->projectRepo->show($project_id);
            return $this->projectRepo->setDeadline($request,$project);
        }catch (\ErrorException $exception){
            throw new CreateDeadlineErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param StoreDocumentsRequestValidation $request
     * @param $project_id
     * @throws DocumentCreateErrorException
     */
    public function storeDocuments(StoreDocumentsRequestValidation $request, $project_id)
    {
        try {
            //            ============================ Plan: (Active Project) Limit Validation ================================
            if($this->getDocTemplatesSize($request)) {
                $plan = auth()->user()->plan;
                if (!$plan) {
                    throw new ProjectCreateErrorException('You don\'t have any personal plan, please select a personal plan to continue.', 400);
                }
                $projectIds = auth()->user()->created_projects()->whereNull('team_id')->pluck('id')->toArray();
                $documentIds = $this->documentRepo->getDocumentIds($projectIds);
                $docTempSize = $this->documentRepo->usedStorageSize($projectIds);
                $attachmentSize = $this->attachmentRepo->usedStorageSize($projectIds, $documentIds);
                if (($docTempSize + $attachmentSize + $this->getDocTemplatesSize($request)) >= $plan->storage * 1073741824) {
                    throw new ProjectCreateErrorException('You already reached storage limit. Please upgrade your plan.', 400);
                }
            }
            //            ============================ Plan Limit Validation End ==============================================

            $this->documentRepo->storeDocumentRequirements($request,$project_id);
        }catch (\ErrorException $exception){
            throw new DocumentCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return int
     */
    public function getDocTemplatesSize(Request $request): int
    {
        $size = 0;
        foreach ($request->documents as $document) {
            if (isset($document['template'])) {
                $file = $document['template'];
                $size += $file->getSize();
            }
        }
        return $size;
    }

    /**
     * @param $id
     * @throws ProjectDeleteFailedException
     */
    public function deleteProject($id){
        try {
            $project = $this->projectRepo->show($id);
            if($project->is_draft == 1){
                $this->projectRepo->deleteProject($project);
                return;
            }

            throw new ProjectDeleteFailedException("Project is not drafted",400);
        }catch (\ErrorException $exception){
            throw new ProjectDeleteFailedException($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @param $id
     * @throws ProjectDeleteFailedException
     */
    public function forceDelete($id){
        try {
            $project = $this->projectRepo->showTrashed($id);
            $this->projectRepo->forceDelete($project);
        }catch (\ErrorException $exception){
            throw new ProjectDeleteFailedException($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @param AssignCollaboratorsRequestValidation $request
     * @throws AssignCollaboratorsException
     */
    public function assignCollaborator(AssignCollaboratorsRequestValidation $request){
        try {
            $this->documentRepo->assignCollaborators($request);
        }catch (\ErrorException $exception){
            throw new AssignCollaboratorsException("Assign collaborators is failed", 500);
        }

    }

    /**
     * @param $project_id
     */
    public function startProject($project_id){
        try{
            //            ============================ Plan: (Active Project) Limit Validation ================================
            $plan = auth()->user()->plan;
            if(!$plan) {
                throw new ProjectCreateErrorException('You don\'t have any personal plan, please select a personal plan to continue.', 400);
            }
            $totalActiveProjects = auth()->user()->created_projects()->where('id', '!=', $project_id)->whereNull('team_id')->where('status', Constant::PROJECT_STATUS_ACTIVE)->count();
            if($totalActiveProjects >= $plan->max_active_project) {
                throw new ProjectCreateErrorException('You already reached active projects limit. Please upgrade your plan.', 400);
            }
            //            ============================ Plan Limit Validation End ==============================================

            $project = $this->projectRepo->show($project_id);
            $project = $this->projectRepo->startProject($project);
            $this->projectRepo->sendProjectInvitation($project);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $project_id
     * @param $id
     * @return bool
     * @throws AttachmentErrorException
     */
    public function deleteAttachment($project_id, $id)
    {
        try {
            $attachment = $this->attachmentRepo->show($id);
            return $this->attachmentRepo->delete($attachment, $project_id);
        }catch (\ErrorException $exception){
            throw new AttachmentErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws ProjectQueryException
     */
    public function readCSVData(Request $request){
        try {
            $header = null;

            $data = array();
            if($request->hasFile("file"))
            {
                $file = fopen($request->file, "r");
                while ( ($row = fgetcsv($file, 100, ",")) !== FALSE) {
                    if (!$header){
                        $header = $row;
                        if(count($row)  != 4 || $row[0] != "email" || $row[1] != "first_name" || $row[2] != "last_name" ||  $row[3] != "organization"){
                            return response()->json(["message" => "invalid file data"], 400);
                        }
                    }
                    else
                        $data[] = array_combine($header, $row);
                }
                fclose($file);
            }
            return $data;
        }catch (\ErrorException $exception){
            throw new ProjectQueryException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $project_id
     * @return Project
     * @throws ProjectCopyFailedException
     */
    public function copyProjectDetails($project_id)
    {
        try {
            DB::beginTransaction();
            $project = $this->projectRepo->show($project_id);
            $clone_project = $this->projectRepo->copyProjectDetails($project);
            DB::commit();
            return $clone_project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new ProjectCopyFailedException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $project_id
     * @param $user_id
     * @param $user_type
     * @throws UserRemoveException
     */
    public function removeUser($project_id, $user_id, $user_type)
    {
        try {
            $project = $this->projectRepo->show($project_id);
            $this->projectRepo->removeUser($project,$user_id, $user_type);
        }catch (\ErrorException $exception){
            throw new UserRemoveException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $document_id
     * @throws DocumentNotFoundException
     */
    public function removeDocument($document_id)
    {
        try {
            $document = $this->documentRepo->show($document_id);
            $this->documentRepo->removeDocument($document);
        }catch (\ErrorException $error){
            throw new DocumentNotFoundException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param $document_id
     * @throws DocumentNotFoundException
     */
    public function removeTemplate($document_id){
        try {
            $document = $this->documentRepo->show($document_id);
            $this->documentRepo->removeDocumentTemplate($document);
        }catch (\ErrorException $error){
            throw new DocumentNotFoundException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param $project_id
     * @return Project
     * @throws ProjectNotFoundException
     */
    public function stopProject($project_id){
        try {
            $project = $this->projectRepo->show($project_id);
            return $this->projectRepo->stopProject($project);
        }catch (\ErrorException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $project_id
     * @return Project
     * @throws ProjectNotFoundException
     */
    public function restoreProject($project_id){
        try {
            return $this->projectRepo->restoreProject($project_id);
        }catch (\ErrorException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }
    }

    /**
     * @param UploadDocumentsRequestValidation $request
     * @param $project_id
     * @throws AttachmentErrorException
     */
    public function uploadDocuments(UploadDocumentsRequestValidation $request, $project_id)
    {
        try {
            DB::beginTransaction();

            //            ============================ Plan: (Active Project) Limit Validation ================================
            if($this->getUploadDocSize($request)) {
                $project = $this->projectRepo->show($project_id);
                $user = $this->userRepo->show($project->created_by);
                $plan = $user->plan;
                if (!$plan) {
                    throw new ProjectCreateErrorException('Your HOST don\'t have any personal plan, please communicate with your HOST.', 400);
                }
                $projectIds = $user->created_projects()->whereNull('team_id')->pluck('id')->toArray();
                $documentIds = $this->documentRepo->getDocumentIds($projectIds);
                $docTempSize = $this->documentRepo->usedStorageSize($projectIds);
                $attachmentSize = $this->attachmentRepo->usedStorageSize($projectIds, $documentIds);
                if (($docTempSize + $attachmentSize + $this->getUploadDocSize($request)) >= $plan->storage * 1073741824) {
                    throw new ProjectCreateErrorException('You HOST has storage limit. please communicate with your HOST.', 400);
                }
            }
            //            ============================ Plan Limit Validation End ==============================================


            foreach ($request->documents as $request_document){
                $document = $this->documentRepo->show($request_document['id']);
                if($this->documentRepo->isValidUploadDocument($document, $request_document['file'], $project_id))
                {
                    $this->attachmentRepo->storeDocumentAttachments($request_document, $project_id);
                    $this->documentRepo->updateDocumentStatus($document, Constant::DOCUMENT_STATUS_SUBMITTED);
                    $this->notificationRepo->uploadDocumentNotification($document);
                }
            }
            DB::commit();
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new AttachmentErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @return int
     */
    public function getUploadDocSize(Request $request): int
    {
        $size = 0;
        foreach ($request->documents as $document) {
            if (isset($document['file'])) {
                $file = $document['file'];
                $size += $file->getSize();
            }
        }
        return $size;
    }

    public function testingDoc(){
        $document = $this->documentRepo->show(16);
        $user = $document->users->where('id', 1)->first();
        return $user;
    }

    public function sendMailToHostSupportStaffs($project_id)
    {
        $project = $this->projectRepo->show($project_id);
        return $project;
    }

    /**
     * @param SaveDecisionRequestValidation $request
     * @throws DocumentCreateErrorException
     */
    public function saveDecision(SaveDecisionRequestValidation $request)
    {
        try {
            DB::beginTransaction();
            $this->documentRepo->saveDecision($request);
            $this->notificationRepo->saveDocumentDecisionNotification($request['user_id'], $request['status']);
            DB::commit();
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new DocumentCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $project_id
     * @param $user_id
     * @return mixed
     * @throws DocumentNotFoundException
     */
    public function downloadDocuments($project_id, $user_id)
    {
        try {
            $documents = $this->documentRepo->getDocumentsByCollaborator($project_id, $user_id);
            return $this->documentRepo->downloadDocumentAttachments($documents);
        }catch (\ErrorException $exception) {
            throw new DocumentNotFoundException($exception->getMessage(), $exception->getCode());
        }
    }

    public function sendDecisionToCollaborators($project_id, $user_id)
    {
        $documents =  $this->documentRepo->getApprovedOrRejectedDocuments($project_id, $user_id);
        $result = array();
        $data = array();
        foreach ($documents as $document){
            $docUser = $document->users()->wherePivot('user_id', $user_id)->first();
            $notes = $document->notes()->where('user_id', $user_id)->orderBy('id', 'desc')->first();
            $data['title'] = $document->title;
            $data['status'] = $docUser ? $docUser->pivot->status : "";
            $data['note'] = $notes ? $notes->note : null;
            $result[] = $data;
        }
        if($docUser){
            Mail::to($docUser->email)
                ->queue(new SendDecisionMail($result));
        }
    }

}
