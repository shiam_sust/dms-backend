<?php


namespace App\Services;


use App\Http\Requests\Documents\SaveDecisionRequestValidation;
use App\Http\Requests\Documents\StoreDocumentsRequestValidation;
use App\Http\Requests\Documents\UploadDocumentsRequestValidation;
use App\Http\Requests\Projects\AssignCollaboratorsRequestValidation;
use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Http\Resources\Projects\ProjectDetailsResource;
use App\Mail\ProjectReminderMail;
use App\Mail\SendDecisionMail;
use App\Models\Project;
use App\Repositories\Exceptions\Attachments\AttachmentErrorException;
use App\Repositories\Exceptions\Documents\AssignCollaboratorsException;
use App\Repositories\Exceptions\Documents\DocumentCreateErrorException;
use App\Repositories\Exceptions\Documents\DocumentNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectCopyFailedException;
use App\Repositories\Exceptions\Projects\ProjectNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectQueryException;
use App\Repositories\Exceptions\Projects\UserRemoveException;
use App\Repositories\Exceptions\Users\CreateUserErrorException;
use App\Repositories\Exceptions\Projects\CreateDeadlineErrorException;
use App\Repositories\Exceptions\Projects\ProjectCreateErrorException;
use App\Repositories\Exceptions\Projects\ProjectDeleteFailedException;
use App\Repositories\Interfaces\AttachmentRepositoryInterface;
use App\Repositories\Interfaces\DocumentRepositoryInterface;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Utils\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ProjectService
{
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepo;

    /**
     * @var AttachmentRepositoryInterface
     */
    private $attachmentRepo;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepo;

    /**
     * ProjectService constructor.
     * @param ProjectRepositoryInterface $projectRepo
     * @param AttachmentRepositoryInterface $attachmentRepo
     * @param UserRepositoryInterface $userRepo
     * @param DocumentRepositoryInterface $documentRepo
     */
    public function __construct(ProjectRepositoryInterface $projectRepo, AttachmentRepositoryInterface $attachmentRepo, UserRepositoryInterface $userRepo, DocumentRepositoryInterface $documentRepo)
    {
        $this->projectRepo = $projectRepo;
        $this->attachmentRepo = $attachmentRepo;
        $this->userRepo = $userRepo;
        $this->documentRepo = $documentRepo;
    }

    /**
     * @param UploadDocumentsRequestValidation $request
     * @param $project_id
     * @throws AttachmentErrorException
     */
    public function uploadDocuments(UploadDocumentsRequestValidation $request, $project_id)
    {
        try {
            DB::beginTransaction();
            foreach ($request->documents as $request_document){
                $document = $this->documentRepo->show($request_document['id']);
                if($this->documentRepo->isValidUploadDocument($document, $request_document['file']))
                {
                    $this->attachmentRepo->storeDocumentAttachments($request_document, $project_id);
                    $this->documentRepo->updateDocumentStatus($document, Constant::DOCUMENT_STATUS_SUBMITTED);
                }
            }
            DB::commit();
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new AttachmentErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    
}
