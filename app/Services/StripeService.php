<?php


namespace App\Services;


use App\Http\Requests\Payments\AddCardRequestValidation;
use App\Http\Requests\Payments\SaveCardRequestValidation;
use App\Http\Requests\Payments\SaveDefaultPlanRequest;
use App\Http\Requests\Payments\SubscribePersonalPlanRequest;
use App\Http\Requests\Teams\StoreTeamPlanRequest;
use App\Repositories\Exceptions\Stripes\StripeErrorException;
use App\Repositories\Interfaces\NotificationRepositoryInterface;
use App\Repositories\Interfaces\PaymentMethodRepositoryInterface;
use App\Repositories\Interfaces\PlanRepositoryInterface;
use App\Repositories\Interfaces\StripeRepositoryInterface;
use App\Repositories\Interfaces\TeamRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;

class StripeService
{
    /**
     * @var StripeRepositoryInterface
     */
    private $stripeRepo;

    /**
     * @var PaymentMethodRepositoryInterface
     */
    private $paymentMethodRepo;

    /**
     * @var PlanRepositoryInterface
     */
    private $planRepo;

    /**
     * @var TeamRepositoryInterface
     */
    private $teamRepo;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var NotificationRepositoryInterface
     */
    private $notificationRepo;

    /**
     * StripeService constructor.
     * @param StripeRepositoryInterface $stripeRepo
     * @param PaymentMethodRepositoryInterface $paymentMethodRepo
     * @param PlanRepositoryInterface $planRepo
     * @param TeamRepositoryInterface $teamRepo
     * @param UserRepositoryInterface $userRepo
     * @param NotificationRepositoryInterface $notificationRepo
     */
    public function __construct(StripeRepositoryInterface $stripeRepo,
                                    PaymentMethodRepositoryInterface $paymentMethodRepo,
                                    PlanRepositoryInterface $planRepo, TeamRepositoryInterface $teamRepo,
                                    UserRepositoryInterface $userRepo, NotificationRepositoryInterface $notificationRepo)
    {
        $this->stripeRepo = $stripeRepo;
        $this->paymentMethodRepo = $paymentMethodRepo;
        $this->planRepo = $planRepo;
        $this->teamRepo = $teamRepo;
        $this->userRepo = $userRepo;
        $this->notificationRepo = $notificationRepo;
    }


    /**
     * @param AddCardRequestValidation $request
     * @throws StripeErrorException
     */
    public function storePaymentMethod(AddCardRequestValidation $request)
    {
        try {
            $this->stripeRepo->storeCustomer();
            $payment_method = $this->stripeRepo->storePaymentMethod($request->card_info, $request->billing_info);

            if($payment_method)
            {
                if($request->is_same){
                    $this->paymentMethodRepo->store($request->card_info,$request->billing_info, $request->billing_info, $payment_method);
                }else{
                    $this->paymentMethodRepo->store($request->card_info,$request->billing_info, $request->mailing_info, $payment_method);
                }
            }
        }catch (\Exception $exception){
            throw new StripeErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $id
     * @throws StripeErrorException
     */
    public function delete($id)
    {
        try {
            $paymentMethod = $this->paymentMethodRepo->show($id);
            $this->stripeRepo->delete($paymentMethod->pm_id);
            $this->paymentMethodRepo->delete($paymentMethod);
        }catch (\Exception $exception){
            throw new StripeErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param SaveCardRequestValidation $request
     */
    public function saveCreditCard(SaveCardRequestValidation $request)
    {
        try {
            $this->paymentMethodRepo->saveCreditCard($request);
        }catch (\Exception $exception){
            throw new StripeErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request){
        try {
            return $this->paymentMethodRepo->index($request);
        }catch (\Exception $exception){
            throw new StripeErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param SaveDefaultPlanRequest $request
     */
    public function saveDefaultPlans(SaveDefaultPlanRequest $request)
    {
        try {
            $this->userRepo->saveDefaultPlans($request);
        }catch (\Exception $exception){
            throw new StripeErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $teamId
     * @return \App\Models\Team
     * @throws StripeErrorException
     */
    public function teamSubscriptionCancel($teamId)
    {
        try {
            $team = $this->teamRepo->show($teamId);
            $subscription = $this->stripeRepo->cancelSubscription($team->subscription_id);
            return $this->teamRepo->cancelTeamSubscription($team, $subscription);
        }catch (\Exception $exception){
            throw new StripeErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @return \App\Models\User
     * @throws StripeErrorException
     */
    public function personalSubscriptionCancel()
    {
        try {
            $user = $this->userRepo->show(auth()->id());
            $subscription = $this->stripeRepo->cancelSubscription($user->subscription_id);
            return $this->userRepo->cancelPersonalSubscription($user, $subscription);
        }catch (\Exception $exception){
            throw new StripeErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param SubscribePersonalPlanRequest $request
     * @throws StripeErrorException
     */
    public function subscribePersonalPlan(SubscribePersonalPlanRequest $request)
    {
        try {
            $plan = $this->planRepo->show($request->plan_id);
            $payment_method = $this->paymentMethodRepo->show($request->payment_method_id);
            $subscription = $this->stripeRepo->subscribePlan($plan->stripe_plan, $payment_method->pm_id);
            $this->userRepo->updateUserOnSubscription($plan->id, $payment_method->id, $subscription->id);
        }catch (\Exception $exception){
            throw new StripeErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    public function createPaymentAlert()
    {
        $users = $this->userRepo->getSubscribedUsers();
        foreach ($users as $user)
        {
            $subscription = $this->stripeRepo->getSubscriptionById($user->subscription_id);
            $today = date("Y-m-d");
            $endingDate = date("Y-m-d", $subscription->current_period_end);

            $invoices = $this->stripeRepo->getUpcomingInvoice($user->stripe_id, $user->subscription_id);

            if($today == date('Y-m-d', strtotime($endingDate. ' - 5 days'))){
                $this->notificationRepo->subscriptionEndAlert($user, $subscription);

                $this->notificationRepo->storeNotification(
                    $user->id,
                    'alert',
                    "Invoice created",
                    "Your invoice has been created for ".date('d M, Y',$invoices->lines->data[0]['period']->start). ' to ' .date('d M, Y',$invoices->lines->data[0]['period']->end)
                );
            }

            if($today == date('Y-m-d', strtotime($endingDate. ' + 1 day')))
            {
                $this->notificationRepo->storeNotification(
                    $user->id,
                    'alert',
                    "Payment status",
                    "Your payment status is- ". $invoices->status
                );
            }

        }
    }
}
