<?php


namespace App\Services;


use App\Http\Requests\Documents\StoreDocumentsRequestValidation;
use App\Http\Requests\Projects\AssignCollaboratorsRequestValidation;
use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\AddTotalCollaboratorsRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Http\Resources\Projects\ProjectDetailsResource;
use App\Models\Project;
use App\Repositories\Exceptions\Attachments\AttachmentErrorException;
use App\Repositories\Exceptions\Documents\AssignCollaboratorsException;
use App\Repositories\Exceptions\Documents\DocumentCreateErrorException;
use App\Repositories\Exceptions\Projects\CreatePidErrorException;
use App\Repositories\Exceptions\Documents\DocumentNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectCopyFailedException;
use App\Repositories\Exceptions\Projects\ProjectNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectQueryException;
use App\Repositories\Exceptions\Projects\UserRemoveException;
use App\Repositories\Exceptions\Users\AssignProjectToUserErrorException;
use App\Repositories\Exceptions\Users\CreateUserErrorException;
use App\Repositories\Exceptions\Projects\CreateDeadlineErrorException;
use App\Repositories\Exceptions\Projects\CreateCollaboratorsNumberErrorException;
use App\Repositories\Exceptions\Projects\ProjectCreateErrorException;
use App\Repositories\Exceptions\Projects\ProjectDeleteFailedException;
use App\Repositories\Interfaces\AttachmentRepositoryInterface;
use App\Repositories\Interfaces\DocumentRepositoryInterface;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Repositories\Interfaces\TeamPublicProjectRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Utils\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TeamPublicProjectService
{
    /**
     * @var PublicProjectRepositoryInterface
     */
    private $projectRepo;

    /**
     * @var AttachmentRepositoryInterface
     */
    private $attachmentRepo;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepo;

    /**
     * ProjectService constructor.
     * @param TeamPublicProjectRepositoryInterface $projectRepo
     * @param AttachmentRepositoryInterface $attachmentRepo
     * @param UserRepositoryInterface $userRepo
     * @param DocumentRepositoryInterface $documentRepo
     */
    public function __construct(TeamPublicProjectRepositoryInterface $projectRepo, AttachmentRepositoryInterface $attachmentRepo, UserRepositoryInterface $userRepo, DocumentRepositoryInterface $documentRepo)
    {
        $this->projectRepo = $projectRepo;
        $this->attachmentRepo = $attachmentRepo;
        $this->userRepo = $userRepo;
        $this->documentRepo = $documentRepo;
    }


    /**
     * @param CreateProjectRequestValidation $request
     * @return \App\Models\Project
     * @throws ProjectCreateErrorException
     */
    public function store(CreateProjectRequestValidation $request)
    {
        DB::beginTransaction();
        try {
            $project = $this->projectRepo->store($request);
            $this->projectRepo->assignProjectToUser($project,Constant::USER_TYPE_HOST);
            $this->attachmentRepo->storePrivateProjectAttachments($request, $project);
            DB::commit();
            return $project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new ProjectCreateErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param UpdateProjectRequestValidation $request
     * @param $project_id
     * @return Project
     * @throws ProjectCreateErrorException
     */
    public function update(UpdateProjectRequestValidation $request, $project_id)
    {
        DB::beginTransaction();
        try {
            $project = $this->projectRepo->show($project_id);
            $project = $this->projectRepo->update($request, $project);
            $this->attachmentRepo->storePrivateProjectAttachments($request, $project);
            DB::commit();
            return $project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new ProjectCreateErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param CreateProjectDeadlineRequestValidation $request
     * @param $project_id
     * @return \App\Models\Project
     * @throws CreateDeadlineErrorException
     */
    public function setDeadline(CreateProjectDeadlineRequestValidation $request, $project_id){
        try {
            $project = $this->projectRepo->show($project_id);
            return $this->projectRepo->setDeadline($request,$project);
        }catch (\ErrorException $exception){
            throw new CreateDeadlineErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $id
     * @param AddTotalCollaboratorsRequestValidation $request
     */
    public function storeCollaboratorsNumber($total_collaborators, $project_id)
    {
        try {
            $project = $this->projectRepo->show($project_id);
            return $this->projectRepo->storeCollaboratorsNumber($total_collaborators, $project);
        }catch (\ErrorException $exception){
            throw new CreateCollaboratorsNumberErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param StoreDocumentsRequestValidation $request
     * @param $project_id
     * @throws DocumentCreateErrorException
     */
    public function storeDocuments(StoreDocumentsRequestValidation $request, $project_id)
    {
        try {
            $this->documentRepo->storeDocumentRequirements($request,$project_id);
        }catch (\ErrorException $exception){
            throw new DocumentCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $document_id
     * @throws DocumentNotFoundException
     */
    public function removeDocument($document_id)
    {
        try {
            $document = $this->documentRepo->show($document_id);
            $this->documentRepo->removeDocument($document);
        }catch (\ErrorException $error){
            throw new DocumentNotFoundException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param $document_id
     * @throws DocumentNotFoundException
     */
    public function removeTemplate($document_id){
        try {
            $document = $this->documentRepo->show($document_id);
            $this->documentRepo->removeDocumentTemplate($document);
        }catch (\ErrorException $error){
            throw new DocumentNotFoundException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param array $persons
     * @param int $projectId
     * @param string $userType
     * @return Project
     * @throws CreateUserErrorException
     */
    public function storeCollaboratorOrSupportStaff(array $persons, int $projectId, string $userType):Project
    {
        DB::beginTransaction();
        try {
            foreach ($persons as $person) {
                if(empty($person["id"])){
                    $user = $this->userRepo->findUserByEmail($person['email']);
                    if(!$user) {
                        $user = $this->userRepo->createCollaboratorOrSupportUser($person);
                    }
                    $this->userRepo->assignProject($user, $projectId, $userType, (isset($person['scopes']))?$person['scopes']: '');
                }
            }
            $project = $this->projectRepo->participantCountUpdate($projectId, $userType, count($persons));
            DB::commit();

            return $project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new CreateUserErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param $project_id
     * @param $user_id
     * @param $user_type
     * @throws UserRemoveException
     */
    public function removeUser($project_id, $user_id, $user_type)
    {
        try {
            $project = $this->projectRepo->show($project_id);
            $this->projectRepo->removeUser($project,$user_id, $user_type);
        }catch (\ErrorException $exception){
            throw new UserRemoveException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $project_id
     */
    public function startProject($project_id){
        try{
            $project = $this->projectRepo->show($project_id);
            $project = $this->projectRepo->startProject($project);
            $this->projectRepo->sendProjectInvitation($project);
            if(!$project->pid){
                $this->pid_generate($project_id);
            }
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), 500);
        }
    }

    public function pid_generate($project_id)
    {
        $data = '123456789';
        $pid = substr(str_shuffle($data), 0, 9);

        $new_pid = $this->projectRepo->findProject($pid);

        if(!$new_pid){
            try {
                $project = $this->projectRepo->show($project_id);
                return $this->projectRepo->savePid($pid, $project);
            }catch (\ErrorException $exception){
                throw new CreatePidErrorException($exception->getMessage(), 500);
            }
        }
    }

    public function addProject($project_id)
    {
        try {
            $project = $this->projectRepo->show($project_id);
            $this->projectRepo->assignProjectToUser($project,Constant::USER_TYPE_COLLABORATOR);
            $this->documentRepo->assignProjectDocumentToUser($project);
        }catch (\ErrorException $exception){
            throw new AssignProjectToUserErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $project_id
     * @param $id
     * @return bool
     * @throws AttachmentErrorException
     */
    public function deleteAttachment($project_id, $id)
    {
        try {
            $attachment = $this->attachmentRepo->show($id);
            return $this->attachmentRepo->delete($attachment, $project_id);
        }catch (\ErrorException $exception){
            throw new AttachmentErrorException($exception->getMessage(), 500);
        }
    }

}
