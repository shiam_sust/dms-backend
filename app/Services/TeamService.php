<?php


namespace App\Services;


use App\Http\Requests\Teams\TeamCreateRequestValidation;
use App\Http\Requests\Teams\TeamUpdateRequestValidation;
use App\Models\Team;
use App\Repositories\Exceptions\Teams\TeamCreateException;
use App\Repositories\Exceptions\Teams\TeamPlanChangeException;
use App\Repositories\Exceptions\Teams\TeamException;
use App\Repositories\Interfaces\PaymentMethodRepositoryInterface;
use App\Repositories\Interfaces\PlanRepositoryInterface;
use App\Repositories\Interfaces\StripeRepositoryInterface;
use App\Repositories\Interfaces\TeamRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TeamService
{
    /**
     * @var TeamRepositoryInterface
     */
    private $teamRepo;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var StripeRepositoryInterface
     */
    private $stripeRepo;
    /**
     * @var PaymentMethodRepositoryInterface
     */
    private $paymentMethodRepo;

    /**
     * @var PlanRepositoryInterface
     */
    private $planRepo;

    /**
     * TeamService constructor.
     * @param TeamRepositoryInterface $teamRepo
     * @param UserRepositoryInterface $userRepo
     * @param StripeRepositoryInterface $stripeRepo
     * @param PaymentMethodRepositoryInterface $paymentMethodRepo
     * @param PlanRepositoryInterface $planRepo
     */
    public function __construct(TeamRepositoryInterface $teamRepo, UserRepositoryInterface $userRepo, StripeRepositoryInterface $stripeRepo, PaymentMethodRepositoryInterface $paymentMethodRepo, PlanRepositoryInterface $planRepo)
    {
        $this->teamRepo = $teamRepo;
        $this->userRepo = $userRepo;
        $this->stripeRepo = $stripeRepo;
        $this->paymentMethodRepo = $paymentMethodRepo;
        $this->planRepo = $planRepo;
    }


    /**
     * @param TeamCreateRequestValidation $request
     * @return Team
     * @throws TeamCreateException
     */
    public function store(TeamCreateRequestValidation $request) : Team
    {
        try {

            $this->stripeRepo->storeCustomer();
            $payment_method = $this->stripeRepo->storePaymentMethod($request->card_info, $request->billing_info);

            if($payment_method)
            {
                if($request->is_same){
                    $payment_method = $this->paymentMethodRepo->store($request->card_info,$request->billing_info, $request->billing_info, $payment_method);
                }else{
                    $payment_method = $this->paymentMethodRepo->store($request->card_info,$request->billing_info, $request->mailing_info, $payment_method);
                }
            }
            $plan = $this->planRepo->show($request->plan_id);

            $subscription = $this->stripeRepo->subscribePlan($plan->stripe_plan, $payment_method->pm_id);


            $team = $this->teamRepo->store($request, $plan->id,$payment_method->id, $subscription);
            $this->assignTeamMembers($team, $request->members);
            return $team;
        }catch (\ErrorException $exception){
            throw new TeamCreateException($exception->getMessage(),500);
        }
    }

    public function changePricingPlan(Request $request)
    {
        try{
            if($request->new_payment_method == 1){
                $payment_method = $this->stripeRepo->storePaymentMethod($request->card_info, $request->billing_info);
                if($payment_method)
                {
                    if($request->is_same){
                        $payment_method = $this->paymentMethodRepo->store($request->card_info,$request->billing_info, $request->billing_info, $payment_method);
                    }else{
                        $payment_method = $this->paymentMethodRepo->store($request->card_info,$request->billing_info, $request->mailing_info, $payment_method);
                    }
                }
                $plan = $this->planRepo->show($request->plan_id);
                $team = $this->teamRepo->show($request->team_id);
                $subscription = $this->stripeRepo->subscribePlan($plan->stripe_plan, $payment_method->pm_id);
                $team = $this->teamRepo->ChangeTeamPlan($team, $plan->id,$payment_method->id, $subscription);
                return $team;
            }else{
                $plan = $this->planRepo->show($request->plan_id);
                $team = $this->teamRepo->show($request->team_id);
                $payment_method = $this->paymentMethodRepo->show($team->payment_method_id);
                $subscription = $this->stripeRepo->subscribePlan($plan->stripe_plan, $payment_method->pm_id);
                $team = $this->teamRepo->ChangeTeamPlan($team, $plan->id,$payment_method->id, $subscription);
                return $team;
            }
        }catch (\ErrorException $exception){
            throw new TeamPlanChangeException($exception->getMessage(),500);
        }
    }

    /**
     * @param TeamUpdateRequestValidation $request
     * @param $teamId
     * @return Team
     * @throws TeamCreateException
     * @throws TeamException
     */
    public function update(TeamUpdateRequestValidation $request, $teamId) : Team
    {
        try {
            $team = $this->teamRepo->show($teamId);
            $team = $this->teamRepo->update($team, $request);
            $this->assignTeamMembers($team, $request->members);
            return $team;
        }catch (\ErrorException $exception){
            throw new TeamException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Team $team
     * @param $members
     * @throws TeamCreateException
     */
    public function assignTeamMembers(Team $team, $members)
    {
        DB::beginTransaction();
        try {
            foreach ($members as $member) {
                $user = $this->userRepo->findUserByEmail($member['email']);
                if(empty($member["id"])){
                    if(!$user) {
                        $user = $this->userRepo->createCollaboratorOrSupportUser($member);
                    }
                    $this->userRepo->assignTeam($user, $team->id, $member['position']);
                }else{
                    $this->userRepo->updateTeamUserPosition($user, $team->id, $member['position']);
                }
            }
            //$project = $this->projectRepo->participantCountUpdate($projectId, $userType, count($persons));
            DB::commit();
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new TeamCreateException($exception->getMessage(),500);
        }
    }

    public function removeMember($teamId, $userId)
    {
        try {
            $team = $this->teamRepo->show($teamId);
            $this->teamRepo->removeTeamMember($team, $userId);
        }catch (\ErrorException $exception){
            throw new TeamException($exception->getMessage(), $exception->getCode());
        }
    }


}
