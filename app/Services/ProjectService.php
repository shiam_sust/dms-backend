<?php


namespace App\Services;


use App\Http\Requests\Documents\SaveDecisionRequestValidation;
use App\Http\Requests\Documents\StoreDocumentsRequestValidation;
use App\Http\Requests\Documents\UploadDocumentsRequestValidation;
use App\Http\Requests\Projects\AssignCollaboratorsRequestValidation;
use App\Http\Requests\Projects\CreateProjectDeadlineRequestValidation;
use App\Http\Requests\Projects\CreateProjectRequestValidation;
use App\Http\Requests\Projects\UpdateProjectRequestValidation;
use App\Http\Resources\Projects\ProjectDetailsResource;
use App\Mail\ProjectReminderMail;
use App\Mail\SendDecisionMail;
use App\Models\Project;
use App\Repositories\Exceptions\Attachments\AttachmentErrorException;
use App\Repositories\Exceptions\Documents\AssignCollaboratorsException;
use App\Repositories\Exceptions\Documents\DocumentCreateErrorException;
use App\Repositories\Exceptions\Documents\DocumentNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectCopyFailedException;
use App\Repositories\Exceptions\Projects\ProjectNotFoundException;
use App\Repositories\Exceptions\Projects\ProjectQueryException;
use App\Repositories\Exceptions\Projects\UserRemoveException;
use App\Repositories\Exceptions\Users\CreateUserErrorException;
use App\Repositories\Exceptions\Projects\CreateDeadlineErrorException;
use App\Repositories\Exceptions\Projects\ProjectCreateErrorException;
use App\Repositories\Exceptions\Projects\ProjectDeleteFailedException;
use App\Repositories\Interfaces\AttachmentRepositoryInterface;
use App\Repositories\Interfaces\DocumentRepositoryInterface;
use App\Repositories\Interfaces\ProjectRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Utils\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ProjectService
{
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepo;

    /**
     * @var AttachmentRepositoryInterface
     */
    private $attachmentRepo;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var DocumentRepositoryInterface
     */
    private $documentRepo;

    /**
     * ProjectService constructor.
     * @param ProjectRepositoryInterface $projectRepo
     * @param AttachmentRepositoryInterface $attachmentRepo
     * @param UserRepositoryInterface $userRepo
     * @param DocumentRepositoryInterface $documentRepo
     */
    public function __construct(ProjectRepositoryInterface $projectRepo, AttachmentRepositoryInterface $attachmentRepo, UserRepositoryInterface $userRepo, DocumentRepositoryInterface $documentRepo)
    {
        $this->projectRepo = $projectRepo;
        $this->attachmentRepo = $attachmentRepo;
        $this->userRepo = $userRepo;
        $this->documentRepo = $documentRepo;
    }


    /**
     * @param CreateProjectRequestValidation $request
     * @return \App\Models\Project
     * @throws ProjectCreateErrorException
     */
    public function store(CreateProjectRequestValidation $request)
    {
        DB::beginTransaction();
        try {
            $project = $this->projectRepo->store($request);
            $this->projectRepo->assignProjectToUser($project,Constant::USER_TYPE_HOST, null);
            $this->attachmentRepo->storePrivateProjectAttachments($request, $project);
            DB::commit();
            return $project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new ProjectCreateErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param UpdateProjectRequestValidation $request
     * @param $project_id
     * @return Project
     * @throws ProjectCreateErrorException
     */
    public function update(UpdateProjectRequestValidation $request, $project_id)
    {
        DB::beginTransaction();
        try {
            $project = $this->projectRepo->show($project_id);
            $project = $this->projectRepo->update($request, $project);
            $this->attachmentRepo->storePrivateProjectAttachments($request, $project);
            DB::commit();
            return $project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new ProjectCreateErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param array $persons
     * @param int $projectId
     * @param string $userType
     * @param int $position
     * @return Project
     * @throws CreateUserErrorException
     */
    public function storeCollaboratorOrSupportStaff(array $persons, int $projectId, string $userType):Project
    {
        DB::beginTransaction();
        try {
            foreach ($persons as $person) {
                $user = $this->userRepo->findUserByEmail($person['email']);

                if(empty($person["id"])){
                    if(!$user) {
                        $user = $this->userRepo->createCollaboratorOrSupportUser($person);
                    }
                    $this->userRepo->assignProject($user, $projectId, $userType, (isset($person['scopes']))?$person['scopes']: '', $person['position']);
                }else{
                    $this->userRepo->updateUserPosition($user, $projectId, $person['position']);
                }
            }
            $project = $this->projectRepo->participantCountUpdate($projectId, $userType, count($persons));
            DB::commit();

            return $project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new CreateUserErrorException($exception->getMessage(),500);
        }
    }

    /**
     * @param $projectId
     * @param $collaboratorId
     * @return Collection
     * @throws DocumentCreateErrorException
     */
    public function getDocumentsByCollaborator($projectId, $collaboratorId): Collection
    {
        try {
            return $this->documentRepo->getDocumentsByCollaborator($projectId, $collaboratorId);
        }catch (\ErrorException $exception){
            throw new DocumentCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param CreateProjectDeadlineRequestValidation $request
     * @param $project_id
     * @return \App\Models\Project
     * @throws CreateDeadlineErrorException
     */
    public function setDeadline(CreateProjectDeadlineRequestValidation $request, $project_id){
        try {
            $project = $this->projectRepo->show($project_id);
            return $this->projectRepo->setDeadline($request,$project);
        }catch (\ErrorException $exception){
            throw new CreateDeadlineErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param StoreDocumentsRequestValidation $request
     * @param $project_id
     * @throws DocumentCreateErrorException
     */
    public function storeDocuments(StoreDocumentsRequestValidation $request, $project_id)
    {
        try {
            $this->documentRepo->storeDocumentRequirements($request,$project_id);
        }catch (\ErrorException $exception){
            throw new DocumentCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $id
     * @throws ProjectDeleteFailedException
     */
    public function deleteProject($id){
        try {
            $project = $this->projectRepo->show($id);
            if($project->is_draft == 1){
                $this->projectRepo->deleteProject($project);
                return;
            }

            throw new ProjectDeleteFailedException("Project is not drafted",400);
        }catch (\ErrorException $exception){
            throw new ProjectDeleteFailedException($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @param $id
     * @throws ProjectDeleteFailedException
     */
    public function forceDelete($id){
        try {
            $project = $this->projectRepo->showTrashed($id);
            $this->projectRepo->forceDelete($project);
        }catch (\ErrorException $exception){
            throw new ProjectDeleteFailedException($exception->getMessage(),$exception->getCode());
        }
    }

    /**
     * @param AssignCollaboratorsRequestValidation $request
     * @throws AssignCollaboratorsException
     */
    public function assignCollaborator(AssignCollaboratorsRequestValidation $request){
        try {
            $this->documentRepo->assignCollaborators($request);
        }catch (\ErrorException $exception){
            throw new AssignCollaboratorsException("Assign collaborators is failed", 500);
        }

    }

    /**
     * @param $project_id
     */
    public function startProject($project_id){
        try{
            $project = $this->projectRepo->show($project_id);
            $project = $this->projectRepo->startProject($project);
            $this->projectRepo->sendProjectInvitation($project);
        }catch (\ErrorException $exception){
            throw new ProjectCreateErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $project_id
     * @param $id
     * @return bool
     * @throws AttachmentErrorException
     */
    public function deleteAttachment($project_id, $id)
    {
        try {
            $attachment = $this->attachmentRepo->show($id);
            return $this->attachmentRepo->delete($attachment, $project_id);
        }catch (\ErrorException $exception){
            throw new AttachmentErrorException($exception->getMessage(), 500);
        }
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws ProjectQueryException
     */
    public function readCSVData(Request $request){
        try {
            $header = null;

            $data = array();
            if($request->hasFile("file"))
            {
                $file = fopen($request->file, "r");
                while ( ($row = fgetcsv($file, 100, ",")) !== FALSE) {
                    if (!$header){
                        $header = $row;
                        if(count($row)  != 4 || $row[0] != "email" || $row[1] != "first_name" || $row[2] != "last_name" ||  $row[3] != "organization"){
                            return response()->json(["message" => "invalid file data"], 400);
                        }
                    }
                    else
                        $data[] = array_combine($header, $row);
                }
                fclose($file);
            }
            return $data;
        }catch (\ErrorException $exception){
            throw new ProjectQueryException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $project_id
     * @return Project
     * @throws ProjectCopyFailedException
     */
    public function copyProjectDetails($project_id)
    {
        try {
            DB::beginTransaction();
            $project = $this->projectRepo->show($project_id);
            $clone_project = $this->projectRepo->copyProjectDetails($project);
            DB::commit();
            return $clone_project;
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new ProjectCopyFailedException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $project_id
     * @param $user_id
     * @param $user_type
     * @throws UserRemoveException
     */
    public function removeUser($project_id, $user_id, $user_type)
    {
        try {
            $project = $this->projectRepo->show($project_id);
            $this->projectRepo->removeUser($project,$user_id, $user_type);
        }catch (\ErrorException $exception){
            throw new UserRemoveException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $document_id
     * @throws DocumentNotFoundException
     */
    public function removeDocument($document_id)
    {
        try {
            $document = $this->documentRepo->show($document_id);
            $this->documentRepo->removeDocument($document);
        }catch (\ErrorException $error){
            throw new DocumentNotFoundException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param $document_id
     * @throws DocumentNotFoundException
     */
    public function removeTemplate($document_id){
        try {
            $document = $this->documentRepo->show($document_id);
            $this->documentRepo->removeDocumentTemplate($document);
        }catch (\ErrorException $error){
            throw new DocumentNotFoundException($error->getMessage(), $error->getCode());
        }
    }

    /**
     * @param $project_id
     * @return Project
     * @throws ProjectNotFoundException
     */
    public function stopProject($project_id){
        try {
            $project = $this->projectRepo->show($project_id);
            return $this->projectRepo->stopProject($project);
        }catch (\ErrorException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }
    }

    /**
     * @param $project_id
     * @return Project
     * @throws ProjectNotFoundException
     */
    public function restoreProject($project_id){
        try {
            return $this->projectRepo->restoreProject($project_id);
        }catch (\ErrorException $exception){
            throw new ProjectNotFoundException($exception->getMessage(), 500);
        }
    }

    /**
     * @param UploadDocumentsRequestValidation $request
     * @param $project_id
     * @throws AttachmentErrorException
     */
    public function uploadDocuments(UploadDocumentsRequestValidation $request, $project_id)
    {
        try {
            DB::beginTransaction();
            foreach ($request->documents as $request_document){
                $document = $this->documentRepo->show($request_document['id']);
                if($this->documentRepo->isValidUploadDocument($document, $request_document['file']))
                {
                    $this->attachmentRepo->storeDocumentAttachments($request_document, $project_id);
                    $this->documentRepo->updateDocumentStatus($document, Constant::DOCUMENT_STATUS_SUBMITTED);
                }
            }
            DB::commit();
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new AttachmentErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param SaveDecisionRequestValidation $request
     * @throws DocumentCreateErrorException
     */
    public function saveDecision(SaveDecisionRequestValidation $request)
    {
        try {
            DB::beginTransaction();
            $this->documentRepo->saveDecision($request);
            DB::commit();
        }catch (\ErrorException $exception){
            DB::rollBack();
            throw new DocumentCreateErrorException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param $project_id
     * @param $user_id
     * @return mixed
     * @throws DocumentNotFoundException
     */
    public function downloadDocuments($project_id, $user_id)
    {
        try {
            $documents = $this->documentRepo->getDocumentsByCollaborator($project_id, $user_id);
            return $this->documentRepo->downloadDocumentAttachments($documents);
        }catch (\ErrorException $exception) {
            throw new DocumentNotFoundException($exception->getMessage(), $exception->getCode());
        }
    }

    public function sendDecisionToCollaborators($project_id, $user_id)
    {
        $documents =  $this->documentRepo->getApprovedOrRejectedDocuments($project_id, $user_id);
        $result = array();
        $data = array();
        foreach ($documents as $document){
            $docUser = $document->users()->wherePivot('user_id', $user_id)->first();
            $notes = $document->notes()->where('user_id', $user_id)->orderBy('id', 'desc')->first();
            $data['title'] = $document->title;
            $data['status'] = $docUser ? $docUser->pivot->status : "";
            $data['note'] = $notes ? $notes->note : null;
            $result[] = $data;
        }
        if($docUser){
            Mail::to($docUser->email)
                ->queue(new SendDecisionMail($result));
        }
    }
}
