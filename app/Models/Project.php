<?php

namespace App\Models;

use App\Utils\Constant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{

    use SoftDeletes;

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps()->withPivot('user_type', 'scopes','position');
    }

    public function collaborators()
    {
        return $this->users()
            ->wherePivot('user_type', Constant::USER_TYPE_COLLABORATOR)
            ->orderBy('position');
    }
    public function userHost()
    {
        return $this->users()
            ->wherePivot('user_type', Constant::USER_TYPE_HOST)
            ->first();
    }

    public function support_staffs()
    {
        return $this->users()
            ->wherePivot('user_type', Constant::USER_TYPE_SUPPORT_STAFF)
            ->orderBy('position');
    }

    public function collaboratorsAndSupportStaff()
    {
        return $this->users()->wherePivotIn('user_type', [Constant::USER_TYPE_SUPPORT_STAFF,Constant::USER_TYPE_COLLABORATOR]);
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function documents_by_position()
    {
        return $this->hasMany(Document::class)
            ->orderBy('position');
    }

    public function created_user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function deleted_by_user()
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }
}
