<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function used_user()
    {
        return $this->hasOne(User::class);
    }

    public function used_team()
    {
        return $this->hasOne(Team::class);
    }
}
