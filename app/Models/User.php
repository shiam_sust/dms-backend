<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'date_of_birth',
        'email',
        'password',
    ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function documents()
    {
        return $this->belongsToMany(Document::class, 'document_user');
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class)->withTimestamps();
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class)->withTimestamps();
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function payment_methods()
    {
        return $this->hasMany(PaymentMethod::class);
    }
    public function used_payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function default_team_plan()
    {
        return $this->belongsTo(Plan::class,'default_team_plan_id');
    }

    public function default_personal_plan()
    {
        return $this->belongsTo(Plan::class,'default_personal_plan_id');
    }

    public function profile_documents()
    {
        return $this->hasMany(ProfileDocument::class);
    }

    public function teamLeader()
    {
        return $this->hasMany(Team::class, 'team_leader');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class)
            ->where('type', 'notification');
    }

    public function alerts()
    {
        return $this->hasMany(Notification::class)
            ->where('type', 'alert')
            ->orderBy('id', 'desc');
    }

    public function created_projects()
    {
        return $this->hasMany(Project::class, 'created_by');
    }
}
