<?php

namespace App\Models;

use App\Utils\Constant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    public function project(){
        return $this->belongsTo(Document::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps()->withPivot('user_type', 'status');
    }

    public function collaborators()
    {
        return $this->users()->wherePivot('user_type', Constant::USER_TYPE_COLLABORATOR);
    }

    public function notes()
    {
        return $this->hasMany(DocumentNote::class);
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

//    public function latest_attachment()
//    {
//        return $this->attachments()->latest();
//    }

    public function latest_attachment()
    {
        return $this->morphOne(Attachment::class, 'attachmentable')->latest();
    }

    public function latest_note()
    {
        return $this->hasOne(DocumentNote::class)->latest();
    }
}
