<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('plan_type');
            $table->string('plan_name');
            $table->string('time_period');
            $table->string('stripe_plan')->nullable();
            $table->integer('plan_price');
            $table->integer('min_team_user')->default(0);
            $table->integer('max_active_project');
            $table->integer('max_staff');
            $table->integer('storage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
