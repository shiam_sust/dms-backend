<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('details');
            $table->string('profile_icon');
            $table->double('icon_size')->nullable();
            $table->integer('plan_id')->unsigned()->nullable();
            $table->integer('payment_method_id')->unsigned()->nullable();
            $table->integer('number_of_members')->nullable();
            $table->integer('team_leader')->unsigned()->nullable();
            $table->string('subscription_status')->nullable();
            $table->string('subscription_id')->nullable();
            $table->timestamp('subscription_cancel_at')->nullable();
            $table->timestamp('subscribed_at')->nullable();
            $table->integer('position')->default(0);
            $table->integer('deleted_by')->unsigned()->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
