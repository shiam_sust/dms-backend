<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultPlanIdToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('payment_method_id')->unsigned()->nullable()->after('plan_id');
            $table->integer('default_personal_plan_id')->unsigned()->nullable()->after('plan_id');
            $table->integer('default_team_plan_id')->unsigned()->nullable()->after('plan_id');
            $table->string('subscription_id')->unsigned()->nullable()->after('plan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('payment_method_id');
            $table->dropColumn('default_personal_plan_id');
            $table->dropColumn('default_team_plan_id');
            $table->dropColumn('subscription_id');
        });
    }
}
