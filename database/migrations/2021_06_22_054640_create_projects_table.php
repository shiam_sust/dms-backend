<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->integer('team_id')->unsigned()->nullable();
            $table->integer('pid')->unique()->nullable();
            $table->string('title');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date_first')->nullable();
            $table->timestamp('end_date_second')->nullable();
            $table->integer('total_collaborators')->default(0);
            $table->integer('total_support_staff')->default(0);
            $table->text('project_summary')->nullable();
            $table->string('reminder_lw_first')->nullable();
            $table->string('reminder_lw_second')->nullable();
            $table->string('reminder_before_lw_first')->nullable();
            $table->string('reminder_before_lw_second')->nullable();
            $table->string('status');
            $table->boolean('is_draft')->default(false);
            $table->string('project_type');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();
            $table->string('deleted_from')->nullable();
            $table->integer('stop_by')->nullable();
            $table->timestamp('stop_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
