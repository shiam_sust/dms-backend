<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id')->unsigned();
            $table->text('instruction');
            $table->string('title');
            $table->string('template')->nullable();
            $table->double('template_size')->nullable();
            $table->timestamp('deadline')->nullable();
            $table->string('max_size')->nullable();
            $table->string('size_unit')->nullable();
            $table->string('formats')->nullable();
            $table->boolean('is_draft')->default(false);
            $table->string('status');
            $table->integer('position')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('modified_by')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
