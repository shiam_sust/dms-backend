<?php

namespace Database\Seeders;

use App\Utils\Constant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->truncate();

        $plans = [
            //Personal - Monthly
            [
                "plan_type" => Constant::PLAN_TYPE_PERSONAL,
                "plan_name" => Constant::PLAN_NAME_FREE,
                "time_period" => Constant::PLAN_TIME_PERIOD_MONTHLY,
                "plan_price" => 0,
                "max_active_project" => 1,
                "max_staff" => 10,
                'storage' => 0.5
            ],

            [
                "plan_type" => Constant::PLAN_TYPE_PERSONAL,
                "plan_name" => Constant::PLAN_NAME_SILVER,
                "time_period" => Constant::PLAN_TIME_PERIOD_MONTHLY,
                "plan_price" => 10,
                "max_active_project" => 10,
                "max_staff" => 100,
                'storage' => 5
            ],
            [
                "plan_type" => Constant::PLAN_TYPE_PERSONAL,
                "plan_name" => Constant::PLAN_NAME_GOLD,
                "time_period" => Constant::PLAN_TIME_PERIOD_MONTHLY,
                "plan_price" => 30,
                "max_active_project" => 40,
                "max_staff" => 400,
                'storage' => 20
            ],
//            [
//                "plan_type" => Constant::PLAN_TYPE_PERSONAL,
//                "plan_name" => Constant::PLAN_NAME_PLATINUM,
//                "time_period" => Constant::PLAN_TIME_PERIOD_MONTHLY,
//                "plan_price" => 120,
//                "max_active_project" => 25,
//                "max_staff" => 600,
//                'storage' => 60
//            ],

            //Personal - Annual
            [
                "plan_type" => Constant::PLAN_TYPE_PERSONAL,
                "plan_name" => Constant::PLAN_NAME_BRONZE,
                "time_period" => Constant::PLAN_TIME_PERIOD_ANNUALLY,
                "plan_price" => 0,
                "max_active_project" => 1,
                "max_staff" => 10,
                'storage' => 0.5
            ],
            [
                "plan_type" => Constant::PLAN_TYPE_PERSONAL,
                "plan_name" => Constant::PLAN_NAME_SILVER,
                "time_period" => Constant::PLAN_TIME_PERIOD_ANNUALLY,
                "plan_price" => 8,
                "max_active_project" => 10,
                "max_staff" => 100,
                'storage' => 5
            ],
            [
                "plan_type" => Constant::PLAN_TYPE_PERSONAL,
                "plan_name" => Constant::PLAN_NAME_GOLD,
                "time_period" => Constant::PLAN_TIME_PERIOD_ANNUALLY,
                "plan_price" => 24,
                "max_active_project" => 40,
                "max_staff" => 400,
                'storage' => 20
            ],
//            [
//                "plan_type" => Constant::PLAN_TYPE_PERSONAL,
//                "plan_name" => Constant::PLAN_NAME_PLATINUM,
//                "time_period" => Constant::PLAN_TIME_PERIOD_ANNUALLY,
//                "plan_price" => 96,
//                "max_active_project" => 25,
//                "max_staff" => 600,
//                'storage' => 60
//            ],

        //Team - Monthly
            [
                "plan_type" => Constant::PLAN_TYPE_TEAM,
                "plan_name" => Constant::PLAN_NAME_PLUS,
                "time_period" => Constant::PLAN_TIME_PERIOD_MONTHLY,
                "plan_price" => 9,
                "max_active_project" => 30,
                "max_staff" => 300,
                'storage' => 15
            ],
            [
                "plan_type" => Constant::PLAN_TYPE_TEAM,
                "plan_name" => Constant::PLAN_NAME_PROFESSIONAL,
                "time_period" => Constant::PLAN_TIME_PERIOD_MONTHLY,
                "plan_price" => 28,
                "max_active_project" => 120,
                "max_staff" => 1200,
                'storage' => 60
            ],

            [
                "plan_type" => Constant::PLAN_TYPE_TEAM,
                "plan_name" => Constant::PLAN_NAME_EXECUTIVE,
                "time_period" => Constant::PLAN_TIME_PERIOD_MONTHLY,
                "plan_price" => 55,
                "max_active_project" => 240,
                "max_staff" => 2400,
                'storage' => 120
            ],
//            [
//                "plan_type" => Constant::PLAN_TYPE_TEAM,
//                "plan_name" => Constant::PLAN_NAME_EXECUTIVE,
//                "time_period" => Constant::PLAN_TIME_PERIOD_MONTHLY,
//                "plan_price" => 112,
//                "max_active_project" => 75,
//                "max_staff" => 2000,
//                'storage' => 180
//            ],

            //Team - Annual
            [
                "plan_type" => Constant::PLAN_TYPE_TEAM,
                "plan_name" => Constant::PLAN_NAME_PLUS,
                "time_period" => Constant::PLAN_TIME_PERIOD_ANNUALLY,
                "plan_price" => 8,
                "max_active_project" => 30,
                "max_staff" => 300,
                'storage' => 15
            ],
            [
                "plan_type" => Constant::PLAN_TYPE_TEAM,
                "plan_name" => Constant::PLAN_NAME_PROFESSIONAL,
                "time_period" => Constant::PLAN_TIME_PERIOD_ANNUALLY,
                "plan_price" => 25,
                "max_active_project" => 120,
                "max_staff" => 1200,
                'storage' => 60
            ],

            [
                "plan_type" => Constant::PLAN_TYPE_TEAM,
                "plan_name" => Constant::PLAN_NAME_EXECUTIVE,
                "time_period" => Constant::PLAN_TIME_PERIOD_ANNUALLY,
                "plan_price" => 50,
                "max_active_project" => 240,
                "max_staff" => 2400,
                'storage' => 120
            ],
//            [
//                "plan_type" => Constant::PLAN_TYPE_TEAM,
//                "plan_name" => Constant::PLAN_NAME_EXECUTIVE,
//                "time_period" => Constant::PLAN_TIME_PERIOD_ANNUALLY,
//                "plan_price" => 89.60,
//                "max_active_project" => 75,
//                "max_staff" => 2000,
//                'storage' => 180
//            ],
        ];

        DB::table('plans')->insert($plans);
    }
}
