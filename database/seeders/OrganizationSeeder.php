<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organizations')->insert([
            'name' => 'Kinetik Dynamics',
            'email' => 'kinetik@gmail.com',
            'description' => 'test description test description',
            'address' => 'Dhaka',
            'phone' => '01712121212',
            'plan_id' => 1,
            'is_default' => true,
            'domain' => null
        ]);
    }
}
