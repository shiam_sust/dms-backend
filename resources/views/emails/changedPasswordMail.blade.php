{{-- <p>Your password is: {{$details['password']}}</p><br/> --}}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
      <!--[if !mso]><!-->
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
      <style>
        body {font-family: 'Muli', sans-serif;}
        .nav{
          background-color: #CCD5AE;
          height:80px;
          border: 1px solid black;
          max-width:100%;
          padding: 3px 5px;
        }
        .logo{
          float: left;
          height: 100%;
        }
        .webkit{
          color:black;
          max-width:100%;
          background-color: #EDEDEF;
          padding: 30px 10px 50px 10px;
          border: 1px solid black;
          font-size: 16px;
        }
        
      </style><!--End Head user entered-->
    </head>
    <body>
        <div class="nav">
          <img src="{!! $message->embed(public_path(). '/images/logo.png')  !!}" class="logo" alt="Hellodocuments Logo">
        </div>
        <div class="webkit">
            <p>Dear {{$details['name']}},</p>

            <p>Thanks for using www.HelloDeadlines.com – a one-stop place for collecting documents in a secure manner.</p>

            <p>
                You have now successfully changed the password. Please visit Profile Section on the Dashboard
                to update your password, if needed.
            </p>

            <p>Sincerely,<br/>Support Team </p>

            <p>PS. Please <a href="mailto:support@hellodeadlines.com" style="color: #FF7945">email</a> us in case you are experiencing any issues with Sign Up. We appreciate your business.</p>

        </div>
  </body>

</html>
