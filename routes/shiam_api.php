<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => 'api', 'namespace' => 'App\Http\Controllers\Api'], function () {

    Route::group([
        'prefix' => 'public/projects',
        'middleware' => 'auth:api',
        'namespace' => 'PublicProjects',
    ], function () {
        Route::post("create","PublicProjectsController@store");
        Route::post("/{id}/update","PublicProjectsController@update");
        Route::get("","PublicProjectsController@index");
        Route::get("{id}/primary-info","PublicProjectsController@getPrimaryInfo");
        // Route::get("{id}/collaborators","PrivateProjectsController@getCollaborators");
        Route::get("{id}/support-staffs","PublicProjectsController@getSupportStaffs");
        // Route::post("{id}/collaborators","PrivateProjectsController@storeCollaborators");
        Route::post("{id}/total-collaborators","PublicProjectsController@storeTotalCollaborators");
        Route::post("{id}/support-staffs","PublicProjectsController@storeSupportStaffs");
        Route::put("{id}/set-deadline","PublicProjectsController@setDeadline");
        Route::get("{id}/documents","PublicProjectsController@getDocuments");
        Route::post("{id}/documents","PublicProjectsController@storeDocuments");
        // Route::post("/assign-collaborators","PrivateProjectsController@assignCollaborators");
        Route::delete("/{project_id}/attachments/{id}","PublicProjectsController@deleteAttachment");
        Route::post("/{id}/start-project","PublicProjectsController@startProject");
        // Route::delete("{id}","PrivateProjectsController@destroy")->middleware('isHost');
        // Route::post("{id}/assign-collaborators","PrivateProjectsController@assignCollaborators");
        // Route::get("/{project_id}","PrivateProjectsController@projectDetails");
        // Route::get("/{id}/collaborators/{collaboratorId}/documents","PrivateProjectsController@getDocumentsByCollaborator");
        // Route::post("upload-csv", "PrivateProjectsController@readCSVData");
        // Route::post("{id}/copy-project", "PrivateProjectsController@copyProject");
        Route::delete("{id}/users/{user_id}", "PublicProjectsController@removeUser")->middleware('isHost');
        Route::delete("{id}/documents/{documentId}", "PublicProjectsController@removeDocument")->middleware('isHost');
        Route::delete("{id}/documents/{documentId}/template", "PublicProjectsController@removeTemplate")->middleware('isHost');
        // Route::post("{id}/stop", "PrivateProjectsController@stopProject")->middleware('isHost');
        // Route::post("{id}/restore", "PrivateProjectsController@restoreProject");

        Route::get("account-external", "PublicProjectsController@accountSettingExternal");
    });
});
