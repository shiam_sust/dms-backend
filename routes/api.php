<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1', 'middleware' => 'api', 'namespace' => 'App\Http\Controllers\Api'], function () {

    Route::group([
        'prefix' => 'auth'
    ],function (){
        Route::post('signin', 'AuthController@login');
        Route::post('send-passcode', 'AuthController@sendPasscode');
        Route::post('resend-password', 'AuthController@resendPasscode');
        Route::post('signup', 'AuthController@register');
        Route::post('validate-password', 'AuthController@validatePasscode');
        Route::post('change-password', 'AuthController@changePassword');
        Route::get('logout', 'AuthController@logout');
        Route::get('refresh', 'AuthController@refresh');
        Route::get('me', 'AuthController@me');
        Route::post('forgot-password', 'AuthController@forgotPassword');
        // Route::post('passcode-time', 'AuthController@timeDiff');
    });

//    Route::get('/stripe-payment', 'StripeController@handleGet');
//    Route::post('/stripe-payment', 'StripeController@processSubscription');
//    Route::post('/subscription', 'SubscriptionController@create')->name('subscription.create');

    //Routes for create Plan
    //Route::get('create/plan', 'SubscriptionController@createPlan')->name('create.plan');
//    Route::post('store/plan', 'SubscriptionController@storePlan')->name('store.plan');

    Route::group([
        'prefix' => 'personal/projects',
        'middleware' => 'auth:api',
        'namespace' => 'PersonalProjects',
    ], function () {
        /*Route::post('projects/collaborators','ProjectUsersController@storeCollaborators');
        Route::apiResources([
            'projects' => ProjectsController::class
        ]);*/
        Route::post("private","PrivateProjectsController@store");
        Route::post("/{id}/update","PrivateProjectsController@update");
        Route::get("","PrivateProjectsController@index");
        Route::get("{id}/primary-info","PrivateProjectsController@getPrimaryInfo");
        Route::get("{id}/collaborators","PrivateProjectsController@getCollaborators");
        Route::get("{id}/support-staffs","PrivateProjectsController@getSupportStaffs");
        Route::post("{id}/collaborators","PrivateProjectsController@storeCollaborators");
        Route::post("{id}/support-staffs","PrivateProjectsController@storeSupportStaffs");
        Route::put("{id}/set-deadline","PrivateProjectsController@setDeadline");
        Route::get("{id}/documents","PrivateProjectsController@getDocuments");
        Route::post("{id}/documents","PrivateProjectsController@storeDocuments");
        Route::post("/assign-collaborators","PrivateProjectsController@assignCollaborators");
        Route::delete("/{project_id}/attachments/{id}","PrivateProjectsController@deleteAttachment");
        Route::post("/{id}/start-project","PrivateProjectsController@startProject");
        Route::delete("{id}","PrivateProjectsController@destroy")->middleware('isHost');
        Route::delete("{id}/force-delete","PrivateProjectsController@forceDelete");
        Route::post("{id}/assign-collaborators","PrivateProjectsController@assignCollaborators");
        Route::get("/{project_id}","PrivateProjectsController@projectDetails");
        Route::get("/{project_id}/collaborator-detail","PrivateProjectsController@projectDetailWithCollaborator");
        Route::get("/{id}/collaborators/{collaboratorId}/documents","PrivateProjectsController@getDocumentsByCollaborator");
        Route::post("upload-csv", "PrivateProjectsController@readCSVData");
        Route::post("{id}/copy-project", "PrivateProjectsController@copyProject");
        Route::delete("{id}/users/{user_id}", "PrivateProjectsController@removeUser")->middleware('isHost');
        Route::delete("{id}/documents/{documentId}", "PrivateProjectsController@removeDocument")->middleware('isHost');
        Route::delete("{id}/documents/{documentId}/template", "PrivateProjectsController@removeTemplate")->middleware('isHost');
        Route::post("{id}/stop", "PrivateProjectsController@stopProject")->middleware('isHost');
        Route::post("{id}/restore", "PrivateProjectsController@restoreProject");
        Route::post("{id}/upload-documents", "PrivateProjectsController@uploadDocuments");
        Route::post("{id}/save-decision", "PrivateProjectsController@saveDecision");
        Route::post("{id}/users/{user_id}/download-documents", "PrivateProjectsController@downloadDocuments");
        Route::post("/{id}/collaborators/{collaboratorId}/send-decisions","PrivateProjectsController@sendDecisionToCollaborators");

        // Route::get("{id}/test-doc","PrivateProjectsController@testDoc");


        // Route::get("{id}/testing", "PrivateProjectsController@testingDoc");

    });

    Route::group([
        'prefix' => 'personal/projects',
        'middleware' => 'auth:api',
        'namespace' => 'PublicProjects',
    ],function (){
        Route::post("verify-pid", "PublicProjectsController@verifyProjectByPID");
        Route::get("{pid}/project-details", "PublicProjectsController@projectDetailsByPID");
        Route::post("{id}/add-project", "PublicProjectsController@addProject");
    });

    Route::group([
        'middleware' => 'auth:api',
    ],function (){
        Route::post('payment-method',"StripeController@storePaymentMethod");
        Route::delete('payment-methods/{id}',"StripeController@deletePaymentMethod");
        Route::put('save-card',"StripeController@saveCreditCard");
        Route::get('payment-methods','StripeController@index');
        Route::post("teams", "TeamsController@store");
        Route::post("teams/{id}", "TeamsController@update");
        Route::delete("teams/{id}/members/{user_id}", "TeamsController@removeMember");
        Route::get("teams/{id}", "TeamsController@show");
        Route::get("teams", "TeamsController@index");
        Route::delete("teams/{id}", "TeamsController@destroy");

        Route::get("plans","PlansController@index");
        Route::post('default-plans', "StripeController@saveDefaultPlans");
        Route::get('default-plans', "StripeController@getDefaultPlans");
        Route::get("team-plans", "TeamsController@getTeamPlans");
        Route::get("personal-plans", "PlansController@personalPlan");
        Route::get("subscribed-plan-info", "PlansController@getSubscribedPlanInfo");
        Route::post("subscribe-personal-plans", "StripeController@subscribePersonalPlan");
        Route::delete("teams/{id}/subscription-cancel", "StripeController@teamSubscriptionCancel");
        Route::delete("personal/subscription-cancel", "StripeController@personalSubscriptionCancel");
        Route::post("upgrade-personal-plans", "PlansController@upgradePersonalPlan");
        Route::post("upgrade-team-plans", "TeamsController@upgradeTeamPlan");
        Route::get("plan-charge", "PlansController@planChargeInSummary");


        Route::get("notifications","NotificationsController@index");
        Route::get("used-space","NotificationsController@usedSpaceOfPlan");
        Route::get("alerts","NotificationsController@getAlerts");
        Route::post("mark-as-read-noti","NotificationsController@markAsRead");
        Route::get("notification-count","NotificationsController@notificationCount");

        Route::post("profile", "ProfileController@profileStore");
        Route::post("profile/username/passcode-send", "ProfileController@passcodeToUsername");
        Route::post("profile/username/save", "ProfileController@updateUsername");
        Route::post("profile/resend-passcode", "ProfileController@resendPasscode");
        Route::get("profile/password/resend-passcode", "ProfileController@resendPasscodePassword");
        Route::post("profile/password/passcode-send", "ProfileController@passcodeSendForPassword");
        Route::post("profile/password/save", "ProfileController@updatePassword");
        Route::post("profile/upload-documents", "ProfileController@uploadDocuments");
        Route::get("profile/get-documents", "ProfileController@getDocuments");
        Route::delete("profile/{id}/document", "ProfileController@deleteDocument");
        Route::get("profile/document/{id}/download", "ProfileController@downloadDocument");
        Route::get("profile/get", "ProfileController@getProfile");

        Route::get("account/personal", "ProfileController@getPersonalAccontData");
        Route::get("account/team", "TeamsController@getTeamAccountStatus");
        Route::get("account/team-plans", "TeamsController@getTeamPlansAccount");

        Route::get("search", "ProjectsController@searchProject");

    });

    Route::group([
        'prefix' => 'team/projects',
        'middleware' => 'auth:api',
        'namespace' => 'TeamPrivateProjects',
    ], function () {
        Route::post("private","PrivateProjectsController@store");
        Route::post("/{id}/update","PrivateProjectsController@update");
        Route::get("{id}","PrivateProjectsController@index");
        Route::get("{id}/primary-info","PrivateProjectsController@getPrimaryInfo");
        Route::get("{id}/collaborators","PrivateProjectsController@getCollaborators");
        Route::get("{id}/support-staffs","PrivateProjectsController@getSupportStaffs");
        Route::post("{id}/collaborators","PrivateProjectsController@storeCollaborators");
        Route::post("{id}/support-staffs","PrivateProjectsController@storeSupportStaffs");
        Route::put("{id}/set-deadline","PrivateProjectsController@setDeadline");
        Route::get("{id}/documents","PrivateProjectsController@getDocuments");
        Route::post("{id}/documents","PrivateProjectsController@storeDocuments");
        Route::post("/assign-collaborators","PrivateProjectsController@assignCollaborators");
        Route::delete("/{project_id}/attachments/{id}","PrivateProjectsController@deleteAttachment");
        Route::post("/{id}/start-project","PrivateProjectsController@startProject");
        Route::delete("{id}","PrivateProjectsController@destroy")->middleware('isHost');
        Route::delete("{id}/force-delete","PrivateProjectsController@forceDelete");
        Route::post("{id}/assign-collaborators","PrivateProjectsController@assignCollaborators");
        Route::get("/{project_id}","PrivateProjectsController@projectDetails");
        Route::get("/{id}/collaborators/{collaboratorId}/documents","PrivateProjectsController@getDocumentsByCollaborator");
        Route::post("upload-csv", "PrivateProjectsController@readCSVData");
        Route::post("{id}/copy-project", "PrivateProjectsController@copyProject");
        Route::delete("{id}/users/{user_id}", "PrivateProjectsController@removeUser")->middleware('isHost');
        Route::delete("{id}/documents/{documentId}", "PrivateProjectsController@removeDocument")->middleware('isHost');
        Route::delete("{id}/documents/{documentId}/template", "PrivateProjectsController@removeTemplate")->middleware('isHost');
        Route::post("{id}/stop", "PrivateProjectsController@stopProject")->middleware('isHost');
        Route::post("{id}/restore", "PrivateProjectsController@restoreProject");
        Route::post("{id}/upload-documents", "PrivateProjectsController@uploadDocuments");
        Route::post("{id}/save-decision", "PrivateProjectsController@saveDecision");
        Route::post("{id}/users/{user_id}/download-documents", "PrivateProjectsController@downloadDocuments");
        Route::post("/{id}/collaborators/{collaboratorId}/send-decisions","PrivateProjectsController@sendDecisionToCollaborators");

    });


    Route::group([
        'prefix' => 'team/public',
        'middleware' => 'auth:api',
        'namespace' => 'TeamPublicProjects',
    ], function () {
        Route::post("create","PublicProjectsController@store");
        Route::post("/{id}/update","PublicProjectsController@update");
        //Route::get("","PublicProjectsController@index");
        Route::get("{id}/primary-info","PublicProjectsController@getPrimaryInfo");
        //Route::get("{id}/collaborators","PublicProjectsController@getCollaborators");
        Route::get("{id}/support-staffs","PublicProjectsController@getSupportStaffs");
        //Route::post("{id}/collaborators","PublicProjectsController@storeCollaborators");
        Route::post("{id}/total-collaborators","PublicProjectsController@storeTotalCollaborators");
        Route::post("{id}/support-staffs","PublicProjectsController@storeSupportStaffs");
        Route::put("{id}/set-deadline","PublicProjectsController@setDeadline");
        Route::get("{id}/documents","PublicProjectsController@getDocuments");
        Route::post("{id}/documents","PublicProjectsController@storeDocuments");
        // Route::post("/assign-collaborators","PrivateProjectsController@assignCollaborators");
        Route::delete("/{project_id}/attachments/{id}","PublicProjectsController@deleteAttachment");
        Route::post("/{id}/start-project","PublicProjectsController@startProject");
        // Route::delete("{id}","PrivateProjectsController@destroy")->middleware('isHost');
        // Route::post("{id}/assign-collaborators","PrivateProjectsController@assignCollaborators");
        // Route::get("/{project_id}","PrivateProjectsController@projectDetails");
        // Route::get("/{id}/collaborators/{collaboratorId}/documents","PrivateProjectsController@getDocumentsByCollaborator");
        // Route::post("upload-csv", "PrivateProjectsController@readCSVData");
        // Route::post("{id}/copy-project", "PrivateProjectsController@copyProject");
        Route::delete("{id}/users/{user_id}", "PublicProjectsController@removeUser")->middleware('isHost');
        Route::delete("{id}/documents/{documentId}", "PublicProjectsController@removeDocument")->middleware('isHost');
        Route::delete("{id}/documents/{documentId}/template", "PublicProjectsController@removeTemplate")->middleware('isHost');
        // Route::post("{id}/stop", "PrivateProjectsController@stopProject")->middleware('isHost');
        // Route::post("{id}/restore", "PrivateProjectsController@restoreProject");
    });

});



